/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import * as VueGoogleMaps from 'vue2-google-maps'

require('./materialize');

window.localforage = require('localforage')

window.Vue = require('vue');

import Vue from 'vue';
import { elementOrParentIsFixed } from 'materialize-css';

window.io = require('socket.io-client');

// window.Echo = new Echo({
//     broadcaster: 'socket.io',
//     host: window.location.hostname + ':6001'
// })


/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('register-component', require('./components/auth/RegisterComponent.vue').default);
Vue.component('login-component', require('./components/auth/LoginComponent.vue').default);
Vue.component('forget-password-component', require('./components/auth/ForgetPasswordComponent.vue').default);
Vue.component('reset-password-component', require('./components/auth/ResetPasswordComponent.vue').default);
Vue.component('csv-modal-component', require('./components/CSVModalComponent.vue').default);
Vue.component('google-map-component', require('./components/GoogleMapComponent.vue').default);
Vue.component('app-component', require('./components/AppComponent.vue').default);
Vue.component('log-component', require('./components/LogComponent.vue').default);
Vue.component('import-modal-component', require('./components/modals/ImportModalComponent.vue').default);
Vue.component('add-address-modal-component', require('./components/modals/AddAddressModalComponent.vue').default);
Vue.component('add-transport-group-modal-component', require('./components/modals/AddTransportGroupModalComponent.vue').default);
Vue.component('add-period-group-modal-component', require('./components/modals/AddPeriodGroupModalComponent.vue').default);
Vue.component('create-organisation-component', require('./components/OrganisationCreateComponent.vue').default);
Vue.component('edit-organisation-component', require('./components/OrganisationEditComponent.vue').default);
Vue.component('invite-user-component', require('./components/InviteUserComponent.vue').default);
Vue.component('invite-user-modal', require('./components/modals/InviteUserModal.vue').default);
Vue.component('contact-component', require('./components/ContactComponent.vue').default);
Vue.component('choose-template-modal-component', require('./components/modals/ChooseTemplateModalComponent.vue').default);
Vue.component('create-planboard-component', require('./components/PlanboardCreateComponent.vue').default);
Vue.component('planboard-component', require('./components/PlanboardComponent.vue').default);
Vue.component('edit-planboard-component', require('./components/PlanboardEditComponent.vue').default);
Vue.component('create-template-component', require('./components/TemplateCreateComponent.vue').default);
Vue.component('user-address-component', require('./components/AddresslistComponent.vue').default);
Vue.component('user-transport-component', require('./components/TransportCreateComponent.vue').default);
Vue.component('user-main-sidenav-component', require('./components/SidenavMainComponent.vue').default);
Vue.component('user-location-sidenav-component', require('./components/SidenavLocationComponent.vue').default);
Vue.component('user-addresslist-sidenav-component', require('./components/SidenavAddresslistComponent.vue').default);
Vue.component('user-qeuelist-sidenav-component', require('./components/SidenavQeuelistComponent.vue').default);
Vue.component('user-tile-sidenav-component', require('./components/SidenavTileComponent.vue').default);
Vue.component('user-fullscreen-sidenav-component', require('./components/SidenavFullscreenComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
 Vue.prototype.trans = (key) => {
    return _.get(window.trans, key, key);
};

const app = new Vue({
    el: '#app',
});
