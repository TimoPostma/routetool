/**
 * Load materialize JS
 */

require('materialize-css/dist/js/materialize')

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

document.addEventListener('DOMContentLoaded', () => {
   const sideNavElem = document.querySelectorAll('.sidenav');
   M.Sidenav.init(sideNavElem, {

   });

   const dropdownElem = document.querySelectorAll('.dropdown-trigger');
   M.Dropdown.init(dropdownElem, {
       coverTrigger : false,
       constrainWidth: false,
   });

});
