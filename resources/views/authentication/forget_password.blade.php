@extends('layouts.frontend.main')

@section('title') || Forget password @endsection

@section('content')
    <h2 class="center">@lang('passwords.password_forgot')</h2>
    <p class="center">@lang('passwords.fill_form')</p>

    <div class="container">
        <div class="card hoverable">
            <div class="card-content">
                <form method="post">
                    @csrf
                    <forget-password-component></forget-password-component>
                </form>
            </div>
        </div>
    </div>
@endsection
