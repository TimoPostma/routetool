@extends('layouts.frontend.main')

@section('title') || @lang('login.login') @endsection

@section('content')
    <h2 class="center">@lang('login.login')</h2>

    <div class="container">
        <div class="card hoverable">
            <div class="card-content">
                <form method="post">
                    @csrf
                    <login-component></login-component>
                </form>
            </div>
        </div>
    </div>
@endsection
