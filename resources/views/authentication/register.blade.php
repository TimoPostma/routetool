@extends('layouts.frontend.main')

@section('title') || @lang('register.register') @endsection

@if(Request::is('invitation'))
@section('metadata')
    {{-- Data for the invitiation share link   --}}
    <meta property="og:url" content="{{ Request()->fullUrl() }}"/>
    <meta property="og:title" content="Start using the route tool"/>
    <meta property="og:image" content="{{ asset('assets/logo.png') }}">
    <meta property="og:description" content="Start making efficient routes in the route tool">
    <meta property="og:type" content="article" />
@endsection
@endif


@section('content')
    <div class="container">
        <div class="card hoverable">
            <div class="card-content">
                <h2 class="center">@lang('register.register')</h2>
                <p class="center">@lang('register.register_text')</p>
                <form method="post">
                    @csrf
                    <register-component></register-component>
                </form>
            </div>
        </div>
    </div>
@endsection
