@extends('layouts.frontend.main')

@section('title') || @lang('login.login') @endsection

@section('content')

    <div class="container">
        <div class="card hoverable">

            <div class="card-content">
                <h2 class="center">@lang('login.login')</h2>
                <form method="post">
                    @csrf
                    <login-component></login-component>
                </form>
            </div>
        </div>
    </div>
@endsection
