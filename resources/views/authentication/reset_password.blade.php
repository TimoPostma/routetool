@extends('layouts.frontend.main')

@section('title') || Reset password @endsection

@section('content')
    <h2 class="center">Reset password</h2>
    <p class="center">Fill the form below to reset your password</p>

    <div class="container">
        <div class="card hoverable">
            <div class="card-content">
                <form method="post">
                    @csrf
                    <reset-password-component></reset-password-component>
                </form>
            </div>
        </div>
    </div>
@endsection
