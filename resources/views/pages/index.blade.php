@extends('layouts.frontend.main')

@section('content')

        <div class="login-reminder card center-children">
            <h3 class="center">Transport route tool</h3>
            <p class="center">@lang('index.trt_description')<br>
               @guest @lang('index.guest_login_message') @endguest </p>
            <div class="center">
                @guest
                    <a class="btn" href="{{route('login')}}">@lang('index.login')</a>
                    <a class="btn" href="{{route('register')}}">@lang('index.register')</a>
                @endguest
                @auth
                    <a class="btn" href="{{ route('user.organisations.index') }}">@lang('index.goto_organisations')</a>
                @endauth

            </div>
        </div>
@endsection
@section('extraCSS')
    <style>
        .login-reminder {
            padding: 5px 20px 30px 20px;
        }

        h3 {
            text-transform: capitalize;
        }

        main {
            background: url("https://images.unsplash.com/photo-1486673748761-a8d18475c757?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&auto=format&fit=crop&w=1350&q=80");
            background-repeat: no-repeat;
            background-position: center;
            background-size: cover;
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
        }

    </style>
@endsection
