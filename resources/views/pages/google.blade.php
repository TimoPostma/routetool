<div id="map">

</div>

@section('extraCSS')
    <style>
        #map {
            height: 400px;
            width: 100%;
        }
    </style>
@endsection

@section('extraJS')
    <script type="text/javascript">
       setTimeout(() => {

           const produvar = { lat: 53.3003053, lng: 6.5984475 }

           const map = new google.maps.Map(document.getElementById("map"), {
               center: produvar,
               zoom: 15,
           });

           const marker = new google.maps.Marker({
               position: produvar,
               map
           })

       }, 150)
    </script>
@endsection
