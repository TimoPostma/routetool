@extends('layouts.frontend.main')

@section('content')

    <h1>You are currently not connected to any networks.</h1>

    <app-component></app-component>

@endsection
