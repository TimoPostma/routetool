@extends('layouts.organisation.main')
@section('title') || @lang('organisations.show') @endsection
@section('content')
    <h2 class="center">@lang('organisations.planning_page')</h2>
    <div class="center">
        <p>@lang('organisations.planning_page_explain')</p>
        <p>@lang('organisations.planning_page_description')</p>
    </div>

    <div class="container">
        @if(count($plannings) < 1)
            <div class="center">
                <p>@lang('organisations.no_plannings_added')</p>
                <a class="btn btn-waves" href="{{ route('organisation.planning.create', ['id' => $organisation->id]) }}">@lang('organisations.add_planning')</a>
            </div>
        @else
            <div class="center">
                <a class="btn btn-waves" href="{{ route('organisation.planning.create', ['id' => $organisation->id]) }}">@lang('organisations.add_planning')</a>
            </div>

            <table class="table striped">
                <thead>
                <tr>
                    <th>@lang('general.name')</th>
                    <th>@lang('general.description')</th>
                    <th>@lang('general.created_at')</th>
                </tr>
                </thead>
                <tbody>
                @foreach($plannings as $planning)
                    <tr>
                        <td>{{ $planning->name }}</td>
                        <td>{{ $planning->description }}</td>
                        <td>{{ \Carbon\Carbon::parse($planning->created_at)->format('d-m-Y H:i') }}</td>
                        <td>
                          @if($planning->is_locked_set == 1)
                          <i class="material-icons">lock</i>
                        @endif()
                        </td>
                        <td><a href="{{ route('organisation.planning.show', ['id' => $organisation->id, 'planningId' => $planning->id]) }}">@lang('general.show')</a></td>
                    </tr>
                @endforeach
                </tbody>

            </table>

        @endif
    </div>
@endsection
