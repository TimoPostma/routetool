@extends('layouts.user.main')

@section('title') || @lang('organisations.create') @endsection
@section('content')
    <h2 class="center">@lang('organisations.create_organisation')</h2>
    <p class="center">@lang('organisations.create_organisation_description')</p>

    <div class="container">
        <div class="card hoverable">
            <div class="card-content">
                <form method="post" action="{{ route('user.organisations.store') }}">
                    @csrf
                    <create-organisation-component></create-organisation-component>
                </form>
            </div>
        </div>
    </div>
@endsection
