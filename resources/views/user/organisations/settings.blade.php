@extends('layouts.organisation.main')
@section('title') || @lang('organisations.settings') @endsection
@section('content')
    <h2 class="center">@lang('organisations.settings_page') {{ $organisation->name }}</h2>

    <div class="container">
        <div class="">
            <div class="">
                <form method="post" action="{{ route('user.organisations.save',$organisation->id) }}">
                    @csrf
                    <edit-organisation-component 
                        organisation_id="{{ $organisation['id'] }}"
                        organisation_name="{{ $organisation->name }}"
                        organisation_description = "{{$organisation->description}}" 
                        organisation_itemtime = "{{$organisation->item_time}}"
                        organisation_overheadtime = "{{$organisation->overhead_time}}"
                    ></edit-organisation-component>
                </form>
            </div>
        </div>
    </div>
@endsection
