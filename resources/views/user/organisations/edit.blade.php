@extends('layouts.user.main')
@section('title') || @lang('organisations.edit') @endsection
@section('content')
    <h2 class="center">@lang('organisations.edit_organisation') {{$organisation->name}}</h2>
    <p class="center">@lang('organisations.edit_organisation_description')</p>

    <div class="container">
        <div class="card hoverable">
            <div class="card-content">
                <form method="post" action="{{ route('user.organisations.save',$organisation->id) }}">
                    @csrf
                    <edit-organisation-component 
                        organisation_id="{{ $organisation['id'] }}"
                        organisation_name="{{ $organisation->name }}"
                        organisation_description = "{{$organisation->description}}" 
                    ></edit-organisation-component>
                </form>
            </div>
        </div>
    </div>
@endsection