@extends('layouts.organisation.main')

@section('content')
    <h2 class="center">@lang('organisations.import_settings')</h2>

    <div class="container">
        @if(count($importSettings) < 1)
            <div class="center">
                <p>@lang('organisations.no_import_settings_added')</p>
                <a href="{{ route('import.settings.create', ['id' => $organisationId]) }}" class="btn btn-waves">@lang('organisations.create_import_settings')</a>
            </div>
        @else
            <div class="center">
                <a href="{{ route('import.settings.create', ['id' => $organisationId]) }}" class="btn btn-waves">@lang('organisations.create_import_settings')</a>
            </div>

            <table class="table striped">
                <thead>
                <tr>
                    <th>@lang('organisations.name')</th>
                    <th>@lang('organisations.description')</th>
                </tr>
                </thead>
                <tbody>
                @foreach($importSettings as $setting)
                    <tr>
                        <td>{{ $setting->name }}</td>
                        <td>{{ $setting->description }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @endif
    </div>
@endsection
