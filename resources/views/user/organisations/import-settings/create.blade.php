@extends('layouts.organisation.main')

@section('content')
    <h2 class="center">Maak import settings</h2>

{{--  @TODO make a vue component for checking data because of time do it this way without frondend validation  --}}

    <div class="container">
        <div class="card hoverable">
            <div class="card-content">
                <div class="row">
                    <form method="post" action="{{ route('import.settings.store', ['id' => $organisationId]) }}">
                        @csrf
                        <div class="col s12 input-field">
                            <input type="text" name="name">
                            <label>@lang('organisations.name')</label>
                        </div>
                        <div class="col s12 input-field">
                            <input type="text" name="description">
                            <label>@lang('organisations.description')</label>
                        </div>
                        <div class="col s12 input-field">
                            <input type="text" name="address_header">
                            <label>@lang('organisations.address_header')</label>
                        </div>
                        <div class="col s12 input-field">
                            <input type="text" name="place_header">
                            <label>@lang('organisations.place_header')</label>
                        </div>
                        <div class="col s12 input-field">
                            <input type="text" name="period_header">
                            <label>@lang('organisations.period_header')</label>
                        </div>
                        <div class="col s12 input-field">
                            <input type="text" name="transport_header">
                            <label>@lang('organisations.transport_header')</label>
                        </div>
                        <div class="col s12 input-field">
                            <input type="text" name="overhead_time_header">
                            <label>@lang('organisations.overhead_time_header')</label>
                        </div>
                        <div class="col s12 input-field">
                            <input type="text" name="item_time_header">
                            <label>@lang('organisations.item_time_header')</label>
                        </div>
                        <div class="col s12 input-field">
                            <input type="text" name="item_header">
                            <label>@lang('organisations.item_header')</label>
                        </div>
                        <div class="center">
                            <button class="btn waves-effect" type="submit">@lang('organisations.save_settings')</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
