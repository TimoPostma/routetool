@extends('layouts.user.main')
@section('title') || @lang('organisations.index') @endsection
@section('content')

    <h2 class="center">@lang('organisations.organisations')</h2>
    <p class="center">@lang('organisations.organisations_description')</p>

    @if(count($ownerOrganisations) < 1 && count($invitedOrganisations) < 1)
        <div class="center">
            <p>@lang('organisations.no_organisations_added')</p>
            <a href="{{ route('user.organisations.create') }}" class="btn btn-waves">@lang('organisations.add_organisation')</a>
        </div>
    @else
        <div class="center">
            <a href="{{ route('user.organisations.create') }}" class="btn btn-waves">@lang('organisations.add_organisation')</a>
        </div>
        <div class="container">
            <table class="table striped">
                <thead>
                <tr>
                    <th>@lang('organisations.name')</th>
                    <th>@lang('organisations.description')</th>
                    <th>@lang('organisations.created_at')</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($ownerOrganisations as $userOrganisation)
                    <tr>
                        <td>{{ $userOrganisation->name }}</td>
                        <td>{{ $userOrganisation->description }}</td>
                        <td>{{ \Carbon\Carbon::parse($userOrganisation->created_at)->format('d-m-Y H:i:s') }}</td>
                        <td><a href="{{ route('user.organisations.show', ['id' => $userOrganisation->id]) }}">@lang('organisations.show')</a></td>
                        
                    </tr>
                @endforeach
                @foreach($invitedOrganisations as $invitedOrganisation)
                    <td>{{ $invitedOrganisation->name }}</td>
                    <td>{{ $invitedOrganisation->description }}</td>
                    <td>{{ \Carbon\Carbon::parse($invitedOrganisation->created_at)->format('d-m-Y H:i:s') }}</td>
                    <td><a href="{{ route('user.organisations.show', ['id' => $invitedOrganisation->id]) }}">@lang('organisations.show')</a></td>
                @endforeach
                </tbody>
            </table>
        </div>
    @endif
@endsection

@section('extraJS')
    <script type="text/javascript">
        const modalElem = document.querySelectorAll('.modal');

    </script>
@endsection
