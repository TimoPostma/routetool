@extends('layouts.organisation.main')
@section('title') || @lang('user.index') @endsection
@section('content')
    <div class="center">
        <h2>@lang('organisations.users')</h2>
        <p>@lang('organisations.users_explain')</p>
        <p>@lang('organisations.users_description')</p>
    </div>

    <div class="container">
        <div class="row">
            <div class="col s12 m12 l12">
                <h4>@lang('organisations.invite_user')</h4>
                <form method="post" action="{{ route('user.organisations.invite', ['id' => $organisation->id]) }}">
                    @csrf
                    <div class="col s12 input-field">
                        <input id="email" type="email" name="email" required>
                        <label for="email">Email</label>
                    </div>
                    <div class="center">
                        <button type="submit" class="btn waves-effect">@lang('organisations.invite_user')</button>
                    </div>
                </form>
            </div>
        </div>
    </div>



    <div class="container">
            @if($organisation->users->count() < 1)

            @else
            <h4 class="center">@lang('organisations.users')</h4>
            <div class="container">
                <table class="table striped">
                    <thead>
                    <tr>
                        <th>@lang('organisations.name')</th>
                        <th>Email</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($organisation->users as $user)
                        <tr>
                            <td> {{ $user->name }} </td>
                            <td>{{ $user->email }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </div>
@endsection
