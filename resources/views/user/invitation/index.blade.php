@extends('layouts.user.main')

@section('content')
    <h2 class="center">@lang('invitations.invite_users')</h2>
    <p class="center">@lang('invitations.invite_a_user')</p>

    <div class="container">
        <invite-user-component invitation-generated-url="{{ route('invitation-url') }}" link-expired-url="{{ route('invitation-link-expired') }}"></invite-user-component>
    </div>
@endsection
