@extends('layouts.organisation.main')

@section('content')
    <h2 class="center">@lang('transport.edit_page')</h2>
    
    
    <div class="container">
        <div class="card hoverable">
            <div class="card-content">
                <form method="post" action="{{ route('user.organisations.store') }}">
                    @csrf
                    <user-transport-component 
                    apitoken="{{Auth()->user()->api_token}}"
                    id="{{$id}}"   
                    organisationstring="{{$organisation}}"
                    transportstring="{{$transport}}" 
                    ></user-transport-component>
                </form>
            </div>
        </div>
    </div>
@endsection





