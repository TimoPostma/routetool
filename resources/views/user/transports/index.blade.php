@extends('layouts.transport.main')

@section('content')
<h2 class="center">@lang('transport.page')</h2>
    <div class="center">
        <p>@lang('transport.explain')</p>
        <p>@lang('transport.page_description')</p>
    </div>
    @if(count($transports) < 1)
        <div class="center">
            <p>@lang('transport.empty')</p>
            <a href="{{ route('organisation.transport.create',['id'=>$id]) }}" class="btn btn-waves">@lang('transport.add')</a>
        </div>
    @else
    <div class="center">
        <a href="{{ route('organisation.transport.create',['id'=>$id]) }}" class="btn btn-waves">@lang('transport.add')</a>
    </div>
    <div class="container">

    <table class="table striped">
    <thead>
                <tr>
                    <th>@lang('general.label')</th>
                    <th>@lang('general.method')</th>
                    <th>@lang('general.speed')</th>
            
                    <th>@lang('general.edit')</th>
                    <th>@lang('general.delete')</th>
             
                </tr>
    </thead>
    
    @foreach($transports as $transport)
    
                    <tr>
                        <td>{{ $transport->label }}</td>
                        <td>{{ json_decode($transport['transportdata'])->method }}</td>
                        <td>{{ json_decode($transport['transportdata'])->speed }}</td>
                       
                           
                        <td><a href="{{ route('organisation.transport.edit',['transportId' => $transport->id,'id'=>$id]) }}"><i class="material-icons">edit</i></a></td>
                        <td>
                            <form action="{{route('organisation.transport.delete',['transportId' => $transport->id,'id'=>$id])}}" method="post">
                            @csrf
                                <input type="hidden" value="{{$transport->organisation_id}}" name="organisationId">
                                <input type="hidden" value="{{$transport->id}}" name="transportId">
                                <button class="btn" type="submit"><i class="material-icons prefix">delete</i></button>
                            </form>
                        </td>
                      
                    </tr>

    @endforeach
    </table>
    </div>
@endif
@endsection

@section('extraJS')
    <script type="text/javascript">
        const modalElem = document.querySelectorAll('.modal');

    </script>
@endsection
