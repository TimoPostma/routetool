@extends('layouts.frontend.main')
@section('title') || @lang('template.create') @endsection
@section('content')
<create-template-component
    transports_string="[{{$transports}}]"
    periods_string="[{{$periods}}]"
    cells_string="[{{$rows}}]"
    organisation="{{$organisation}}"
></create-template-component>
    
@endsection