@extends('layouts.organisation.main')
@section('title') || @lang('template.index') @endsection
@section('content')
<h2 class="center">@lang('template.header')</h2>

    @if(count($templates) < 1)
        <div class="center">
            <p>@lang('template.no_templates_added')</p>

        </div>
    @else
    <div class="center">
    <p>@lang('template.explain')</p>
    <p class="center">@lang('template.subheader')</p>
        </div>
        <div class="container">
            <table class="table striped">
                <thead>
                <tr>
                    <th>@lang('general.name')</th>
                    <th>@lang('general.description')</th>
                    <!-- <th>@lang('general.created_at')</th> -->
                    <!-- <th></th> -->
                </tr>
                </thead>
@foreach ($templates as $template)
    <tr><td>{{$template->label}}</td>
    <td>{{$template->description}}</td>
    <!-- <td><a href="{{ route('user.templates.show', ['id' => $organisation->id, 'templateId'=> $template->id]) }}">@lang('general.show')</a></td>
    <td><a href=""><i class="material-icons">edit</i></a></td></tr> -->
@endforeach

</table>
@endif
</div>


@endsection
