@extends('layouts.organisation.main')

@section('content')
    <h2 class="center">@lang('address.create_page')</h2>
    

    <div class="container">
        <div class="card hoverable">
            <div class="card-content">
                <form method="post" action="{{ route('user.organisations.store') }}">
                    @csrf
                    <user-address-component 
                    apitoken="{{Auth()->user()->api_token}}"
                    id="{{$id}}"   
                    organisationstring="{{$organisation}}" 
                    ></user-address-component>
                </form>
            </div>
        </div>
    </div>
@endsection





