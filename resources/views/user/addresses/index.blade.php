@extends('layouts.organisation.main')

@section('content')
<h2 class="center">@lang('address.page')</h2>
    <div class="center">
        <p>@lang('address.explain')</p>
        <p>@lang('address.page_description')</p>
    </div>
    @if(count($addresses) < 1 && count($addresses) < 1)
        <div class="center">
            <p>@lang('address.empty')</p>
            <a href="{{ route('organisation.address.create',['id'=>$id]) }}" class="btn btn-waves">@lang('address.add')</a>
        </div>
    @else
    <div class="center">
        <a href="{{ route('organisation.address.create',['id'=>$id]) }}" class="btn btn-waves">@lang('address.add')</a>
    </div>
    <div class="container">

    <table class="table striped">
    <thead>
                <tr>
                    <th>@lang('general.label')</th>
                    <th>@lang('general.street')</th>
                    <th>@lang('general.zipcode')</th>
                    <th>@lang('general.city')</th>
                    <th>@lang('general.edit')</th>
                    <th>@lang('general.delete')</th>
                    <th>@lang('general.favorite')</th>
                </tr>
    </thead>
    @foreach($addresses as $address)

                    <tr>
                        <td>{{ $address->label }}</td>
                        <td>{{ $address->street }}</td>
                        <td>{{ $address->zipcode }}</td>
                        <td>{{ $address->city }}</td>

                        <td><a href="{{ route('organisation.address.edit',['addressId' => $address->id,'id'=>$id]) }}"><i class="material-icons">edit</i></a></td>
                        <td>
                            <form action="{{route('organisation.address.delete',['addressId' => $address->id,'id'=>$id])}}" method="post">
                            @csrf
                                <input type="hidden" value="{{$address->organisation_id}}" name="organisationId">
                                <input type="hidden" value="{{$address->id}}" name="addressId">
                                <button class="btn" type="submit"><i class="material-icons prefix">delete</i></button>
                            </form>
                        </td>
                        <td>
                            <form action="{{route('organisation.address.favorite',['addressId' => $address->id,'id'=>$id])}}" method="post">
                            @csrf
        @if($address->is_favorite)                    
                        <button class="btn" type="submit"><i class="material-icons prefix">favorite</i></button>
         
        @else
                        <button class="btn favorite" type="submit"><i class="material-icons prefix">favorite</i></button>
                        </form>
        @endif                
                        </td>
                    </tr>

    @endforeach
    </table>
    </div>
@endif
@endsection

@section('extraJS')
    <script type="text/javascript">
        const modalElem = document.querySelectorAll('.modal');

    </script>
@endsection
