@extends('layouts.planboard.main')
@section('title') || @lang('planboard.show') @endsection
@section('content')
 

    <app-component 
    organisationId="{{ $planning->organisation_id }}"
    organisationlabel="{{ $organisation->name }}" 
    organisationitemtime= "{{ $organisation->item_time }}" 
    organisationoverheadtime= "{{ $organisation->overhead_time }}" 

    planningId="{{ $planning->id }}" 
    planboardlabel="{{ $planning->name }}"
    planboardtype="{{ $planning->is_locked_set }}"

    planboarddescription="{{$planning->description}}"
    apitoken="{{ Auth()->user()->api_token }}"
    start="{{$start}}" 
    ></app-component>
@endsection
