@extends('layouts.frontend.main')
@section('title') || @lang('planboard.edit') @endsection
@section('content')
    <h2 class="center">@lang('organisations.edit_planning')</h2>

    <div class="container">

        <edit-planboard-component
        planboard_name="{{$planboard['name']}}"
        planboard_desc="{{$planboard['description']}}"
        ></edit-planboard-component>
    </div>
@endsection
