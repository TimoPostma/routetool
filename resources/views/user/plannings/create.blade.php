@extends('layouts.organisation.main')
@section('title') || @lang('planboard.create') @endsection
@section('content')
    <h2 class="center">@lang('organisations.add_planning')</h2>

    <div class="container">
        <create-planboard-component></create-planboard-component>
    </div>
@endsection
