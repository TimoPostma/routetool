@extends('layouts.admin.main')

@section('content')
    <h2 class="center">Logs</h2>
    <p class="center">Hieronder vindt u alle logs die onze database zijn gedaan</p>

    <div class="container">
        <log-component apitoken="{{ Auth()->user()->api_token }}"></log-component>
    </div>
@endsection
