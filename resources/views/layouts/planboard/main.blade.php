<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    @include('layouts.planboard.components._head')
</head>

<body>

<header>
    @include('layouts.planboard.components._nav')
</header>

<main>
    <div id="app">
        @yield('content')
    </div>
</main>



@include('layouts.planboard.components._scripts')
</body>
</html>
