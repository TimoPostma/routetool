@include('layouts.planboard.components._stylesheets')

<!--Let browser know website is optimized for mobile-->
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
@yield('metadata')
<title>Route tool @yield('title')</title>
@laravelPWA
