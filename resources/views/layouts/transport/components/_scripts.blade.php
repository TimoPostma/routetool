<script type="text/javascript" src="{{ mix('js/app.js') }}"></script>
<script type="text/javascript">
    if('{{ Session::has('error') }}') {
        M.toast({html: '{{ Session::get('error') }}', classes: 'red', displayLength: 4000})
    }
    if('{{ Session::has('warning') }}') {
        M.toast({html: '{{ Session::get('warning') }}', classes: 'orange', displayLength: 4000})
    }
    if('{{ Session::has('success') }}') {
        M.toast({html: '{{ Session::get('success') }}', classes: 'green', displayLength: 4000})
    }
</script>


@yield('extraJS')
