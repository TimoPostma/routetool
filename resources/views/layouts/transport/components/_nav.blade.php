<nav>
    <div class="nav-wrapper">

        <a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
    
        <div class="breadcrumbs">
            <a class="organisation" href="{{ route('user.organisations.index') }}">@lang('organisations.organisations')</a> / 
            <a class="organisation">{{ $organisation->name }}</a> /
            <a class="organisation">@lang('transport.transport')</a>
           
          
        </div>
   
 
        <ul class="right hide-on-med-and-down">
            @guest()
                <li><a href="{{ route('register') }}">@lang('nav.register')</a></li>
                <li><a href="{{ route('login') }}">@lang('nav.login')</a></li>
            @endguest
                <li><a class="dropdown-trigger" href="#!" data-target="language">@lang('nav.language')<i class="material-icons right">arrow_drop_down</i></a></li>
                <li><a class="dropdown-trigger" href="#!" data-target="account">Account<i class="material-icons right">arrow_drop_down</i></a></li>

        </ul>
    </div>
</nav>

<ul class="sidenav sidenav-fixed" id="mobile-demo">
    <div class="center" style="padding:50px">
        <img width="150px" height="150px" src="/assets/logo.png">
    </div>
    
    
    @if($organisation->user_id === Auth()->user()->id)
    
    <!-- <li><a href="{{ route('user.organisations.index', ['id' => $organisation->id]) }}">@lang('nav.organisations')<i class="material-icons">business</i></a></li> -->
    <a class="btn menu-button" href="{{ route('user.organisations.show', ['id' => $organisation->id]) }}"><i class="material-icons icon">laptop</i><span class="text">@lang('nav.planboards')</span></a>
    <a class="btn menu-button" href="{{ route('user.templates.index', ['id' => $organisation->id]) }}">@lang('nav.templates')<i class="material-icons icon">art_track</i></a>
    <a class="btn menu-button" href="{{ route('user.organisations.users', ['id' => $organisation->id]) }}">@lang('nav.users')<i class="material-icons icon">people_outline</i></a>
    <a class="btn menu-button" href="{{ route('organisation.address.index', ['id' => $organisation->id]) }}">@lang('nav.addresses')<i class="material-icons icon">edit_location</i></a>
    <a class="btn menu-button" href="{{ route('organisation.transport.index', ['id' => $organisation->id]) }}">@lang('nav.transports')<i class="material-icons icon">local_shipping</i></a>
    <a class="btn menu-button" href="{{ route('user.organisations.settings', ['id' => $organisation->id]) }}">@lang('nav.settings')<i class="material-icons icon">settings</i></a>
    <a class="btn menu-button align-bottom" href="{{ route('user.organisations.index', ['id' => $organisation->id]) }}">@lang('nav.back')<i class="material-icons icon">arrow_backward</i></a>    
    <!-- <li><a href="{{ route('import.settings.index', ['id' => $organisation->id]) }}">Import/Export Excel<i class="material-icons">import_export</i></a></li> -->
    @endif
</ul>

<ul id="language" class="dropdown-content">
    <li><a href="{{ route('locale', ['locale' => 'nl']) }}">@lang('nav.languages.nl')</a></li>
    <li><a href="{{ route('locale', ['locale' => 'en']) }}">@lang('nav.languages.en')</a></li>
</ul>
<ul id="language-mobile" class="dropdown-content">
    <li><a href="{{ route('locale', ['locale' => 'nl']) }}">@lang('nav.languages.nl')</a></li>
    <li><a href="{{ route('locale', ['locale' => 'en']) }}">@lang('nav.languages.en')</a></li>
</ul>

<ul id="account" class="dropdown-content">
    <li><a href="{{ route('user.organisations.index') }}">@lang('nav.organisations')</a></li>
    <li><a href="{{ route('user.invitations.index') }}">@lang('nav.invitations')</a></li>
    <li><a href="{{ route('logout') }}">@lang('nav.logout')</a></li>
</ul>


