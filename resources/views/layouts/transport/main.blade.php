<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    @include('layouts.transport.components._head')
</head>

<body>

<header>
    @include('layouts.transport.components._nav')
</header>

<main>
    <div id="app">
        @yield('content')
    </div>
</main>

<footer class="page-footer">
    @include('layouts.transport.components._footer')
</footer>

<!--JavaScript at end of body for optimized loading-->
@include('layouts.transport.components._scripts')
</body>
</html>
