<nav>
    <div class="nav-wrapper">

        <img width="62px" height="62px" class="main-logo-frontend" src="{{ asset('assets/logo.png') }}">
        <a href="{{ route('pages.index') }}" class="brand-logo">Transport Route Tool</a>
        <div class="breadcrumbs">
 
        </div>
        <a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
        <ul class="right hide-on-med-and-down">
            @guest()
                <li><a href="{{ route('register') }}">@lang('nav.register')</a></li>
                <li><a href="{{ route('login') }}">@lang('nav.login')</a></li>
            @endguest
            <li><a class="dropdown-trigger" href="#!" data-target="language">@lang('nav.language')<i class="material-icons right">arrow_drop_down</i></a></li>
            @auth()
                <li><a href="#!" class="dropdown-trigger" data-target="account">Account<i class="material-icons right">arrow_drop_down</i></a></li>
            @endauth()
        </ul>
    </div>
    <div class="nav-margin">

    </div>
</nav>

<ul class="sidenav" id="mobile-demo">
    @guest()
        <li><a href="{{ route('register') }}">@lang('nav.register')</a></li>
        <li><a href="{{ route('login') }}">@lang('nav.login')</a></li>
    @endguest
        <li><a class="dropdown-trigger" href="#!" data-target="language-mobile">@lang('nav.language')<i class="material-icons right">arrow_drop_down</i></a></li>
</ul>

<ul id="language" class="dropdown-content">
    <li><a href="{{ route('locale', ['locale' => 'nl']) }}">@lang('nav.languages.nl')</a></li>
    <li><a href="{{ route('locale', ['locale' => 'en']) }}">@lang('nav.languages.en')</a></li>
</ul>
<ul id="language-mobile" class="dropdown-content">
    <li><a href="{{ route('locale', ['locale' => 'nl']) }}">@lang('nav.languages.nl')</a></li>
    <li><a href="{{ route('locale', ['locale' => 'en']) }}">@lang('nav.languages.en')</a></li>
</ul>

<ul id="account" class="dropdown-content">
    <li><a href="{{ route('user.organisations.index') }}">Organisations</a></li>
    <li><a href="{{ route('user.invitations.index') }}">Invitations</a></li>
    <li><a href="{{ route('logout') }}">Uitloggen</a></li>
</ul>
