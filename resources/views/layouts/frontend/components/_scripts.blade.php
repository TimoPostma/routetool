<script src="{{ 'https://maps.googleapis.com/maps/api/js?key=' . env('GOOGLE_HOST_KEY') }}" async defer></script>

<script type="text/javascript" src="{{ mix('js/app.js') }}"></script>
<script type="text/javascript">
    if('{{ Session::has('error') }}') {
        M.toast({html: '{{ Session::get('error') }}', classes: 'red', displayLength: Infinity})
    }
    if('{{ Session::has('warning') }}') {
        M.toast({html: '{{ Session::get('warning') }}', classes: 'orange', displayLength: Infinity})
    }
    if('{{ Session::has('success') }}') {
        M.toast({html: '{{ Session::get('success') }}', classes: 'green', displayLength: Infinity})
    }
</script>
@yield('extraJS')
