<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    @include('layouts.frontend.components._head')
</head>

<body>

<header>
    @include('layouts.frontend.components._nav')
</header>

<main>
    <div id="app">
        @yield('content')
    </div>
</main>

<footer class="page-footer">
    @include('layouts.frontend.components._footer')
</footer>

@include('layouts.frontend.components._scripts')
</body>
</html>
