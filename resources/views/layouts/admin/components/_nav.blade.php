<nav>
    <div class="nav-wrapper">

        <a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
        <ul class="right hide-on-med-and-down">
            @guest()
                <li><a href="{{ route('register') }}">@lang('nav.register')</a></li>
                <li><a href="{{ route('login') }}">@lang('nav.login')</a></li>
            @endguest
                <li><a class="dropdown-trigger" href="#!" data-target="language">@lang('nav.language')<i class="material-icons right">arrow_drop_down</i></a></li>
        </ul>
    </div>
</nav>

<ul class="sidenav sidenav-fixed" id="mobile-demo">
    <li>
        <div class="center">
            <a class="black-text" href="{{ route('admin.dashboard.index') }}"><h5>Transport Route Tool</h5></a>
        </div>
    </li>
    <li><a href="{{ route('admin.users.index') }}">Gebruikers<i class="material-icons">people_outline</i><span class="badge">{{ $userCount }}</span></a></li>
    <li><a href="{{ route('admin.logs.index') }}"><i class="material-icons">info_outline</i>Logs</a></li>
    <li><a href="{{ route('admin.logout') }}"><i class="material-icons">exit_to_app</i>Uitloggen</a></li>
</ul>

<ul id="language" class="dropdown-content">
    <li><a href="{{ route('locale', ['locale' => 'nl']) }}">@lang('nav.languages.nl')</a></li>
    <li><a href="{{ route('locale', ['locale' => 'en']) }}">@lang('nav.languages.en')</a></li>
</ul>
<ul id="language-mobile" class="dropdown-content">
    <li><a href="{{ route('locale', ['locale' => 'nl']) }}">@lang('nav.languages.nl')</a></li>
    <li><a href="{{ route('locale', ['locale' => 'en']) }}">@lang('nav.languages.en')</a></li>
</ul>
