<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    @include('layouts.admin.components._head')
</head>

<body>

<header>
    @include('layouts.admin.components._nav')
</header>

<main>
    <div id="app">
        @yield('content')
    </div>
</main>

<footer class="page-footer">
    @include('layouts.admin.components._footer')
</footer>

<!--JavaScript at end of body for optimized loading-->
@include('layouts.admin.components._scripts')
</body>
</html>
