<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    @include('layouts.organisation.components._head')
</head>

<body>

<header>
    @include('layouts.organisation.components._nav')
</header>

<main>
    <div id="app">
        @yield('content')
    </div>
</main>

<footer class="page-footer">
    @include('layouts.organisation.components._footer')
</footer>

<!--JavaScript at end of body for optimized loading-->
@include('layouts.organisation.components._scripts')
</body>
</html>
