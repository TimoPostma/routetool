
<html lang="{{ app()->getLocale() }}">
<head>
    @include('layouts.user.components._head')
</head>

<body>

<header>
    @include('layouts.user.components._nav')
</header>

<main>
    <div id="app">
        @yield('content')
    </div>
</main>

<footer class="page-footer">
    @include('layouts.user.components._footer')
</footer>

@include('layouts.frontend.components._scripts')
</body>
</html>
