<nav>
    <div class="nav-wrapper">

        <a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
        <ul class="right hide-on-med-and-down">
            @guest()
                <li><a href="{{ route('register') }}">@lang('nav.register')</a></li>
                <li><a href="{{ route('login') }}">@lang('nav.login')</a></li>
            @endguest
                <li><a class="dropdown-trigger" href="#!" data-target="language">@lang('nav.language')<i class="material-icons right">arrow_drop_down</i></a></li>
               

        </ul>
    </div>
</nav>

<ul class="sidenav sidenav-fixed" id="mobile-demo">
    <div class="logo">
        <img width="150px" height="150px" src="/assets/logo.png">
    </div>
    <a class="menu-button btn" href="{{ route('user.organisations.index') }}">@lang('nav.organisations')<i class="material-icons icon">business</i></a>
    <a class="menu-button btn" href="{{ route('user.organisations.index') }}">@lang('nav.settings')<i class="material-icons icon">settings</i></a>
    <a class="menu-button btn" href="{{ route('user.invitations.index') }}">@lang('nav.invitations')<i class="material-icons icon">person_pin_circle</i></a>
    <a class="menu-button btn" href="{{ route('logout') }}" class="organisation">@lang('nav.logout')<i class="material-icons icon">exit_to_app</i></a>
    
</ul>

<ul id="language" class="dropdown-content">
    <li><a href="{{ route('locale', ['locale' => 'nl']) }}">@lang('nav.languages.nl')</a></li>
    <li><a href="{{ route('locale', ['locale' => 'en']) }}">@lang('nav.languages.en')</a></li>
</ul>
<ul id="language-mobile" class="dropdown-content">
    <li><a href="{{ route('locale', ['locale' => 'nl']) }}">@lang('nav.languages.nl')</a></li>
    <li><a href="{{ route('locale', ['locale' => 'en']) }}">@lang('nav.languages.en')</a></li>
</ul>

<ul id="account" class="dropdown-content">
    <li><a href="{{ route('user.organisations.index') }}">@lang('nav.organisations')</a></li>
    <li><a href="{{ route('user.invitations.index') }}">@lang('nav.invitations')</a></li>
    <li><a href="{{ route('logout') }}">@lang('nav.logout')</a></li>
</ul>


