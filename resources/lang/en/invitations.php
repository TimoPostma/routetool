<?php

return [
  'invite_users' => 'Invite users',
  'invite_a_user' => 'Invite a user',
    'unknown_link' => 'Link bestaat niet',
    'expired_link' => 'Link is verlopen'
];
