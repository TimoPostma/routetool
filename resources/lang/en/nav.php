<?php

return [
    'login' => 'Login',
    'register' => 'Register',
    'language' => 'Language',
    'languages' => [
        'nl' => 'Dutch',
        'en' => 'English'
    ],
    'addresses'=>'addresses',
    'planboards'=>'Planboards',
    'templates'=>'Templates',
    'users' => 'Users',
    'settings' => 'Settings',
    'organisations' => 'Organisations',
    'transports'=>'Transport methods',
    'invitations' => 'Invitations',
    'logout' => 'Logout',
    'back' => 'Back'
];
