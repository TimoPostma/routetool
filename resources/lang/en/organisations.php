<?php

return [
    'organisations' => 'Organisations',
    'index'=>'Organisations',
    'create'=>'Create organisation',
    'edit'=>'Edit organisation',
    'show'=>'Organisation',
    'settings'=>'Organisation settings',
    'organisations_description' => 'Below you can add organisations and invite users',
    'edit_organisation_description' => 'Below you can update the name and description of the organisation',
    'add_organisation' => 'Add Organisation',
    'edit_organisation' => 'Update Organisation',
    'no_organisations_added' => 'No organisations added',
    'name' => 'Name',
    'description' => 'Description',
    'created_at' => 'Created at',
    'show' => 'Show',
    'planning_page_explain' => 'A Planboard can be used to create routes with the directions on Google Maps and calculate the expected travel time.',


    'users_explain' => 'Here you give your employees access to this application via email.',

    'create_organisation' => 'Create organisation',
    'create_organisation_description' => 'Make an organisation and add settings to it',
    'save' => 'Save',

    'planning_page' => 'Planboards',
    'planning_page_description' => 'Below you will find all your planboards that belong to',
    'no_plannings_added' => 'No schedules added',
    'add_planning' => 'Add a Planboard',
    'edit_planning' => 'Edit Planboard',

    'settings_page' => 'Settings page',

    'users' => 'Users',
    'users_description' => 'Below are all users part of your organisation',
    'invite_user' => 'Invite a user',
    'no_users_added' => 'No users added to the organisation yet',

    'import_settings' => 'Import settings',
    'no_import_settings_added' => 'No settings added yet. Importing will use the default setting.',
    'create_import_settings' => 'Create import settings',

    "address_header" => "Address header",
    "place_header" => "Place header",
    "period_header" => "Period header",
    "transport_header" => "Transport header",
    "overhead_time_header" => "Overhead time header",
    "item_time_header" => "Item time header",
    "item_header" => "Item header",
    "save_settings" => "Save settings",

    "no_permission" => "You have insuficcient permissions to view this page",
    "something_went_wrong" => "Something went wrong, try again later",
    "organisation_added" => "Organisation successfully added",
    "invited_self" => "You can not invite yourself",
    "could_not_save" => "Could not save import settings",

    "email_not_sent" => "Email could not be sent to the user. Try again later",
    "user_added" => "User has been added to the organisation",
    "user_already_added" => "User is already part of the organisation",
    "link_expired" => "Link has expired"
];
