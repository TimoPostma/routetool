<?php
return [
    'header' => 'Templates',

    'subheader' => 'All templates that belong to your organisation',
    'no_templates_added'=>'This organisation has no templates yet',

    'explain' => "You can save a Planboard as a template so you don't have create one again from scratch."

];
