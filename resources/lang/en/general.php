<?php
return [
'name'=>'Name',
'description'=>'Description',
'label'=>'Label',
'street'=>'Street',
'zipcode'=>'Zipcode',
'city'=>'City',
'country'=>'Country',
'edit'=>'Edit',
'delete'=>'Delete',
'created_at'=>'Created',
'show'=>'Show',
'method'=>'Method',
'speed'=>'Speed'

];