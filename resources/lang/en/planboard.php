<?php
return[
    'new' => 'A new planboard has been added',
    'add_planning' => 'Add a Planboard',
    'create'=>'Add planboard',
    'edit'=>'Aanpassen planboard',
    'show'=>'Planboard',
    'cellempty'=>'The selected cell has no locations, could not make pdf.'
    
];