<?php

return [
    'guest_login_message' => 'You need to be logged in to use the tool',
    'trt_description' => 'For planning routes.',
    'login' => 'Login',
    'register' => 'Register',
    'goto_organisations' => 'Go to organisations'
];
