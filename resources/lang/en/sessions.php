<?php

return [
    'locale_not_exist' => 'This locale does not exist',
    'link_expired' => 'Link has expired',
    'password_reset_email_sent' => 'Password reset email has been sent',
    'password_updated' => 'Password has been updated',
    'invalid_credentials' => 'Cannot login with these credentials',
    'user_cannot_added' => 'User can not be added. Try again later',
    'activation_mail_cannot_send' => 'Activation email could not be sent',
    'user_made' => 'User has been made, check your email to activate the user Also check your SPAM email',
    'user_activated' => 'User is activated. You can now login',
    'user_not_activated' => 'User is not activated yet. Check your email.',
    'error_sending_mail' => 'Error sending email. try again later.',
];
