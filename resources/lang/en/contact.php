<?php
return [
    'title' => 'Contact',
    'contact_information' => 'Contact information',
    'contact_information_description' => 'You can find our contact information below. You can also send us a message via the contact form.',
    'address' => 'Address',
    'contact_form_title' => 'Contact form',
];
