<?php
return [
    'page' => 'Addresses',
    'explain' => 'Here you can save an address for use on the Planboard',
    'edit_page'=>'Edit the address',
    'create_page'=>'Create a new address',
    'page_description' => 'All addresses that belong to your organisation',
    'button'=>[
        'add'=>'Add new address',
        'edit'=>'Edit addres'
    ],
    'add'=>'Add new address',
    'empty'=>'Your organisation has no addresses yet',



];
