<?php
return [
    'page' => 'Transports',
    'explain' => 'Here you can save an transport for use on the Planboard',
    'edit_page'=>'Edit the transport',
    'create_page'=>'Create a new transport',
    'page_description' => 'All Transports that belong to your organisation',
    'button'=>[
        'add'=>'Add new transport',
        'edit'=>'Edit addres'
    ],
    'add'=>'Add new transport',
    'empty'=>'Your organisation has no Transports yet',
    'transport'=>'Transport'

];
