<?php
return [
'name'=>'Naam',
'description'=>'Omschrijving',
'label'=>'Label',
'street'=>'Straat',
'zipcode'=>'Postcode',
'city'=>'Stad',
'country'=>'Land',
'edit'=>'Pas aan',
'delete'=>'Verwijder',
'created_at'=>'Aanmaak datum',
'show'=>'Toon',
'method'=>'Methode',
'speed'=>'Snelheid'

];