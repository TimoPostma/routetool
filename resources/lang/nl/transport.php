<?php
return [
    'page' => 'Transporten',
    'explain' => 'Hier kunt u een transport opslaan om te gebruiken op het Planboard.',
    'edit_page'=>'Pas transport aan',
    'create_page'=>'Voeg nieuw transport toe',
    'page_description' => 'Alle transporten die bij uw organisatie horen',
    'button'=>[
        'add'=>'Voeg transport toe',
        'edit'=>'Pas transport toe'
    ],
    'add'=>'Voeg transport toe',
    'empty'=>'Uw organisaties heeft nog geen transporten',
    'transport'=>'transport'


];