<?php

return [
    'login' => 'Inloggen',
    'register' => 'Registreren',
    'language' => 'Taal',
    'languages' => [
        'nl' => 'Nederlands',
        'en' => 'Engels'
    ],

    'users' => 'Gebruikers',
    'settings' => 'Instellingen',
    'organisations' => 'Organisaties',
    'transports'=>'Transportmethoden',
    'invitations' => 'Uitnodigingen',
    'logout' => 'Uitloggen',
    'back' => 'terug'
];
