<?php

return [
    'locale_not_exist' => 'Deze locale bestaat niet',
    'link_expired' => 'Link is verlopen',
    'password_reset_email_sent' => 'Password reset email is verstuurd',
    'password_updated' => 'Wachtwoord is gewijzigd',
    'invalid_credentials' => 'Kan niet met deze gegevens inloggen',
    'user_cannot_added' => 'Gebruiker kon niet worden toegevoegd. Probeer het later opnieuw.',
    'activation_mail_cannot_send' => 'Activatiemail kon niet verstuurd worden',
    'user_made' => 'Een gebruiker is gemaakt. Ga naar je inbox om de gebruiker te activeren, en kijk indien nodig tussen spam.',
    'user_activated' => 'Gebruiker is geactiveerd. Je kan nu inloggen.',
    'user_not_activated' => 'Gebruiker is nog niet geactiveerd. Check je email',
    'error_sending_mail' => 'Er ging iets fout bij het sturen van een email. probeer het later opnieuw.',
];
