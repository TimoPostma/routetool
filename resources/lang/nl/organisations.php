<?php

return [
    'organisations' => 'Organisaties',
    'create'=>'Organisatie aanmaken',
    'edit'=>'Organisatie aanpassen',
    'show'=>'Organisatie',
    'settings'=>'Organisatie instellingen',
    'organisations_description' => 'Hieronder kan je organisaties toevoegen en gebruikers uitnodigen',
    'add_organisation' => 'Voeg organisatie toe',
    'no_organisations_added' => 'Geen organisaties toegevoegd',
    'name' => 'Naam',
    'description' => 'Beschrijving',
    'created_at' => 'Toevoevoegd op',
    'show' => 'Toon',

    'users_explain' => 'Hier kunt u uw medewerkers toegang geven tot deze applicatie via email.',

    'create_organisation' => 'Voeg organisatie toe',
    'create_organisation_description' => 'Maak een organisatie en voeg er instellingen aan toe',
    'save' => 'Opslaan',

    'planning_page_explain' => 'Op een Planboard kunt u verschillende routes toevoegen, de route zien op google maps en de verwachte reistijd inzien.',

    'planning_page' => 'Planboardpagina',
    'planning_page_description' => 'Hieronder vind u al uw planboards',
    'no_plannings_added' => 'Geen planboards toegevoegd',
    'add_planning' => 'Voeg planboard toe',

    'settings_page' => 'Instellingspagina',

    'users' => 'Gebruikers',
    'users_description' => 'Hieronder vind u alle gebruikers in uw organisatie',
    'invite_user' => 'Nodig een gebruiker uit',
    'no_users_added' => 'Nog geen gebruikers toegevoegd aan de organisatie',

    'import_settings' => 'Import instellingen',
    'no_import_settings_added' => 'Nog geen instellingen toegevoegd. Het importeren gebruikt de standaard instelling',
    'create_import_settings' => 'Maak importinstellingen',

    "address_header" => "Adresheader",
    "place_header" => "Plaatsheader",
    "period_header" => "Periode header",
    "transport_header" => "Transport header",
    "overhead_time_header" => "Overhead tijd header",
    "item_time_header" => "Item tijd header",
    "item_header" => "Item header",
    "save_settings" => "Sla instellingen op",

    "no_permission" => "U heeft geen toegang om deze pagina te bekijken",
    "something_went_wrong" => "Er ging iets fout. Probeer het later opnieuw",
    "organisation_added" => "Organisatie succesvol toegevoegd",
    "invited_self" => "Je kan jezelf niet uitnodigen",
    "could_not_save" => "Kon importinstellingen niet opslaan",

    "email_not_sent" => "Email kon niet verstuurd worden. Probeer het later nog eens",
    "user_added" => "Gebruiker is toegevoegd aan de organisatie",
    "user_already_added" => "Gebruiker maakt al deel uit van de organisatie",
    "link_expired" => "Link is verlopen"
];
