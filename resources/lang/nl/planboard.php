<?php
return[
    'new' => 'Het planboard is aangemaakt',
    'add_planning' => 'Voeg planboard toe',
    'create'=>'Planboard toevoegen',
    'edit'=>'Planboard aanpassen',
    'show'=>'Planboard',
    'cellempty'=>'De geselecteerde tegel heeft geen locaties, Pdf kon niet gemaakt worden.'
];