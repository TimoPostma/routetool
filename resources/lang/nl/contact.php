<?php
return [
    'title' => 'Contact',
    'contact_information' => 'Contactgegevens',
    'contact_information_description' => 'Hieronder vindt u onze contactgegevens. U kunt ons ook een bericht sturen via het contactformulier',
    'address' => 'Adres',
    'contact_form_title' => 'Contactformulier',


];
