<?php
return [
    'page' => 'Adressen',
    'explain' => 'Hier kunt u een adres opslaan om te gebruiken op het Planboard.',
    'edit_page'=>'Pas adres aan',
    'create_page'=>'Voeg nieuw adres toe',
    'page_description' => 'Alle adressen die bij uw organisatie horen',
    'button'=>[
        'add'=>'Voeg adres toe',
        'edit'=>'Pas adres toe'
    ],
    'add'=>'Voeg adres toe',
    'empty'=>'Uw organisaties heeft nog geen adressen',



];
