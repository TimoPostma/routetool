<?php

return [
    'guest_login_message' => 'Om de tool te kunnen gebruiken moet je inloggen',
    'trt_description' => 'Voor het plannen van routes.',
    'login' => 'Inloggen',
    'register' => 'Registreren',
    'goto_organisations' => 'Ga naar organisaties'
];
