<?php
return [
    'header' => 'Templates',
    'subheader' => 'Alle templates die bij jouw  organisatie horen',
    'no_templates_added'=>'Deze organisatie heeft nog geen templates',

    'explain' => 'Een template is een blauwdruk van een Planboard.'
];
