# Route-tool

The Route-tool is a tool used to manage transportation. The route-tool can accommodate all your organisations in one tool. Every organisation can have unlimited planboards Where you can add the addresses, transports and dates.

The planboard can be exported in JSON format that is compatible with "Produvar OVS".

## usage

1. make a planboard yourself
   You can start a planboard from nothing by adding an organisation and adding a new planboard to it. You can do this by choosing the option "start from scratch". From here you can add `transports`, `periods` and `addresses` with their `orders`.
   Everything you change will be saved automaticaly.
2. start a planboard from a template
   You can also start a planboard from a premade template. You can do this by choosing the option "start from template". You can choose a template to prefill your planboard with the right periods.
3. import a planboard from OVS
   You can also start a planboard with the data you get from the ovs. choose import JSON to import the data from ovs

## licence

Produvar 2022
