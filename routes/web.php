<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'auth'], function () {
    Route::name('pages.index')->get('/', 'PagesController@getIndexPage');
    Route::name('excel.template')->get('excelTemplate', 'User\TemplateController@getExcelTemplate');
});

Route::group(['middleware' => ['locale', 'activated']], function () {

    //Route to login and redirect to the planningpage
    Route::name('planning.login')->get('planning/login/{userId}/{organisationId}/{planningId}', 'Authentication\LoginController@planningLogin');

    Route::name('pages.special')->get('kswqjdiwdqkdddxdew', 'PagesController@getSpecialPage');
    Route::name('pages.contact')->match(['get', 'post'], 'contact', 'PagesController@contact');
    Route::name('pages.index')->get('/', 'PagesController@getIndexPage');
    Route::get('distance', 'PagesController@getShortestRoute');
    Route::get('duration', 'PagesController@getShortestDuration');
    Route::name('locale')->get('locale/{locale}', 'LocaleController@setLocale');
    Route::group(['middleware' => 'guest'], function () {
        Route::name('register')->match(['get', 'post'], 'register', 'Authentication\RegisterController@register');
        Route::name('login')->match(['get', 'post'], 'login', 'Authentication\LoginController@authenticate');
        Route::name('login.with-url')->get('login-with-url', 'Authentication\\LoginController@loginWithUrl');
        Route::name('activate-user')->get('activate-user/{id}', 'Authentication\RegisterController@activateUser');
        Route::name('forget-password')->match(['get', 'post'], 'forget-password', 'Authentication\ForgetPasswordController@forgetPassword');
        Route::name('reset-password')->match(['get', 'post'], 'reset-password/{email}', 'Authentication\ForgetPasswordController@resetPassword');
        Route::name('organisation.invite')->match(['get', 'post'], 'invite-organisation', 'User\OrganisationController@makeUserAndAttachToOrganisation');

        //Invitation  links
        Route::name('invitation')->match(['get', 'post'], 'invitation', 'InvitationController@invitedUser');

    });

    Route::group(['prefix' => 'user', 'middleware' => 'auth'], function () {

        Route::name('logout')->get('logout', 'Authentication\LogoutController@logout');

        //The user invitation routes
        Route::group(['prefix' => 'invitations'], function () {
            Route::name('user.invitations.index')->get('/', 'User\InvitationController@getIndexPage');
            Route::name('invitation-url')->get('invitation-url', 'User\InvitationController@generateInvitationUrl');
            Route::name('invite-user-by-email')->post('invite-user-by-email', 'User\InvitationController@inviteUserByEmail');
            Route::name('invitation-link-expired')->get('invitation-link-expired', 'User\InvitationController@checkIfGeneratedLinkHasExpired');
        });

        // The organisation routes
        Route::group(['prefix' => 'organisations'], function () {
            Route::name('user.organisations.index')->get('/', 'User\OrganisationController@getIndexPage');
            Route::name('user.organisations.create')->get('create', 'User\OrganisationController@create');
            Route::name('user.organisations.edit')->get('edit/{id}', 'User\OrganisationController@getEditPage');
            Route::name('user.organisations.store')->post('store', 'User\OrganisationController@store');
            Route::name('user.organisations.save')->post('save/{id}', 'User\OrganisationController@save');
            Route::name('user.organisations.show')->get('{id}', 'User\OrganisationController@show');
            Route::name('user.organisations.settings')->get('settings/{id}', 'User\OrganisationController@settings');
            Route::name('user.organisations.users')->get('{id}/users', 'User\OrganisationController@getUsers');
            Route::name('user.organisations.invite')->post('invite/{id}', 'User\OrganisationController@inviteUserToOrganisation');

            Route::group(['prefix' => '{id}/templates'], function () {
                Route::name('user.templates.create')->get('create', 'User\TemplateController@create');
                Route::name('user.templates.show')->get('{templateId}/show', 'User\TemplateController@show');
                Route::name('user.templates.index')->get('/', 'User\TemplateController@getIndexPage');
                Route::name('user.templates.fetch')->get('fetch', 'User\TemplateController@getTemplates');

            });

            //Import Settings routes
            Route::group(['prefix' => '{id}/import-settings'], function () {
                Route::name('import.settings.index')->get('/', 'User\ImportSettingsController@getIndexPage');
                Route::name('import.settings.create')->get('create', 'User\ImportSettingsController@create');
                Route::name('import.settings.store')->post('store/', 'User\ImportSettingsController@store');

            });



            //Planning routes
            Route::group(['prefix' => '{id}/plannings'], function () {
                Route::name('organisation.planning.export')->get('{planningId}/excel/{transportKey}/{periodKey}', 'User\PlanningController@exportCellToExcel');
                Route::name('organisation.planning.exportall')->get('{planningId}/excel', 'User\PlanningController@exportPlanboardToExcel');
                Route::name('organisation.planning.pdfs')->get('{planningId}/pdf', 'User\PlanningController@exportPlanboardToPdf');
                Route::name('organisation.planning.pdf')->get('{planningId}/pdf/{transportKey}/{periodKey}', 'User\PlanningController@exportCellToPdf');
                Route::name('organisation.planning.create')->get('create', 'User\PlanningController@create');
                Route::name('organisation.planning.edit')->get('{planningId}/edit', 'User\PlanningController@getEditPage');
                Route::name('organisation.planning.update')->post('{planningId}/update', 'User\PlanningController@update');
                Route::name('organisation.planning.apply')->post('{planningId}/apply/{templateId}', 'User\TemplateController@apply');
                Route::name('organisation.planning.store')->post('store', 'User\PlanningController@store');
                Route::name('organisation.planning.delete')->post('delete', 'User\PlanningController@deleteRoute');
                Route::name('organisation.planning.show')->get('{planningId}/{start?}', 'User\PlanningController@show');

            });
            Route::group(['prefix' => '{id}/addresses'], function () {
                Route::name('organisation.address.index')->get('index', 'User\AddressController@getIndexPage');
                Route::name('organisation.address.create')->get('create', 'User\AddressController@create');
                Route::name('organisation.address.edit')->get('edit/{addressId}', 'User\AddressController@edit');
                Route::name('organisation.address.save')->post('save', 'User\AddressController@save');
                Route::name('organisation.address.store')->post('store', 'User\AddressController@store');
                Route::name('organisation.address.favorite')->post('favorite', 'User\AddressController@favorite');
                Route::name('organisation.address.list')->get('list', 'User\AddressController@list');
                Route::name('organisation.address.qeuelist')->get('qeuelist', 'User\AddressController@qeuelist');
                Route::name('organisation.address.qeueliststore')->post('qeuelist/store', 'User\AddressController@storeqeuelist');
                Route::name('organisation.address.delete')->post('delete', 'User\AddressController@delete');
            });
            Route::group(['prefix' => '{id}/transports'], function () {
                Route::name('organisation.transport.index')->get('index', 'User\TransportController@getIndexPage');
                Route::name('organisation.transport.create')->get('create', 'User\TransportController@create');
                Route::name('organisation.transport.edit')->get('edit/{transportId}', 'User\TransportController@edit');
                Route::name('organisation.transport.save')->post('save', 'User\TransportController@save');
                Route::name('organisation.transport.store')->post('store', 'User\TransportController@store');
                Route::name('organisation.transport.favorite')->post('favorite', 'User\TransportController@favorite');
                Route::name('organisation.transport.list')->get('list', 'User\TransportController@list');
                Route::name('organisation.transport.delete')->post('delete', 'User\TransportController@delete');
            });

        });

    });


    Route::group(['prefix' => 'admin', 'middleware' => 'guest:admin'], function () {
        Route::name('admin.login')->match(['get', 'post'], 'login', 'Admin\LoginController@login');
    });

    Route::group(['prefix' => 'admin', 'middleware' => ['auth:admin', 'apitoken']], function () {
        Route::name('admin.dashboard.index')->get('/', 'Admin\DashboardController@getIndexPage');
        Route::name('admin.logout')->get('logout', 'Admin\LogoutController@logout');

        //Logs routes
        Route::group(['prefix' => 'logs'], function () {
           Route::name('admin.logs.index')->get('/', 'Admin\LogController@getIndexPage');
        });

        //User routes
        Route::group(['prefix' => 'users'], function () {
            Route::name('admin.users.index')->get('/', 'Admin\UserController@getIndexPage');
        });
    });
});
