<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login-with-bearer-token', 'Api\\AuthenticationController@authenticateWithBearerToken');
Route::post('save-address-hash', 'Api\\AddressController@saveHash');
Route::get('get-lat-long', 'Api\\AddressController@getLatLong');

Route::post('test/{planningId}', 'Api\ExportController@exportToExcel');


Route::group(['middleware' => 'auth:api'], function () {
    Route::post('calculate-distance', 'Api\GoogleMatrixController@getDistances');
    Route::post('upload-csv', 'Api\CsvController@uploadCsv');
    Route::post('import-excel', 'Api\ImportExcelController@convertExcelToArray');
    Route::get('download-example', 'Api\ImportExcelController@downloadExcelExample');
    Route::post('store-planning', 'Api\PlanningController@savePlanning');
    Route::post('store-template', 'Api\TemplateController@saveTemplate');
    Route::get('get-route/{organisationId}/{planningId}', 'Api\PlanningController@getRouteData');
    Route::get('import-excel-example', 'Api\ImportExcelController@convertExcelExampleDataToPlanning');
    Route::post('synchronize-ovs', 'Api\PlanningController@synchronizePlanning');
    Route::post('send-documenttool', 'Api\DocumenttoolController@sendToDocumenttool');
});

Route::group(['prefix' => 'v1' , 'middleware' => 'auth:passport_api'], function () {
    Route::get('organisations/{organisationId}/import-settings/', 'Api\ImportSettingController@getImportSettingsByOrganisation');
    Route::get('organisations/{organisationId}/import-settings/{importSettingId}', 'Api\ImportSettingController@getImportSettingDetails');
    Route::post('organisations/{organisationId}/import-settings/{importSettingId}/plannings/{planningId}/import-to-planning', 'Api\ImportExcelController@ImportExcelToPlanning');
    Route::get('organisations', 'Api\OrganisationController@getOrganisations');
    route::get('organisations/{organisationId}', 'Api\OrganisationController@getOrganisation');
    Route::get('organisations/{organisationId}/plannings' ,'Api\PlanningController@getOrganisationPlannings');
});

Route::group(['prefix' => 'v2/'], function() {
	Route::post('login-with-bearer-token', 'Api\\AuthenticationController@authenticateWithBearerToken');
});

/**
 * Routes available to external services
 */
Route::group(['prefix' => 'v2/organisations/{organisationId}/'], function() {
        Route::post('planboards/{planboardId}/synchronize-ovs', 'Api\PlanningController@synchronizeWithOvs');
	Route::get('planboards/{planboardId}', 'Api\PlanningController@getOrganisationPlanboard');
	Route::get('planboards', 'Api\PlanningController@getOrganisationPlanboards');
	Route::post('planboards/{planboardId}/locations/add', 'Api\PlanningController@addOrganisationPlanboardLocation');
	Route::post('planboards/{planboardId}/update_order_transport', 'Api\PlanningController@updateOrganisationPlanboardOrderTransport');
	Route::post('planboards/{planboardId}/update_order_period', 'Api\PlanningController@updateOrganisationPlanboardOrderPeriod');
	Route::post('planboards/planboard/create', 'Api\PlanningController@createOrganisationPlanboard');
});

Route::group(['middleware' => 'auth:admin_api'], function () {
    Route::get('logs', 'Api\LogController@getLogs');
});
