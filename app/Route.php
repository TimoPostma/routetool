<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Route extends Model
{
    public function addRoute($hash, $origin, $destination, $distance, $duration)
    {
        $this->hash = $hash;
        $this->origin = $origin;
        $this->destination = $destination;
        $this->distance = $distance;
        $this->duration = $duration;
        $this->save();
    }

    public function transport()
    {
        return $this->belongsTo('App\Transport');
    }

    public function getDestinations($origin)
    {
        return $this->where(
            [
                ['origin', '=', $origin], ['destination', '!=', [$origin]]
            ])->get();
    }

    public function getOrigins($destination, $origins)
    {
        return $this->where([
            ['destination', $destination]
        ])->get();
    }
}
