<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invitation extends Model
{
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function storeInvitation($userId, $signature, $expiration_date, $url, $timestamp)
    {
        $this->url = $url;
        $this->user_id = $userId;
        $this->expiration_date = $expiration_date;
        $this->signature = $signature;
        $this->timestamp = $timestamp;
        $this->save();
    }

    public function updateInvitation($invitation, $signature, $expiration_date, $url, $timestamp)
    {
        $invitation->signature = $signature;
        $invitation->expiration_date = $expiration_date;
        $this->url = $url;
        $this->timestamp = $timestamp;
        $this->update();
    }

    public function checkIfSignatureExists($signature)
    {
        return $this->where('signature', $signature)->exists();
    }

    public function getInvitation($userId)
    {
        return $this->where('user_id', $userId)->first();
    }

    public function getInvitationByUrl($url)
    {
        return $this->where('url', $url)->first();
    }

    public function checkIfTimeStampAndSignatureAreValid($signature, $timestamp, $userId)
    {
        return $this->where('signature', $signature)->where('timestamp', $timestamp)->where('user_id', $userId)->exists();
    }
}
