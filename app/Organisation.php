<?php

namespace App;

use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Model;

class Organisation extends Model
{
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function users()
    {
        return $this->belongsToMany('App\User', 'organisation_user');
    }

    public function importSettings()
    {
        return $this->hasMany('App\ImportSettings');
    }

    public function plannings()
    {
        return $this->hasMany('App\Planning');
    }

    public function addresses()
    {
        return $this->hasMany('App\Address');
    }

    public function invitations()
    {
        return $this->hasMany('App\InviteOrganisation');
    }

    public function getOwnerOrganisationsByUserId($userId)
    {
        return $this->where('user_id', $userId)->get();
    }

    public function getOrganisationById($id)
    {
        return $this->where('id', $id)->first();
    }

    public function storeOrganisation($name, $description, $userId)
    {
        $this->name = $name;
        $this->description = $description;
        $this->user_id = $userId;
        $this->save();
    }
    public function saveOrganisation($organisation)
    {   $this->organisation = $organisation;
        $this->organisation->update();
    }

    public function isPartOfTheOrganisation($user)
    {
        return $this->users()->where('user_id', $user->id)->exists();
    }

}
