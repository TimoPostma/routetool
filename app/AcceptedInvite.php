<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AcceptedInvite extends Model
{
    public function storeAcceptedInvite($userId, $invitationId)
    {
        $this->user_id = $userId;
        $this->invitation_id = $invitationId;
        $this->save();
    }
}
