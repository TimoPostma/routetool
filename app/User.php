<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function organisations()
    {
        return $this->hasMany('App\Organisation');
    }

    public function invitation()
    {
        return $this->hasOne('App\Invitation');
    }

    public function organisationUser()
    {
        return $this->belongsToMany('App\Organisation', 'organisation_user');
    }

    /**
     * Stores the user into the database
     * @param $request object with the request data for the user
     * @param bool $activated
     * @return $this the user object
     */


    public function addUser($request, $activated = false)
    {
        $this->name = $request->name;
        $this->email = $request->email;
        $this->password = bcrypt($request->password);
        $this->api_token = Str::random(80);
        $this->activated = $activated;
        $this->save();
        return $this;
    }

    /**
     * Get the user by his id
     * @param $id int id of the user
     * @return mixed returns the user object or null
     */

    public function getUserById($id)
    {
        return $this->where('id', $id)->first();
    }

    /**
     * Get the user by his email
     * @param $email string Contains the email of the user
     * @return mixed returns the user object or null
     */

    public function getUserByEmail($email)
    {
        return $this->where('email', $email)->first();
    }
}
