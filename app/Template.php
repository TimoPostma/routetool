<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Template extends Model
{
    use HasFactory;
    public function organisation(){

    }

    public function getTemplates(){
        return $this->where('user_id', $user->id)->get();
    }
    public function getTemplateById($id){
        return $this->select('*')->where('id', $id)->first();
    }
    public function getTemplatesByOrganisationId($id){
        return $this->where('organisation_id',$id)->get();
    }
    public function storeTemplate($label ,$description, $organisationId,$templatedata)
    {
        $this->label = $label;
        $this->description = $description;
        $this->organisation_id = $organisationId;
        $this->templatedata = $templatedata;
        $this->save();
        return $this;
    }
}
