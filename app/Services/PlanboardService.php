<?php

namespace App\Services;
use App\Planning;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class PlanboardService
{
    public static function set_default_itemtime(Planning $planning,$itemtime){
       
        if ($planning->is_locked_set){
            return "planboard is locked";
        }
        
        $routedata = $planning->routedata;
        //Arr::set($routedata,"default.overhead_time",$overhead);  
        Arr::set($routedata,"default.item_time",$itemtime);
        $planning->routedata = $routedata;
        // var_dump($planning);
        // die();
       // $cell['defaults']['item_time'] = $itemtime;

        return $planning;
    }
    public static function set_default_overheadtime(Planning $planning,$overheadtime){
       
        $routedata = $planning->routedata;
        //Arr::set($routedata,"default.overhead_time",$overhead);  
        Arr::set($routedata,"default.overhead_time",$overheadtime);
        $planning->routedata = $routedata;

        return $planning;
    }

}
