<?php

namespace App\Services;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use App\Transport;

class PlanningBoardService
{


    /**
     * Creates a planboard from the OVS data
     * @param array $transports Key-value mapping that maps key to name of transport (key is equal to label in OVS)
     * @param array $periods
     * @param array $ovsData
     * @param string $organisationId The id of the organisation (uuid)
     * @return array
     */
    public static function createPlanBoardData(array $transports, array $periods, array $ovsData, string $organisationId) : array
    {
        //@TODO check if orders are valid

        $periodKeys = [];

        $index = 0;
        $routeData = ['meta' => ['type' => 'planitems', 'version' => '1.0.0'], 'startlocation' => ['label' => 'no_startlocation'], 'cells' => []];
        foreach ($transports as $tIdx => $transport) {
            $currenTransport = self::getOrCreateTransport($transport, $organisationId);
            foreach ($periods as $pIdx => $period) {
                if(!array_key_exists($pIdx, $periodKeys)) {
                    $periodKeys[$pIdx] = self::generatePeriodKey();
                }

                $period['key'] = $periodKeys[$pIdx];

                $routeData['cells'][$index]['transport'] = $currenTransport;

                $routeData['cells'][$index]['period'] = $period;
                if(empty(self::addOrderDataToLocation($ovsData, $period['date'], $transport))) {
                    $routeData['cells'][$index]['locations'] = [];
                } else {
                    $routeData['cells'][$index]['locations'][] = self::addOrderDataToLocation($ovsData, $period['date'], $transport);
                }

                $index++;
            }
        }

        return $routeData;
    }


    /**
     * Add the order data to the location
     * @param array $ovsData
     * @param string $period_date
     * @param string $transportLabel
     * @return array
     */
    private static function addOrderDataToLocation(array $ovsData, string $period_date, string $transportLabel) : array
    {
        $locations = [];

        //@TODO we need to check the order
        foreach ($ovsData as $ovsLevering) {
            if($ovsLevering['period_date'] !== $period_date || $ovsLevering['transport_label'] !== $transportLabel) {
                continue;
            }
            //Build the address and the orders
            $locations['address'] = self::makeAddress($ovsLevering);
            Log::debug(print_r($ovsLevering['orders'], true));
            $orders = json_decode($ovsLevering['orders'], true);
            $locations['orders'][] = $orders[0];
        }
        return $locations;
    }

    /**
     * Creates the address from the ovs data
     * @param $ovsLevering
     * @return array
     */
    private static function makeAddress($ovsLevering) : array
    {
        $address = [];
        $address['city'] = $ovsLevering['city'];
        $address['country'] = $ovsLevering['country'];
        $address['zipcode'] = $ovsLevering['zipcode'];
        $address['street'] = $ovsLevering['street'];
        $address['label'] = $ovsLevering['label'];
        return $address;
    }

    /**
     * Periods are already set, but still need to generate a index key
     * @return string
     */
    private static function generatePeriodKey() : string
    {
        return Str::random(5);
    }

    /**
     * Find a transport, if it does not exist, create it.
     * @param string $label The name of the transport to match on
     * @param string $organisationId The uuid of the organisation
     * @return array All properties of the transport
     */
    private static function getOrCreateTransport(string $label, $organisationId) : array 
    {

	$transport_record = new Transport();
	
	$transport_record = $transport_record->getTransportByLabel($label);
	if($transport_record === null){
		$transport = self::createTransport($label);

		// register in database, because it does not exist yet.
		$transport_record = new Transport();
		$transport_record->organisation_id = $organisationId;
		$transport_record->label = $label;
		$properties = [
                        'map' => true,
                        'showTime' => true,
                        'circular' => true,
                        'selected' => false,
                        'total_time' => 0,
                        'total_item_time' => 0,
                ];
		$transport_record->transportdata = json_encode($properties);
		$transport_record->save();

	} else {
		// process transport record
		$transportdata = json_decode($transport_record->transportdata, true);

		$transport = [];
		$transport['key'] = Str::random(5);
		$transport['label'] = $transport_record->label;

		// copy configurable properties
		$properties = [
			'map' => true, 
			'showTime' => true, 
			'circular' => true, 
			'selected' => false, 
			'total_time' => 0, 
			'total_item_time' => 0,
		];
		foreach($properties as $prop=>$default) {
			$transport[$prop] = Arr::get($transportdata, $prop, $default);
		}
	}
	return $transport;
    }

    /**
     * Generate the transport data
     * @param string $label
     * @return array
     */
    private static function createTransport(string $label) : array
    {
        $transport = [];
        $transport['key'] = Str::random(5);
        $transport['label'] = $label;
        $transport['map'] = true;
        $transport['showTime'] = true;
        $transport['circular'] = true;
        $transport['selected'] = false;
        $transport['total_time'] = 0;
        $transport['total_item_time'] = 0;
        return $transport;
    }

    /**
     * Check if their are orders in the OVS data, otherwise we won't create a plan-board
     * @param $orders
     * @return bool
     */
    private static function orderExists($orders) : bool
    {
        if(isset($orders) && count($orders) > 0) {
            return false;
        }

        $ordersComplete = true;

        foreach ($orders as $order) {
            if(!array_key_exists('code', $order) || $order['code'] === '') {
                $ordersComplete = false;
                break;
            }

            if(!array_key_exists('label', $order) || $order['label'] === '') {
                $ordersComplete = false;
                break;
            }

            if(!array_key_exists('customer', $order) || $order['customer'] === '') {
                $ordersComplete = false;
                break;
            }
        }
        return $ordersComplete;
    }

    public static function updateTransport(array $routeData, array $order, string $transportLabel) : array
    {
        $cells = Arr::get($routeData, 'cells', []);
        $orderHash = self::locationHash($order);

        //We have the period, but we have to find the location to determine the transport
        $oldLocation = self::getOldLocation($cells, $orderHash);

        return self::moveTransportToNewCell($cells, $oldLocation, $transportLabel, $orderHash, $order);
    }

    /**
     * Find the transport cell and returns the label
     * @param array $cells
     * @param string $orderHash
     * @return string | null
     */
    private static function getOldLocation(array $cells, string $orderHash) : array
    {
        $transportLabel = null;
        $transportFound = false;
        $perioddate = null;
        $makeLocationEmpty = false;
        $address = [];

        Log::debug(print_r($cells, true));

        foreach ($cells as $cIdx => $cell) {
            $locations = Arr::get($cell, 'locations', []);

            foreach ($locations as $location) {
                $orders = Arr::get($location, 'orders', []);
                if(empty($orders)) {
                    continue;
                }

                if(count($orders) === 1) {
                    $makeLocationEmpty = true;
                }

                foreach ($orders as $order) {

                    $locHash = self::locationHash($order);

                    Log::debug($locHash .'==' . $orderHash);

                    if($locHash === $orderHash) {
                        $transportFound = true;
                        $transportLabel = Arr::get($cell, 'transport.label');
                        $address = Arr::get($location, 'address');
                        $perioddate = Arr::get($cell, 'period.date');
                        break;
                    }
                }

            }

            if($transportFound) {
                break;
            }
        }

        if(!$transportFound) {
            throw new \Exception('Transport not found, cannot update the period');
        }

        return ['oldTransportLabel' => $transportLabel, 'oldPeriodDate' => $perioddate, 'address' => $address, 'locationEmpty' => $makeLocationEmpty];
    }

    /**
     * Updates the period
     * @param array $routeData
     * @param array $order
     * @param string $perioddate
     * @return array
     */
    public static function updatePeriod(array $routeData, array $order, string $perioddate) : array
    {
        $cells = Arr::get($routeData, 'cells', []);
        $orderHash = self::locationHash($order);

        //We have the period, but we have to find the location to determine the transport
        $oldLocation = self::getOldLocation($cells, $orderHash);

        return self::movePeriodToNewCell($cells, $oldLocation, $perioddate, $orderHash, $order);
    }

    private static function moveTransportToNewCell(array $cells, array $oldLocation, string $transportLabel, string $orderHash, $newOrders) : array
    {
        //Path for deleting the location
        $deletePath = null;
        //New path for adding the location
        $newPath = null;

        foreach ($cells as $cIdx => &$cell) {
            $cellPeriodDate = Arr::get($cell, 'period.date');
            $cellTransportLabel = Arr::get($cell, 'transport.label');

            Log::debug($transportLabel . '==' . $cellPeriodDate);

            //If the old location is the same as the cell, we delete it
            if($cellPeriodDate === $oldLocation['oldPeriodDate'] && $cellTransportLabel === $oldLocation['oldTransportLabel']) {
                Log::debug('Old location found');
                if($oldLocation['locationEmpty']) {
                    Log::debug('Deleting old location');
                    $cell['locations'] = [];
                    continue;
                }

                $locations = Arr::get($cell, 'locations', []);

                foreach ($locations as $lIdx => $location) {
                    $orders = Arr::get($location, 'orders', []);
                    if(empty($orders)) {
                        continue;
                    }
                    foreach ($orders as $oIdx => $order) {
                        $locOrderHash = self::locationHash($order);
                        if($locOrderHash === $orderHash) {
                            $deletePath = '.' . $cIdx . '.locations' . $lIdx . '.orders' . $oIdx;
                            break 2;
                        }
                    }
                }
            }

            if($oldLocation['oldPeriodDate'] === $cellPeriodDate && $cellTransportLabel === $transportLabel) {
                $locations = Arr::get($cell, 'locations', []);
                $addressFound = false;
                Log::debug('New location found to place the order');

                foreach ($locations as $lIdx => $locationAddress) {
                    $cldata = $locationAddress['address'];
                    $cellAddressHash = self::addressHash($cldata);
                    $oldAddressHash = self::addressHash($oldLocation['address']);

                    Log::debug('Testing address hashes' . ' ' . $oldAddressHash . '==' . $cellAddressHash);

                    if($oldAddressHash === $cellAddressHash) {
                        Log::debug('Address found');
                        $addressFound = true;
                        $path = '.' . $cIdx . '.locations' . $lIdx . '.orders';
                        Arr::add($cells , $path, $newOrders);
                    }
                }

                if(!$addressFound) {
                    $cell['locations'][] = ['orders' => [$newOrders], 'address' => $oldLocation['address']];
                }
            }
        }

        //Remove the old cell
        if($deletePath !== null) {
            Arr::forget($cells, $deletePath);
        }

        return $cells;
    }

    private static function movePeriodToNewCell(array $cells, array $oldLocation, string $perioddate, string $orderHash, $newOrders) : array
    {
        //Path for deleting the location
        $deletePath = null;
        //New path for adding the location
        $newPath = null;

        foreach ($cells as $cIdx => &$cell) {
            $cellPeriodDate = Arr::get($cell, 'period.date');
            $cellTransportLabel = Arr::get($cell, 'transport.label');

            Log::debug($perioddate . '==' . $cellPeriodDate);

            //If the old location is the same as the cell, we delete it
            if($cellPeriodDate === $oldLocation['oldPeriodDate'] && $cellTransportLabel === $oldLocation['oldTransportLabel']) {
                Log::debug('Old location found');
                if($oldLocation['locationEmpty']) {
                    Log::debug('Deleting old location');
                    $cell['locations'] = [];
                    continue;
                }

                $locations = Arr::get($cell, 'locations', []);

                foreach ($locations as $lIdx => $location) {
                    $orders = Arr::get($location, 'orders', []);
                    if(empty($orders)) {
                        continue;
                    }
                    foreach ($orders as $oIdx => $order) {
                        $locOrderHash = self::locationHash($order);
                        if($locOrderHash === $orderHash) {
                            $deletePath = '.' . $cIdx . '.locations' . $lIdx . '.orders' . $oIdx;
                            break 2;
                        }
                    }
                }
            }



            if($perioddate === $cellPeriodDate && $cellTransportLabel === $oldLocation['oldTransportLabel']) {
                $locations = Arr::get($cell, 'locations', []);
                $addressFound = false;
                Log::debug('New location found to place the order');


                foreach ($locations as $lIdx => $locationAddress) {
                    $cldata = $locationAddress['address'];
                    $cellAddressHash = self::addressHash($cldata);
                    $oldAddressHash = self::addressHash($oldLocation['address']);

                    Log::debug('Testing address hashes' . ' ' . $oldAddressHash . '==' . $cellAddressHash);

                    if($oldAddressHash === $cellAddressHash) {
                        Log::debug('Address found');
                        $addressFound = true;
                        $path = '.' . $cIdx . '.locations' . $lIdx . '.orders';
                        Arr::add($cells , $path, $newOrders);
                    }
                }
                if(!$addressFound) {
                    $cell['locations'][] = ['orders' => [$newOrders], 'address' => $oldLocation['address']];
                }
            }
        }

        //Remove the old cell
        if($deletePath !== null) {
            Arr::forget($cells, $deletePath);
        }

        return $cells;
    }

    /**
     * Returns the hash of the order
     * @param array $location
     * @return string
     */
    private static function locationHash(array $location) : string
    {
        $locorder_hash = [
            trim(Arr::get($location, 'label', '')),
            trim(Arr::get($location, 'customer', '')),
            trim(Arr::get($location, 'code', '')),
        ];
        $locorder_hash = implode('|', $locorder_hash);
        Log::info('Local order hash '. $locorder_hash);
        return md5($locorder_hash);
    }

    /**
     * CHeck if the address exists
     * @param array $address
     * @return string
     */
    private static function addressHash(array $address) : string
    {
        $cellAddress =[
            trim($address['label']),
            trim($address['street']),
            trim($address['zipcode']),
            trim($address['city']),
            trim($address['country'])
        ];
        $cellAddress = implode('|', $cellAddress);
        $cellAddress = strtolower($cellAddress);
        return md5($cellAddress);
    }

	/**
	 * Takes the planboard data and then add a location in the correct position. This function returns the new
	 * planboard data as array.
	 */
	public static function addCellToPlanboard(array $plandata, array $cell) {
		$current_routedata = $plandata;
		$newcell = $cell;

		if($current_routedata === null){
			Log::info('API AddLocation created new plandboard and cell for ' . Arr::get($newcell, 'locations.0.label'));
			return createPlanningWithOneCell($newcell);

		}
		// there is routedata available, we need to look for the correct place to merge the ne location into the routedata
		$new_routedata = $current_routedata;
		if(!array_key_exists('cells', $new_routedata)){
			$new_routedata['cells'] = [$newcell];
			return $new_routedata;
		}

		$transport_label = Arr::get($newcell, 'transport.label');
		$period_label = Arr::get($newcell, 'period.label');
		$period_date = Arr::get($newcell, 'period.date');

		// loop through all cells to find similar cells, to prevent duplicates
		$found_cell = false;
		foreach($new_routedata['cells'] as $cidx => $cell){
			$cell_transport_label = Arr::get($cell, 'transport.label');
			if($cell_transport_label == $transport_label) {// @todo make utf8 safe string comparisons
				// update keys for when a new cell has to be added (to make sure all transports with the same label have the same internal key)
				Arr::set($newcell, 'transport.key', Arr::get($cell, 'transport.key'));
			}

			$cell_period_label = Arr::get($cell, 'period.label');
			if($cell_period_label == $period_label){
				// update keys for when a new cell has to be added (to make sure that all periods with the same label have the same internal key)
				Arr::set($newcell, 'period.key', Arr::get($cell, 'period.key'));
			}

			$cell_period_date = Arr::get($cell, 'period.date');

			// we are no longer checking the label, this might be an issue when in future instances, there is no date available
			if(($cell_transport_label == $transport_label) /* && ($cell_period_label == $period_label)*/ && ($cell_period_date == $period_date)){
				// we found the correct cell, now we need to find the correct handling of the location (add or merge)

				// try to add each location from $newcell into this cells locations, trying to match each location
				$newcell_locations = Arr::get($newcell, 'locations');
				$plancell_locations = Arr::get($cell, 'locations');

				foreach($newcell_locations as $newcell_location){
					$could_merge = false;
					foreach($plancell_locations as $plancell_index=>$plancell_location){
						if(self::canMergeLocationIntoLocation($newcell_location, $plancell_location)){
							$could_merge = true;
							Arr::set($plancell_location, 'orders', array_merge($plancell_location['orders'], $newcell_location['orders']));
							Arr::set($new_routedata, 'cells.' . $cidx . '.locations.' . $plancell_index, $plancell_location);
							break;
						}
					}

					if(!$could_merge){
						// we could not match, so just add the location to the cell
						Arr::set($new_routedata, 'cells.' . $cidx . '.locations', array_merge(Arr::get($new_routedata, 'cells.' . $cidx . '.locations'), [$newcell_location]));
					}
				}
				$found_cell = true;

			}
		}

		if(!$found_cell) {
			// no match was found, so we add a new cell to the cells
			Log::info('API AddLocation created new cell for ' .  Arr::get($cell, 'locations.0.label'));
			$new_routedata['cells'][] = $newcell; //@todo check for other cells to add the location in stead
		}

		return $new_routedata;
	}

	/**
	 * Calculate a hash for an address, allowing some similarities
	 */
	public static function hashAddress($address){
		$testAddress = [
			trim($address['label']),
			trim($address['street']),
			trim($address['zipcode']),
			trim($address['city']),
			trim($address['country'])
		];
		$testAddress = implode('|', $testAddress);
		$testAddress = strtolower($testAddress);
		$testAddressHash = md5($testAddress);
		return $testAddressHash;
	}

	/**
	 * Checks if the address of both locations are identical
	 */
	private static function canMergeLocationIntoLocation($locationa, $locationb){
		$hashA = self::hashAddress(Arr::get($locationa, 'address'));
		$hashB = self::hashAddress(Arr::get($locationb, 'address'));

		return $hashA === $hashB;
	}

	/**
	 * Creates a cell that can be added and merged into a planning.
 	 *
         * This function only creates the data to add to the planning.
	 *
	 * @param string $transport_label The label of the transport
	 * @param string $period_label The label of the period
	 * @param string $period_date The real date of the period
	 * @param array $location The location to add in the new cell
	 * @param array $routedata The current routedata in the planning, this helps finding the correct period
	 */
	public static function createAddableCell($transport_label, $period_label, $period_date, array $location, array $routedata){
		$transport_label = trim($transport_label);
		$transport_collection = new Transport;	
		$transport = $transport_collection->getTransportByLabel($transport_label);

		if ($transport === null){
			// create a new transport and register into the database
			$transport = new Transport();
			$transportdata = $transport->makeTransportData($transport_label);
            
			$transport->saveFromOvs($transport_label,$transportdata);
		} 

		// Generate random keys in case this is a totally new entry.
		$random_period_key = Str::random(6);

		// try to find the period
		$cells = Arr::get($routedata, 'cells', []);
		foreach($cells as $cell){
			$period = Arr::get($cell, 'period', null);
			if($period !== null){
				if(Arr::get($period, 'date') === $period_date){
					$random_period_key = Arr::get($period, 'key');
					$period_label = Arr::get($period, 'label');
				}
			}
		}




		$newcell =
			[
				"transport" => $transport,
				"period" => [
					"key" => $random_period_key,
					"label" => $period_label,
					"date" => $period_date
				],
				"locations" => [$location]
			];

		
        
		return $newcell;
	}

	/**
	 * Creates an array that can be added as a location to a cell
	 * @param string $label A human readable label (mostly name of the business)
	 * @param string $street The street
	 * @param string $zipcode The zipcode / postal code
	 * @param string $city The city
	 * @param string $country The country
	 * @param array $orders The orders to add in the location
 	 */
	public static function createAddableLocation($label, $street, $zipcode, $city, $country, array $orders){
		$location = [
			'address' => [
				'label'   => $label,
				'street'  => $street,
				'zipcode' => $zipcode,
				'city'    => $city,
				'country' => $country
			],
			'orders' => $orders
		];
		return $location;
	}

	/**
	 * Creates the data for a new planning with only one cell.
	 *
 	 * @param array $cell The cell to add to the planning
	 */
	public static function createPlanningWithOneCell($cell){
		return  [
				"meta" => [
					"type" => "planitems",
					"version" => "1.0.0"
				],
				"cells" =>[
					$cell
				]
			];

	}

	/**
         * Creates an empty planning
	 */
        public static function createEmptyPlanning(){
                return  [
                                "meta" => [
                                        "type" => "planitems",
                                        "version" => "1.0.0"
                                ],
                                "cells" =>[
                                ]
                        ];
        }


}
