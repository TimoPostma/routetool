<?php

namespace App\Services;


use Illuminate\Support\Facades\Log;

class RouteTemplateService
{
    public static function transformTemplateDataWithOVSData(array $templateData, array $ovsLeveringen): array
    {
        foreach ($ovsLeveringen as $ovsLevering) {

            Log::debug(print_r($ovsLevering['orders'], true));

            $orders = json_decode($ovsLevering['orders'], true);

            $orderExists = self::orderExists($orders);



            if(!$orderExists) {
                throw new \Exception('Order not found');
            }

            $locations['orders'] = $orders;
            $locations['address'] = self::makeLocationData($ovsLevering['city'], $ovsLevering['zipcode'], $ovsLevering['label'], $ovsLevering['street'], $ovsLevering['country']);

            $cellFound = false;

            $periodLabel = self::translateDateLabels($ovsLevering['period_date']);

            foreach ($templateData['cells'] as $cIdx => $cell) {
                if(self::cellExist($cell, $ovsLevering['transport_label'], $periodLabel)) {
                    $cellFound = true;
                    $templateData['cells'][$cIdx]['locations'][] = $locations;
                    break;
                }
            }

            if(!$cellFound) {
                throw new \Exception('No cell found to place hte order');
            }
        }
        return $templateData;
    }


    /**
     * Check if an order exists
     * @param $orders
     * @return bool
     */
    private static function orderExists($orders) : bool
    {
        return isset($orders) && count($orders) > 0;
    }

    private static function makeLocationData($city, $zipcode, $label, $street, $country) : array
    {
        $address['city'] = $city;
        $address['label'] = $label;
        $address['street'] = $street;
        $address['country'] = $country;
        $address['zipcode'] = $zipcode;
        return $address;
    }

    /**
     * Check if the
     * @param array $cell
     * @param string $transportLabel
     * @param string $periodLabel
     * @return bool
     */
    private static function cellExist(array $cell, string $transportLabel, string $periodLabel) : bool
    {
        return $cell['period']['label'] === $periodLabel && $cell['transport']['label'] === $transportLabel;
    }

    /**
     * Check the date and returns the day as  a label
     * @param string $period_date
     * @return string
     */
    private static function translateDateLabels(string $period_date) : string
    {
        $days = ['Zondag', 'Maandag', 'Dinsdag', 'Woensdag', 'Donderdag', 'Vrijdag', 'Zaterdag'];
        $dayOfWeek = \Carbon\Carbon::parse($period_date)->dayOfWeek;
        return $days[$dayOfWeek];
    }

    //@TODO for now we only check for existing periods or trnasport FOR THE FUTURE
    private function newPeriod() : array
    {
        return [];
    }

    /**
     * Generate a new transport
     * @param $transportLabel
     * @return array
     */
    private function newTransport($transportLabel) : array
    {
        $transport = [];
        $transport['key'] = \Illuminate\Support\Str::random(5);
        $transport['label'] = $transportLabel;
        $transport['map']  = true;
        $transport['showTime'] = true;
        $transport['circular'] = true;
        $transport['selected'] = false;
        $transport['total_time'] = 0;
        $transport['total_item_time'] = 0;

        return $transport;
    }
}
