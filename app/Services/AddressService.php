<?php

namespace App\Services;
use App\AddressHash;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use GuzzleHttp\Client;



class AddressService
{   
    public $addressHash;


   public function __construct(AdressHash $addressHash)
   {
      $this->addresshash = $addressHash;
  
   }
    public static function save_Hash($address){
    $address = str_replace(" ", "+", $address);

    $json = file_get_contents("https://maps.google.com/maps/api/geocode/json?address=$address&sensor=false&key=".env('APP_GOOGLE_KEY'));
    $json = json_decode($json);
    $hash = md5($address);
   $lat = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lat'};
   $long = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lng'};
        $addressHash = new AddressHash;
        $addressHash->hash = $hash;
        $addressHash->lat = $lat;
        $addressHash->long = $long;
        $addressHash->saveHash($hash,$lat,$long);
        return $addressHash;
        
    }
    public static function getLatLong($hash){
        $addressHash = $this->addresshash->getAddressHashByHash($hash);
    
        return [$addressHash->lat,$addressHash->long];

            
    }

}
