<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Admin extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getAdminById($id)
    {
        return $this->where('id', $id)->first();
    }

    public function checkIfApiTokenExists($token)
    {
        return $this->where('api_token', $token)->exists();
    }

    public function updateApiToken($admin, $token)
    {
        $admin->api_token = $token;
        $admin->update();
    }

}
