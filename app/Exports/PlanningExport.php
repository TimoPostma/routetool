<?php

namespace App\Exports;

use Carbon\Carbon;
use App\Planning;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\FromCollection;

class PlanningExport implements FromArray,WithMapping, withHeadings
{
    protected $planning;
    protected $array;

    public function __construct($array)
    {
      $this->array = $array;
    }
  
    public function headings() : array
    {
        return [
          'period',
          'transport',  
          'Label',  
          'Street',
          'Zipcode',
          'City',
          'Country',
          'Product',
          'Code',
          'Commission'

        ];
    }


    public function array(): array
    {
      
        return $this->array;
        
    }

    public function map($row): array
    {   
        Log::debug(print_r($row, true));
        return $row;
    }
}
