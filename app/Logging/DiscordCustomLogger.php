<?php


namespace App\Logging;


use DiscordHandler\DiscordHandler;
use Monolog\Logger;

class DiscordCustomLogger
{
    public function __invoke(array $config)
    {
        $environment = env('APP_ENV');
        $log = new Logger('DiscordHandler');
        return $log->pushHandler(new DiscordHandler(env('DISCORD_WEBHOOK'), strtoupper($environment)));
    }
}
