<?php


namespace App\Logging;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Monolog\Logger;
use Monolog\Handler\AbstractProcessingHandler;

class MySQLLoggingHandler extends AbstractProcessingHandler
{
    public function __construct($level = Logger::DEBUG, $bubble = true)
    {
        parent::__construct($level, $bubble);
    }

    protected function write(array $record) : void
    {
        try {
            DB::table('logs')->insert([
                'status' => $record['level_name'],
                'description' => $record['message'],
                'level' => $record['level'],
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]);
        } catch (\Exception $exception) {

        }

    }
}
