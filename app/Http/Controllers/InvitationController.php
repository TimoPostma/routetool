<?php

namespace App\Http\Controllers;

use App\AcceptedInvite;
use App\Helpers\MandrillHelper;
use App\Invitation;
use App\Mail\SendActivationEmail;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\URL;
use Illuminate\Validation\Rule;
use Session;

class InvitationController extends Controller
{
    public $invitation;
    public $acceptedInvite;
    public $user;

    public function __construct(Invitation $invitation, AcceptedInvite $acceptedInvite, User $user)
    {
        $this->invitation = $invitation;
        $this->acceptedInvite = $acceptedInvite;
        $this->user = $user;
    }

    public function invitedUser(Request $request)
    {

        $signature = $request->signature;
        $timestamp = $request->timestamp;
        $userId = $request->userId;


        if(!isset($signature) || !isset($timestamp) || !isset($userId)) {
            return redirect()->route('login');
        }

        $signatureValid = $this->invitation->checkIfTimeStampAndSignatureAreValid($signature, $timestamp, $userId);

        if(!$signatureValid) {
            Session::flash('warning', __('invitations.unknown_link'));
            return redirect()->route('login');
        }

        $invitation = $this->invitation->getInvitation($userId);

        if(Carbon::parse($invitation->expiration_date) < Carbon::now()) {
            Session::flash('warning', __('invitations.expired_link'));
            return redirect()->route('login');
        }

        if($request->method() === "GET") {
            return view('authentication.register');
        }

        $request->validate([
            'name' => 'required',
            'email' => [
                'required',
                'email',
                Rule::unique('users', 'email')
            ],
            'password' => ['required', 'min:8']
        ]);

        try {
            $user = $this->user->addUser($request);
        } catch (\Exception $exception) {
            Session::flash('error', __('sessions.user_cannot_added'));
            Log::channel('db_log')->critical('Gebruiker kan niet worden aangemaakt');
            return back();
        }

        //Add the invitation to the user
        try {
            $this->acceptedInvite->storeAcceptedInvite($user->id, $invitation->id);
        } catch (\Exception $exception) {
            Log::error('Gebruiker kon niet aan de accepted_invites worden toegevoegd'. ' ' . $exception->getMessage());
        }

        $signedUrl = URL::temporarySignedRoute(
            'activate-user', Now()->addHours(72), ['id' => $user->id]
        );

        try {
            if(env('APP_ENV') === 'production') {

                $template = 'activate account';
                $fromMail = 'info@routetool.online';
                $fromName = 'routetool.online';
                $mergeVars = ['ACTIVATEURL' => $signedUrl];
                $toEmail = $user->email;
                $toName = $user->name;

                MandrillHelper::sendEmailWithoutAttachment($template, 'activate account', $fromMail, $fromName, $mergeVars, $toEmail, $toName);
            } else {
                Mail::to($user->email)->send(new SendActivationEmail($signedUrl));
            }

        } catch (\Exception $exception) {
            Session::flash('warning', __('sessions.activation_mail_cannot_send'));
            Log::channel('db_log')->warning('Email kan niet naar' . ' ' . $user->email . ' ' . 'worden verstuurd');
        }

        Log::channel('db_log')->info('Gebruiker' . ' ' . $user->email . ' ' . 'is successvol aangemaakt');

        Session::flash('success', __('sessions.user_made'));
        return redirect()->route('login');
    }
}
