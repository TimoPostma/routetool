<?php

namespace App\Http\Controllers\Authentication;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Session;

class LoginController extends Controller
{

    /**
     * @OA\Info (
     *     title="Routetool API",
     *     version="1.0.0"
     * )
     */

    /**
     * @OA\Post (
     *     path="oauth/authorize",
     *     description="Authorize a client and returns a personal authorization token",
     *     operationId="oauthLogin",
     *      @OA\Parameter(
     *          name="grant_type",
     *          description="Authorize oath method",
     *          required=true,
     *          in="query",
     *          example="password",
     *      ),
     *      @OA\Parameter(
     *          name="client_id",
     *          description="Id of the oauth client",
     *          required=true,
     *          in="query",
     *          example="1",
     *     @OA\Schema (type="integer"),
     *      ),
     *               @OA\Parameter(
     *          name="client_secret",
     *          description="Secret of the client",
     *          required=true,
     *          in="query",
     *          example="client secret",
     *      ),
     *      @OA\Parameter(
     *          name="email",
     *          description="email",
     *          required=true,
     *          in="query",
     *          example="taylor@laravel.com",
     *      ),
     *      @OA\Parameter(
     *          name="password",
     *          description="Get the password",
     *          required=true,
     *          in="query",
     *          example="password",
     *      ),
     *      @OA\Parameter(
     *          name="scope",
     *          description="The scope",
     *          required=true,
     *          in="query",
     *          example="*",
     *      ),
     *     @OA\Response(response=200, description="returns token"),
     *     @OA\Response(response=401, description="Unautorized")
     * )
     */

    public function authenticate(Request $request)
    {
        if($request->method() === 'GET') {
            return view('authentication.login');
        }

        $request->validate([
            'email' => [
                'email',
                'required',
            ],
            'password' => [
                'required',
            ]
        ]);

        if(!Auth()->attempt(['email' => $request->email, 'password' => $request->password])) {
            Session::flash('error', __('sessions.invalid_credentials'));
            return back();
        }
        Log::info('User with email' . ' ' . $request['email'] . ' ' . 'has logged in');
        return redirect()->route('user.organisations.index');
    }

    public function planningLogin(Request $request, $userId, $organisationId, $planningId)
    {
        if(!$request->hasValidSignature()) {
            abort(401);
        }

        //Check if the user has already logged in and redirect to the planning page
        if(Auth()->check())  {
            return redirect()->route('organisation.planning.show', ['id' => $organisationId, 'planningId' => $planningId]);
        }

        //Login the user and redirect to the planning
        Auth()->loginUsingId($userId);
        return redirect()->route('organisation.planning.show', ['id' => $organisationId, 'planningId' => $planningId]);
    }

    /**
     * Handles login with a one-time token
     */
    public function loginWithUrl(Request  $request)
    {
        if(Auth()->check()) {
            return redirect()->route('user.organisations.index');
        }

        if($request['signature'] === null) {
            Session()->flash('error', 'Uw token is verlopen');
            return redirect()->route('login');
        }

        $cacheExists = Cache::has('consumable|' . $request['signature']);

        if($cacheExists || !$request->hasValidSignature()) {
            Session()->flash('warning', 'Uw token is verlopen');
            return redirect()->route('login');
        }


        Cache::put('consumable|' . $request['signature'], 'used', $request['expires']);
        $userId = $request['userId'];
        Auth()->loginUsingId($userId);
        return redirect()->route('user.organisations.index');
    }
}
