<?php

namespace App\Http\Controllers\Authentication;

use App\Helpers\MandrillHelper;
use App\Http\Controllers\Controller;
use App\Mail\SendActivationEmail;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Mockery\Exception;
use Session;

class RegisterController extends Controller
{
    public $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function register(Request $request)
    {
        if($request->method() === 'GET') {
            return view('authentication.register');
        }

        $request->validate([
            'name' => 'required',
            'email' => [
                'required',
                'email',
                Rule::unique('users', 'email')
            ],
            'password' => ['required', 'min:8']
        ]);

        try {
            $user = $this->user->addUser($request);
        } catch (Exception $exception) {
            Session::flash('error', __('sessions.user_cannot_added'));
            Log::channel('db_log')->critical('Gebruiker kan niet worden aangemaakt');
            return back();
        }

        Log::info('User with email' . $user->email . ' has been created');

        $signedUrl = URL::temporarySignedRoute(
          'activate-user', Now()->addHours(72), ['id' => $user->id]
        );

	log::info('User with email' . $user->email . ' has signed URL ' . $signedUrl);


        try {
            if(env('APP_ENV') === 'production') {

                $template = 'routetool-activate-en';
                $fromMail = 'info@routetool.online';
                $fromName = 'routetool.online';
                $mergeVars = ['ACTIVATEURL' => $signedUrl];
                $toEmail = $user->email;
                $toName = $user->name;

                MandrillHelper::sendEmailWithoutAttachment($template, 'activate account', $fromMail, $fromName, $mergeVars, $toEmail, $toName);
            } else {
                try {
                    Mail::to($user->email)->send(new SendActivationEmail($signedUrl));
                } catch(\Exception $exception) {
                    Session::flash('warning', __('sessions.activation_mail_cannot_send'));
                    return view('authentication.register');
                }
            }

        } catch (Exception $exception) {
            Session::flash('warning', __('sessions.activation_mail_cannot_send'));
            Log::channel('db_log')->warning('Email kan niet naar' . ' ' . $user->email . ' ' . 'worden verstuurd');
        }

        Log::channel('db_log')->info('Gebruiker' . ' ' . $user->email . ' ' . 'is successvol aangemaakt');

        Session::flash('success', __('sessions.user_made'));
        return redirect()->route('login');
    }

    public function activateUser(Request $request, $id)
    {
        if(!$request->hasValidSignature()) {
            Session::flash('warning', __('sessions.link_expired'));
            return redirect()->route('login');
        }

        $user = $this->user->getUserById($id);

        if(!isset($user)) {
            Session::flash('warning', __('sessions.link_expired'));
            return redirect()->route('login');
        }

        if($user->activated) {
            Session::flash('warning', __('sessions.link_expired'));
            return redirect()->route('login');
        }

        $user->activated = 1;
        $user->api_token = Str::random(80);
        $user->update();
        Session::flash('success', __('sessions.user_activated'));
        Log::channel('db_log')->info('Gebruiker' . ' ' . $user->email . ' ' . 'heeft zijn account geactiveerd');
        return redirect()->route('login');

    }
}
