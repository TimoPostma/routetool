<?php

namespace App\Http\Controllers\Authentication;

use App\Helpers\MandrillHelper;
use App\Http\Controllers\Controller;
use App\Mail\SendPasswordResetLink;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\URL;
use Session;

class ForgetPasswordController extends Controller
{
    public $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }


    public function forgetPassword(Request $request)
    {
        if($request->method() === 'GET') {
            return view('authentication.forget_password');
        }

        $request->validate([
            'email' => ['required']
        ]);

        $user = $this->user->getUserByEmail($request->email);

        if(!isset($user)) {
		// the user does not exist, but we don't want to publish that kind of information
		Session::flash('success', __('sessions.password_reset_email_sent'));
		return redirect()->route('login');
        }

        // Make a  signed url which expires within in a hour
        $signedUrl = URL::temporarySignedRoute(
            'reset-password', Now()->addHour(), ['email' => $user->email]
        );

        try {
            if(env('APP_ENV') === 'production') {
                $template = 'routetool-recover-en';
                $fromMail = 'info@routetool.online';
                $fromName = 'routetool.online';
                $mergeVars = ['RESETURL' => $signedUrl];
                $toEmail = $user->email;
                $toName = $user->name;

                MandrillHelper::sendEmailWithoutAttachment($template, 'Recover password', $fromMail, $fromName, $mergeVars, $toEmail, $toName);
            } else {
                Mail::to($user->email)->send(new SendPasswordResetLink($signedUrl));
            }
        } catch (\Throwable $exception) {

		Log::debug('Recovery email could not be sent: ' . $exception->getMessage());			
            	Session::flash('error', __('sessions.error_sending_mail'));
            	return back();
        }

        Session::flash('success', __('sessions.password_reset_email_sent'));
        return redirect()->route('login');
    }

    public function resetPassword(Request $request, $email)
    {
        if(!$request->hasValidSignature()) {
            Session::flash('warning', __('sessions.link_expired'));
            return redirect()->route('login');
        }

        if($request->method() === 'GET') {
            return view('authentication.reset_password');
        }

        $request->validate([
            'password' => ['required', 'min:8']
        ]);

        $user = $this->user->getUserByEmail($email);

        if(!isset($user)) {
            Session::flash('warning', __('sessions.link_expired'));
            return redirect()->route('login');
        }

        $user->password = bcrypt($request->password);
        $user->update();
        Session::flash('success', __('sessions.link_expired'));
        return redirect()->route('login');
    }
}
