<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function getIndexPage()
    {
        return view('admin.dashboard.index');
    }

    public function logout()
    {
        Auth('admin')->logout();
    }
}
