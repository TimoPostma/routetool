<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    public function login(Request $request)
    {
        if($request->method() === 'GET') {
            return view('authentication.admin.login');
        }

        $request->validate([
            'email' => [
                'email',
                'required'
            ],
            'password' => [
                'required'
            ]
        ]);

        $email = $request->email;
        $password = $request->password;

        if(Auth('admin')->attempt(['email' => $email, 'password' => $password])) {
            return redirect()->route('admin.dashboard.index');
        }
        return back();
    }
}
