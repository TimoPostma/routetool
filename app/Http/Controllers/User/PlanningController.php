<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Exports\PlanningExport;
use App\Exports\UsersExport;
use App\Organisation;
use App\PermissionPlanning;
use App\Planning;
use App\Template;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Http;
use Maatwebsite\Excel\Facades\Excel;
use Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Response;

class PlanningController extends Controller
{   
   public $template;
   public $planning;
   public $organisation;
   public $permissionPlanning;

   public function __construct(Organisation $organisation, Planning $planning, PermissionPlanning $permissionPlanning,Template $template)
   {
      $this->organisation = $organisation;
      $this->template = $template;
      $this->permissionPlanning = $permissionPlanning;
      $this->planning = $planning;
   }

   public function create($id)
   {
      $organisation = $this->organisation->getOrganisationById($id);

      if(!isset($organisation)) {
         abort(404);
      }
      return view('user.plannings.create', ['organisationId' => $id]);
   }

   public function store(Request $request, $id)
   {
      $organisation = $this->organisation->getOrganisationById($id);

      if(!isset($organisation)) {
         abort(404);
      }
      //@TODO Upload excel to the server with organisation and label from the planning
      $request->validate([
         'name' => ['required'],
      ]);
      //return $this->template->getTemplateById(2);
      if(!isset($request['routeData'])){
         $routeData = "";
         if(isset($request['json'])){
            $routeData = json_encode($request['json']);
         }
      } 
      else{
         $routeData = $this->template->getTemplateById($request['routeData'])[0]->templatedata;
      }
      $permission = $this->permissionPlanning->getPermissionLevel(2);
      Session::flash('success', __('planboard.new'));
      if($permission == null){
         abort(403);
      }
      return $this->planning->storePlanning($request, $id, $permission->id,$routeData);  
    }

    public function show($organisationId, $planningId, $start = null)
    {
      $organisation = $this->organisation->getOrganisationById($organisationId);
       
      if(!isset($organisation)) {
         abort(404);
      }

      $planning = $this->planning->getPlanningById($planningId);

      if(!isset($planning)) {
         abort(404);
      }
      if($planning->is_locked_set == null){
         $planning->is_locked_set = 0;
      }
        //$planning->is_locked_set = !$planning->is_locked_set;
        //return $planning;
      return view('user.plannings.show', ['organisation' => $organisation,'planning' => $planning,'start' => $start]);
   }
   public function getEditPage($id,$planningId){
      $organisation = $this->organisation->getOrganisationById($id);

      if(!isset($organisation)) {
         abort(404);
      }

      $planning = $this->planning->getPlanningById($planningId);

      if(!isset($planning)) {
         abort(404);
      }
      return view('user.plannings.edit',['planboard' => $planning,'organisation' => $organisation]);
   }
   public function update(Request $request,$id,$planningId){

      $organisation = $this->organisation->getOrganisationById($id);
      if(!isset($organisation)) {
         abort(404);
      }
      $planning = $this->planning->getPlanningById($planningId);

      if(!isset($planning)) {
         abort(404);
      }
      $permission = $this->permissionPlanning->getPermissionLevel(2);
      //return $planning;
      return $planning->updatePlanning($planning,$request->name,$request->description,$id,$permission);
      //return view('user.plannings.edit',['planboard' => $planning,'organisation' => $organisation]);
   }
   public function exportCellToExcel(Request $request,$id,$planningId,$transportKey,$periodKey){
      $planning = $this->planning->getPlanningById($planningId);
      $startlocation = false;
      $planningArr = json_decode($planning->routedata,true);
      if ($planningArr['startlocation']){
         $startlocation = $planningArr['startlocation'];
      }     
      $selectedCell = [];
      $newcell;
      $circular = false;
      foreach($planningArr['cells'] as $cell){
         $pKey = Arr::get($cell, 'period.key');
         $tKey = Arr::get($cell, 'transport.key');
         $pLabel = Arr::get($cell, 'period.label');
         $tLabel = Arr::get($cell, 'transport.label');
         if($pKey == $periodKey && $tKey == $transportKey){
            if(isset($cell['transport']['circular'])){
               $circular = true;
               if($startlocation){
                  $circular = $startlocation;
                  $row[] = Arr::get($cell, 'period.label');
                  $row[] = Arr::get($cell, 'transport.label');
                  $row[] = Arr::get($startlocation, 'label');
                  $row[] = Arr::get($startlocation, 'street');
                  $row[] = Arr::get($startlocation, 'zipcode');
                  $row[] = Arr::get($startlocation, 'city');
                  $row[] = Arr::get($startlocation, 'country');
                  array_push($selectedCell,$row);
               }
            }

            foreach($cell['locations'] as $location){
               if($circular === true){
                  $circular = $location;
               }
               $row = [];                 
               if (count($location['orders']) === 0) {
                  $row[] = Arr::get($cell, 'period.label');
                  $row[] = Arr::get($cell, 'transport.label');
                  $row[] = Arr::get($location, 'address.label');
                  $row[] = Arr::get($location, 'address.street');
                  $row[] = Arr::get($location, 'address.zipcode');
                  $row[] = Arr::get($location, 'address.city');
                  $row[] = Arr::get($location, 'address.country');
                  array_push($selectedCell,$row);
               }
               else{
                  foreach($location['orders'] as $order){
                     $row = [];
                     $row[] = Arr::get($cell, 'period.label');
                     $row[] = Arr::get($cell, 'transport.label');
                     $row[] = Arr::get($location, 'address.label');
                     $row[] = Arr::get($location, 'address.street');
                     $row[] = Arr::get($location, 'address.zipcode');
                     $row[] = Arr::get($location, 'address.city');
                     $row[] = Arr::get($location, 'address.country');
                     $row[] = Arr::get($order, 'label');
                     $row[] = Arr::get($order, 'code');
                     $row[] = Arr::get($order, 'customer');
                     array_push($selectedCell,$row);
                  }
               }                             
            }
            if($circular){
               $row = [];
               $row[] = Arr::get($cell, 'period.label');
               $row[] = Arr::get($cell, 'transport.label');
               $row[] = Arr::get($circular, 'label');
               $row[] = Arr::get($circular, 'street');
               $row[] = Arr::get($circular, 'zipcode');
               $row[] = Arr::get($circular, 'city');
               $row[] = Arr::get($circular, 'country');
               array_push($selectedCell,$row);
            }
         }
      }
      return Excel::download(new PlanningExport($selectedCell), $planning->name.' - '.$pLabel.' - '.$tLabel.'.xlsx');
   }
   public function exportPlanboardToExcel(Request $request,$id,$planningId){
      $planning = $this->planning->getPlanningById($planningId);
      $startlocation = false;
      $planningArr = json_decode($planning->routedata,true);
      if ($planningArr['startlocation']){
         $startlocation = $planningArr['startlocation'];
      }
      $selectedCell = [];
      $newcell;
      $circular = false;
      $periodlabel = $planningArr['cells'][0]['period']['label'];
      $transportlabel = $planningArr['cells'][0]['transport']['label'];
      foreach($planningArr['cells'] as $cell){
         $pKey = Arr::get($cell, 'period.key');
         $tKey = Arr::get($cell, 'transport.key');
         $pLabel = Arr::get($cell, 'period.label');
         $tLabel = Arr::get($cell, 'transport.label');
              
         if(isset($cell['transport']['circular'])){
            $circular = true;
            if($startlocation){
               $circular = $startlocation;
               $row = [];
               $row[] = Arr::get($cell, 'period.label');
               $row[] = Arr::get($cell, 'transport.label');
               $row[] = Arr::get($startlocation, 'label');
               $row[] = Arr::get($startlocation, 'street');
               $row[] = Arr::get($startlocation, 'zipcode');
               $row[] = Arr::get($startlocation, 'city');
               $row[] = Arr::get($startlocation, 'country');
               array_push($selectedCell,$row);
               }
            }
            foreach($cell['locations'] as $location){
               if($circular === true){
                  $circular = $location;
            }                  
            if (count($location['orders']) === 0) {
               $row = [];
               $row[] = Arr::get($cell, 'period.label');
               $row[] = Arr::get($cell, 'transport.label');
               $row[] = Arr::get($location, 'address.label');
               $row[] = Arr::get($location, 'address.street');
               $row[] = Arr::get($location, 'address.zipcode');
               $row[] = Arr::get($location, 'address.city');
               $row[] = Arr::get($location, 'address.country');
               array_push($selectedCell,$row);
               }
               else{          
                  foreach($location['orders'] as $order){
                     $row = [];
                     $row[] = Arr::get($cell, 'period.label');
                     $row[] = Arr::get($cell, 'transport.label');
                     $row[] = Arr::get($location, 'address.label');
                     $row[] = Arr::get($location, 'address.street');
                     $row[] = Arr::get($location, 'address.zipcode');
                     $row[] = Arr::get($location, 'address.city');
                     $row[] = Arr::get($location, 'address.country');
                     $row[] = Arr::get($order, 'label');
                     $row[] = Arr::get($order, 'code');
                     $row[] = Arr::get($order, 'customer');
                     array_push($selectedCell,$row);
               }
            }
         }
         if($circular){
            $row = [];
            $row[] = Arr::get($cell, 'period.label');
            $row[] = Arr::get($cell, 'transport.label');
            $row[] = Arr::get($circular, 'label');
            $row[] = Arr::get($circular, 'street');
            $row[] = Arr::get($circular, 'zipcode');
            $row[] = Arr::get($circular, 'city');
            $row[] = Arr::get($circular, 'country');
            array_push($selectedCell,$row);
         }        
      }
      return Excel::download(new PlanningExport($selectedCell), $planning->name.' - '.$pLabel.' - '.$tLabel.'.xlsx');
   }
   public function exportCellToPdf(Request $request,$id,$planningId,$transportKey,$periodKey){
      $organisation = $this->organisation->getOrganisationById($id);
      $planning = $this->planning->getPlanningById($planningId);
      $planningArr = json_decode($planning->routedata,true);
      foreach(($planningArr['cells']) as $cellArr){
         if($cellArr['transport']['key'] == $transportKey && $cellArr['period']['key'] == $periodKey){
            $cell = $cellArr;
            $transportlabel = $cellArr['transport']['label'];
            $periodlabel = $cellArr['period']['label'];
            //return count($cellArr['locations']) == 0;
            if(count($cellArr['locations']) == 0){
               Session::flash('error', __('planboard.cellempty'));
               return redirect()->route('organisation.planning.show', [$organisation,$planning]);
            } 
         }
      }      
      $filename = $this->cellToPdf($id,$planningId,$transportKey,$periodKey);
      return Storage::download($filename,$organisation->name." - ".$planning->name."(".$transportlabel." - ".$periodlabel.").pdf");
   }
   private function cellToPdf($id,$planningId,$transportKey,$periodKey){
      $logo = "assets/logo.png";
      $logo = file_get_contents("assets/logo.png");
      $logo = 'data:image/' . 'png'. ';base64,' . base64_encode($logo);
      $urls = [];
      $url = "https://maps.googleapis.com/maps/api/directions/json?";
      $origin = null;
      $waypoints = [];
      $organisation = $this->organisation->getOrganisationById($id);
      $planning = $this->planning->getPlanningById($planningId);
      $startlocation = false;
      $planningArr = json_decode($planning->routedata,true);
      $pageCount = 0;
      if ($planningArr['startlocation']){
         $startlocation = $planningArr['startlocation'];
      }
      foreach(($planningArr['cells']) as $cellArr){
         if($cellArr['transport']['key'] == $transportKey && $cellArr['period']['key'] == $periodKey){
            $cell = $cellArr;
            $transportlabel = $cellArr['transport']['label'];
            $periodlabel = $cellArr['period']['label'];
         }
      }
      $lines = [];
      $pageInfo = [];
      $pageInfo = [["organisation"=>$organisation->name],["planboard"=>$planning->name],["period"=>$cell['period']['label']],["transport"=>$cell['transport']['label']]];
            
      $qrcode = "https://www.google.nl/maps/dir/";
      if ($startlocation['label'] !== "no_startlocation"){
         $line = [];
         $qrcode .= Arr::get($startlocation, 'street')." ";
         $qrcode .= Arr::get($startlocation, 'zipcode')." ";
         $qrcode .= Arr::get($startlocation, 'city')." ";
         $qrcode .= Arr::get($startlocation, 'country')." ";
         $qrcode .= "/";
         $lines[] = ["label"=>Arr::get($startlocation, 'label')];
         $lines[] = ["street"=>Arr::get($startlocation, 'street')];
         $lines[] = ["zipcode"=>Arr::get($startlocation, 'zipcode')];
         $lines[] = ["city"=>Arr::get($startlocation, 'city')];
         $lines[] = ["country"=>Arr::get($startlocation, 'country')];
         $lines[] = ['break'=>''];
         $lines[] = ['break'=>''];
         $origin = Arr::get($startlocation, 'street')."+".Arr::get($startlocation, 'city')." "; 
      }        
      foreach($cell['locations'] as $index => $location){
               
         if ($origin === null){
            $origin = Arr::get($location['address'], 'street')."+".Arr::get($location['address'], 'city');
         }
         else{
            $waypoints[] = Arr::get($location['address'], 'street')."+".Arr::get($location['address'], 'city');
         }
         if(count($location['orders']) == 0){
            $line = [];
            $lines[] = ["label"=>Arr::get($location['address'], 'label')];
            $lines[] = ["street"=>Arr::get($location['address'], 'street')];
            $lines[] = ["zipcode"=>Arr::get($location['address'], 'zipcode')];
            $lines[] = ["city"=>Arr::get($location['address'], 'city')];
            $lines[] = ["country"=>Arr::get($location['address'], 'country')];
            $lines[] = ['break'=>''];
            if ($index == 0){
               $lines[] = ['break'=>''];
            }
         }else{
            $line = [];
            $lines[] = ["label"=>Arr::get($location['address'], 'label')];
            $lines[] = ["street"=>Arr::get($location['address'], 'street')];
            $lines[] = ["zipcode"=>Arr::get($location['address'], 'zipcode')];
            $lines[] = ["city"=>Arr::get($location['address'], 'city')];
            $lines[] = ["country"=>Arr::get($location['address'], 'country')];
            $lines[] = ['break'=>''];
            if ($index == 0){
               $lines[] = ['break'=>''];
            }
            $lines[] = ["header"=>"Order items"];
            $lines[] = ["itemlabelhead"=>"Label",
               "codehead"=>"Code",
               "customerhead"=>"Commission"];
            foreach($location['orders'] as $order){
               //   $lines[] = ["tablehead"=>"product   code   commission"];
               //$lines[] = ["order"=>Arr::get($order, 'label').Arr::get($order, 'code').Arr::get($order, 'customer')];
               $lines[] = ["itemlabel"=>Arr::get($order, 'label'),
                           "code"=>Arr::get($order, 'code'),
                           "customer"=>Arr::get($order, 'customer')];
                  //   $lines[] = $line;
               }
               $lines[] = ['break'=>''];
               $lines[] = ['break'=>''];   
               }
               $qrcode .= Arr::get($location['address'], 'street')." ";
               $qrcode .= Arr::get($location['address'], 'zipcode')." ";
               $qrcode .= Arr::get($location['address'], 'city')." ";
               $qrcode .= Arr::get($location['address'], 'country')." ";
               $qrcode .= "/";    
            }
            if(isset($cell['transport']['circular'])){
               
               $line = [];
               $qrcode .= Arr::get($startlocation, 'street')." ";
               $qrcode .= Arr::get($startlocation, 'zipcode')." ";
               $qrcode .= Arr::get($startlocation, 'city')." ";
               $qrcode .= Arr::get($startlocation, 'country');
               $lines[] = ["street"=>Arr::get($startlocation, 'street')];
               $lines[] = ["zipcode"=>Arr::get($startlocation, 'zipcode')];
               $lines[] = ["city"=>Arr::get($startlocation, 'city')];
               $lines[] = ["country"=>Arr::get($startlocation, 'country')];
              
               $destination = $origin;
                
            }else{
              
               $destination = $waypoints[count($waypoints)-1];
               unset($waypoints[count($waypoints)-1]);
            }
            //dd($origin,$destination);
            //dd($pageInfo,$lines);
            $url .= 'origin='.$origin.'&waypoints=';
            foreach($waypoints as $waypoint){
               $url .= $waypoint.'|';
            }
            $url .= '&destination='.$destination;
            //$url = urlencode($url); 
           
            $url .= "&mode=driving&key=".env('GOOGLE_MAPS_KEY');
            
            $url = str_replace(' ', '', $url);
            $url = str_replace('%7C', '7c', $url);
            //dd($url);
            $polyline = Http::withHeaders([
               'Content-Type' => 'application/pdf',
               'Accept' => 'application/pdf',
               'Authorization' => 'Bearer zE10WhWIdddJYCKe2UlywbeTJHEd9FWS3VpIfyhHQqty6CG7l3lZRQhXRGyslZ3caqCNIycFBNMqO5rH'
            ])->get($url);
            //dd($polyline['routes']);
            //return "<pre>$polyline<pre>";
            if(isset($polyline['routes'][0])){
               $polyline = $polyline['routes'][0]['overview_polyline']['points'];
               }else{
                  $polyline = "";
            }
            $url = "https://maps.googleapis.com/maps/api/staticmap?size=960x600&path=enc%3A".$polyline."&key=".env('GOOGLE_MAPS_KEY');
            //dd($url);
            $img = Http::withHeaders([
                'Content-Type' => 'application/pdf',
                'Accept' => 'application/pdf',
                'Authorization' => 'Bearer zE10WhWIdddJYCKe2UlywbeTJHEd9FWS3VpIfyhHQqty6CG7l3lZRQhXRGyslZ3caqCNIycFBNMqO5rH'
            ])->get($url);

            $img = 'data:image/' . 'png'. ';base64,' . base64_encode($img);
            //dd('data:image/' . 'png'. ';base64,' . base64_encode($logo));
            //return $logo;     
            $data = [
               "dynamicdata" => [
                  "lineset" => $lines,
                  "pageinfo"=> $pageInfo        
               ], 
               "version" => "0.1.0", 
               "title" => "Offer", 
               "pageLayouts" => [
                  [
                  "key" => "pagelayout1", 
                  "config" => [
                  "sheet-size" => "A4", 
                  "orientation" => "portrait", 
                  "margin_left" => 15, 
                  "margin_right" => 15, 
                  "margin_top" => 15, 
                  "margin_bottom" => 15, 
                  "margin_header" => 10, 
                  "margin_footer" => 15 
                  ], 
                  "elements" => [
                  [
                     "type" => "qrcode", 
                     "config" => [
                     "height" => "50", 
                     "x" => "145", 
                     "y" => "15", 
                     "contents" => $qrcode, 
                     "type" => "qr" 
                     ] 
                  ],
                  [
                     "type"=> "image",
                     "config"=> [
                        "width"=> "120",
                        "height"=> "75",
                        "x"=> "25",
                        "y"=> "18",
                        "contents"=> $img,
                        "uploaded"=> false,
                        "uploading"=> false
                        ],
                     "showUpload"=> false
                  ],
                  [
                     "type"=> "image",
                     "config"=> [
                        "width"=> "15",
                        "height"=> "15",
                        "x"=> "78",
                        "y"=> "285",
                        "contents"=> $logo,
                        "uploaded"=> false,
                        "uploading"=> false
                     ],
                     "showUpload"=> false
                  ],
                  [
                     "type" => "text", 
                     "config" => [
                        "x" => 91, 
                        "y" => 292, 
                        "contents" => 'Generated by Routetool', 
                        "font" => [
                           "Arial", 
                           false, 
                           10, 
                           false, 
                           false, 
                           false 
                        ], 
                        "colors" => [
                           "hsl" => [
                              "h" => 0, 
                              "s" => 0, 
                              "l" => 0, 
                              "a" => 1 
                           ], 
                           "hex" => "#000000", 
                           "hex8" => "#000000FF", 
                           "rgba" => [
                              "r" => 0, 
                              "g" => 0, 
                              "b" => 0, 
                              "a" => 1 
                           ], 
                           "hsv" => [
                              "h" => 0, 
                              "s" => 0, 
                              "v" => 0, 
                              "a" => 1 
                           ], 
                           "oldHue" => 0, 
                           "source" => "hsva", 
                           "a" => 1 
                        ], 
                        "tag" => false, 
                        "tag_text" => "" 
                     ] 
                  ]
               ], 
               "repeater" => [
                 [
                  "elements" => [
                     [
                     "type" => "text", 
                     "config" => [
                     "x" => 0, 
                     "y" => 0, 
                     "contents" => '$item.label', 
                     "font" => [
                        "Arial", 
                        true, 
                        11, 
                        false, 
                        false, 
                        false 
                     ], 
                     "colors" => [
                     "hsl" => [
                        "h" => 0, 
                        "s" => 0, 
                        "l" => 0, 
                        "a" => 1 
                        ], 
                        "hex" => "#000000", 
                        "hex8" => "#000000FF", 
                        "rgba" => [
                                                                  "r" => 0, 
                                                                  "g" => 0, 
                                                                  "b" => 0, 
                                                                  "a" => 1 
                                                               ], 
                                                            "hsv" => [
                                                                     "h" => 0, 
                                                                     "s" => 0, 
                                                                     "v" => 0, 
                                                                     "a" => 1 
                                                                  ], 
                                                            "oldHue" => 0, 
                                                            "source" => "hsva", 
                                                            "a" => 1 
                                                         ], 
                                                      "tag" => false, 
                                                      "tag_text" => "" 
                                                   ] 
                                                ],
                                                   [
                                                    "type" => "text", 
                                                    "config" => [
                                                       "x" => 0, 
                                                       "y" => 0, 
                                                       "contents" => '$item.street', 
                                                       "font" => [
                                                          "Arial", 
                                                          false, 
                                                          11, 
                                                          false, 
                                                          false, 
                                                          false 
                                                       ], 
                                                       "colors" => [
                                                             "hsl" => [
                                                                "h" => 0, 
                                                                "s" => 0, 
                                                                "l" => 0, 
                                                                "a" => 1 
                                                             ], 
                                                             "hex" => "#000000", 
                                                             "hex8" => "#000000FF", 
                                                             "rgba" => [
                                                                   "r" => 0, 
                                                                   "g" => 0, 
                                                                   "b" => 0, 
                                                                   "a" => 1 
                                                                ], 
                                                             "hsv" => [
                                                                      "h" => 0, 
                                                                      "s" => 0, 
                                                                      "v" => 0, 
                                                                      "a" => 1 
                                                                   ], 
                                                             "oldHue" => 0, 
                                                             "source" => "hsva", 
                                                             "a" => 1 
                                                          ], 
                                                       "tag" => false, 
                                                       "tag_text" => "" 
                                                    ] 
                                                 ], 
                                                 [
                                                                         "type" => "text", 
                                                                         "config" => [
                                                                            "x" => 0, 
                                                                            "y" => 0, 
                                                                            "contents" => '$item.city', 
                                                                            "font" => [
                                                                               "Arial", 
                                                                               false, 
                                                                               11, 
                                                                               false, 
                                                                               false, 
                                                                               false 
                                                                            ], 
                                                                            "colors" => [
                                                                                  "hsl" => [
                                                                                     "h" => 221.25, 
                                                                                     "s" => 0, 
                                                                                     "l" => 0, 
                                                                                     "a" => 1 
                                                                                  ], 
                                                                                  "hex" => "#000000", 
                                                                                  "hex8" => "#000000FF", 
                                                                                  "rgba" => [
                                                                                        "r" => 0, 
                                                                                        "g" => 0, 
                                                                                        "b" => 0, 
                                                                                        "a" => 1 
                                                                                     ], 
                                                                                  "hsv" => [
                                                                                           "h" => 221.25, 
                                                                                           "s" => 0, 
                                                                                           "v" => 0, 
                                                                                           "a" => 1 
                                                                                        ], 
                                                                                  "oldHue" => 221.25, 
                                                                                  "source" => "hsva", 
                                                                                  "a" => 1 
                                                                               ], 
                                                                            "tag" => false, 
                                                                            "tag_text" => "" 
                                                                         ] 
                                                                      ], 
                                                 [
                                                                                              "type" => "text", 
                                                                                              "config" => [
                                                                                                 "x" => 0, 
                                                                                                 "y" => 0, 
                                                                                                 "contents" => '$item.itemlabel', 
                                                                                                 "font" => [
                                                                                                    "Arial", 
                                                                                                    false, 
                                                                                                    "10", 
                                                                                                    false, 
                                                                                                    false, 
                                                                                                    false 
                                                                                                 ], 
                                                                                                 "colors" => [
                                                                                                       "hsl" => [
                                                                                                          "h" => 0, 
                                                                                                          "s" => 0, 
                                                                                                          "l" => 0.0427, 
                                                                                                          "a" => 1 
                                                                                                       ], 
                                                                                                       "hex" => "#0B0B0B", 
                                                                                                       "hex8" => "#0B0B0BFF", 
                                                                                                       "rgba" => [
                                                                                                             "r" => 11, 
                                                                                                             "g" => 11, 
                                                                                                             "b" => 11, 
                                                                                                             "a" => 1 
                                                                                                          ], 
                                                                                                       "hsv" => [
                                                                                                                "h" => 0, 
                                                                                                                "s" => 0, 
                                                                                                                "v" => 0.0427, 
                                                                                                                "a" => 1 
                                                                                                             ], 
                                                                                                       "oldHue" => 0, 
                                                                                                       "source" => "hsva", 
                                                                                                       "a" => 1 
                                                                                                    ], 
                                                                                                 "tag" => false, 
                                                                                                 "tag_text" => "" 
                                                                                              ] 
                                                                                           ],
                                                                                           [
                                                                                             "type" => "text", 
                                                                                             "config" => [
                                                                                                "x" => 0, 
                                                                                                "y" => 0, 
                                                                                                "contents" => '$item.itemlabelhead', 
                                                                                                "font" => [
                                                                                                   "Arial", 
                                                                                                   true, 
                                                                                                   "11", 
                                                                                                   false, 
                                                                                                   false, 
                                                                                                   false 
                                                                                                ], 
                                                                                                "colors" => [
                                                                                                      "hsl" => [
                                                                                                         "h" => 0, 
                                                                                                         "s" => 0, 
                                                                                                         "l" => 0.0427, 
                                                                                                         "a" => 1 
                                                                                                      ], 
                                                                                                      "hex" => "#0B0B0B", 
                                                                                                      "hex8" => "#0B0B0BFF", 
                                                                                                      "rgba" => [
                                                                                                            "r" => 11, 
                                                                                                            "g" => 11, 
                                                                                                            "b" => 11, 
                                                                                                            "a" => 1 
                                                                                                         ], 
                                                                                                      "hsv" => [
                                                                                                               "h" => 0, 
                                                                                                               "s" => 0, 
                                                                                                               "v" => 0.0427, 
                                                                                                               "a" => 1 
                                                                                                            ], 
                                                                                                      "oldHue" => 0, 
                                                                                                      "source" => "hsva", 
                                                                                                      "a" => 1 
                                                                                                   ], 
                                                                                                "tag" => false, 
                                                                                                "tag_text" => "" 
                                                                                             ] 
                                                                                          ], 
                                                 [
                                                                                                                   "type" => "text", 
                                                                                                                   "config" => [
                                                                                                                      "x" => 0, 
                                                                                                                      "y" => 0, 
                                                                                                                      "contents" => '$item.country', 
                                                                                                                      "font" => [
                                                                                                                         "Arial", 
                                                                                                                         false, 
                                                                                                                         11, 
                                                                                                                         false, 
                                                                                                                         false, 
                                                                                                                         false 
                                                                                                                      ], 
                                                                                                                      "colors" => [
                                                                                                                            "hsl" => [
                                                                                                                               "h" => 0, 
                                                                                                                               "s" => 0, 
                                                                                                                               "l" => 0.0578, 
                                                                                                                               "a" => 1 
                                                                                                                            ], 
                                                                                                                            "hex" => "#0F0F0F", 
                                                                                                                            "hex8" => "#0F0F0FFF", 
                                                                                                                            "rgba" => [
                                                                                                                                  "r" => 15, 
                                                                                                                                  "g" => 15, 
                                                                                                                                  "b" => 15, 
                                                                                                                                  "a" => 1 
                                                                                                                               ], 
                                                                                                                            "hsv" => [
                                                                                                                                     "h" => 0, 
                                                                                                                                     "s" => 0, 
                                                                                                                                     "v" => 0.0578, 
                                                                                                                                     "a" => 1 
                                                                                                                                  ], 
                                                                                                                            "oldHue" => 0, 
                                                                                                                            "source" => "hsva", 
                                                                                                                            "a" => 1 
                                                                                                                         ], 
                                                                                                                      "tag" => false, 
                                                                                                                      "tag_text" => "" 
                                                                                                                   ] 
                                                                                                                ], 
                                                 [
                                                                                                                                        "type" => "text", 
                                                                                                                                        "config" => [
                                                                                                                                           "x" => 0, 
                                                                                                                                           "y" => 0, 
                                                                                                                                           "contents" => '$item.zipcode', 
                                                                                                                                           "font" => [
                                                                                                                                              "Arial", 
                                                                                                                                              false, 
                                                                                                                                              11, 
                                                                                                                                              false, 
                                                                                                                                              false, 
                                                                                                                                              false 
                                                                                                                                           ], 
                                                                                                                                           "colors" => [
                                                                                                                                                 "hsl" => [
                                                                                                                                                    "h" => 0, 
                                                                                                                                                    "s" => 0, 
                                                                                                                                                    "l" => 0, 
                                                                                                                                                    "a" => 1 
                                                                                                                                                 ], 
                                                                                                                                                 "hex" => "#000000", 
                                                                                                                                                 "hex8" => "#000000FF", 
                                                                                                                                                 "rgba" => [
                                                                                                                                                       "r" => 0, 
                                                                                                                                                       "g" => 0, 
                                                                                                                                                       "b" => 0, 
                                                                                                                                                       "a" => 1 
                                                                                                                                                    ], 
                                                                                                                                                 "hsv" => [
                                                                                                                                                          "h" => 0, 
                                                                                                                                                          "s" => 0, 
                                                                                                                                                          "v" => 0, 
                                                                                                                                                          "a" => 1 
                                                                                                                                                       ], 
                                                                                                                                                 "oldHue" => 0, 
                                                                                                                                                 "source" => "hsva", 
                                                                                                                                                 "a" => 1 
                                                                                                                                              ], 
                                                                                                                                           "tag" => false, 
                                                                                                                                           "tag_text" => "" 
                                                                                                                                        ] 
                                                                                                                                     ], 
                                                 [
                                                                                                                                                             "type" => "text", 
                                                                                                                                                             "config" => [
                                                                                                                                                                "x" => 70, 
                                                                                                                                                                "y" => 0, 
                                                                                                                                                                "contents" => '$item.customerhead', 
                                                                                                                                                                "font" => [
                                                                                                                                                                   "Arial", 
                                                                                                                                                                   true, 
                                                                                                                                                                   11, 
                                                                                                                                                                   false, 
                                                                                                                                                                   false, 
                                                                                                                                                                   false 
                                                                                                                                                                ], 
                                                                                                                                                                "colors" => [
                                                                                                                                                                      "hex" => "#000000" 
                                                                                                                                                                   ], 
                                                                                                                                                                "tag" => false, 
                                                                                                                                                                "tag_text" => "" 
                                                                                                                                                             ] 
                                                                                                                                                          ],
                                                                                                                                                          [
                                                                                                                                                            "type" => "text", 
                                                                                                                                                            "config" => [
                                                                                                                                                               "x" => 70, 
                                                                                                                                                               "y" => 0, 
                                                                                                                                                               "contents" => '$item.customer', 
                                                                                                                                                               "font" => [
                                                                                                                                                                  "Arial", 
                                                                                                                                                                  false, 
                                                                                                                                                                  10, 
                                                                                                                                                                  false, 
                                                                                                                                                                  false, 
                                                                                                                                                                  false 
                                                                                                                                                               ], 
                                                                                                                                                               "colors" => [
                                                                                                                                                                     "hex" => "#000000" 
                                                                                                                                                                  ], 
                                                                                                                                                               "tag" => false, 
                                                                                                                                                               "tag_text" => "" 
                                                                                                                                                            ] 
                                                                                                                                                         ], 
                                                 [
                                                                                                                                                                         "type" => "text", 
                                                                                                                                                                         "config" => [
                                                                                                                                                                            "x" => 133, 
                                                                                                                                                                            "y" => 0, 
                                                                                                                                                                            "contents" => '$item.code', 
                                                                                                                                                                            "font" => [
                                                                                                                                                                               "Arial", 
                                                                                                                                                                               false, 
                                                                                                                                                                               10, 
                                                                                                                                                                               false, 
                                                                                                                                                                               false, 
                                                                                                                                                                               false 
                                                                                                                                                                            ], 
                                                                                                                                                                            "colors" => [
                                                                                                                                                                                  "hex" => "#000000" 
                                                                                                                                                                               ], 
                                                                                                                                                                            "tag" => false, 
                                                                                                                                                                            "tag_text" => "" 
                                                                                                                                                                         ] 
                                                                                                                                                                      ],
                                                                                                                                                                      [
                                                                                                                                                                        "type" => "text", 
                                                                                                                                                                        "config" => [
                                                                                                                                                                           "x" => 133, 
                                                                                                                                                                           "y" => 0, 
                                                                                                                                                                           "contents" => '$item.codehead', 
                                                                                                                                                                           "font" => [
                                                                                                                                                                              "Arial", 
                                                                                                                                                                              true, 
                                                                                                                                                                              11, 
                                                                                                                                                                              false, 
                                                                                                                                                                              false, 
                                                                                                                                                                              false 
                                                                                                                                                                           ], 
                                                                                                                                                                           "colors" => [
                                                                                                                                                                                 "hex" => "#000000" 
                                                                                                                                                                              ], 
                                                                                                                                                                           "tag" => false, 
                                                                                                                                                                           "tag_text" => "" 
                                                                                                                                                                        ] 
                                                                                                                                                                           ], 
                                                 [
                                                                                                                                                                                     "type" => "text", 
                                                                                                                                                                                     "config" => [
                                                                                                                                                                                        "x" => "125", 
                                                                                                                                                                                        "y" => 0, 
                                                                                                                                                                                        "contents" => '$item.commission', 
                                                                                                                                                                                        "font" => [
                                                                                                                                                                                           "Arial", 
                                                                                                                                                                                           false, 
                                                                                                                                                                                           11, 
                                                                                                                                                                                           false, 
                                                                                                                                                                                           false, 
                                                                                                                                                                                           false 
                                                                                                                                                                                        ], 
                                                                                                                                                                                        "colors" => [
                                                                                                                                                                                              "hex" => "#000000" 
                                                                                                                                                                                           ], 
                                                                                                                                                                                        "tag" => false, 
                                                                                                                                                                                        "tag_text" => "" 
                                                                                                                                                                                     ] 
                                                                                                                                                                                  ], 
                                                 [
                                                                                                                                                                                                 "type" => "text", 
                                                                                                                                                                                                 "config" => [
                                                                                                                                                                                                    "x" => 0, 
                                                                                                                                                                                                    "y" => 0, 
                                                                                                                                                                                                    "contents" => '$item.break', 
                                                                                                                                                                                                    "font" => [
                                                                                                                                                                                                       "Arial", 
                                                                                                                                                                                                       false, 
                                                                                                                                                                                                       11, 
                                                                                                                                                                                                       false, 
                                                                                                                                                                                                       false, 
                                                                                                                                                                                                       false 
                                                                                                                                                                                                    ], 
                                                                                                                                                                                                    "colors" => [
                                                                                                                                                                                                          "hex" => "#000000" 
                                                                                                                                                                                                       ], 
                                                                                                                                                                                                    "tag" => false, 
                                                                                                                                                                                                    "tag_text" => "" 
                                                                                                                                                                                                 ] 
                                                                                                                                                                                              ], 
                                                 [
                                                                                                                                                                                                             "type" => "text", 
                                                                                                                                                                                                             "config" => [
                                                                                                                                                                                                                "x" => 0, 
                                                                                                                                                                                                                "y" => 0, 
                                                                                                                                                                                                                "contents" => '$item.header', 
                                                                                                                                                                                                                "font" => [
                                                                                                                                                                                                                   "Arial", 
                                                                                                                                                                                                                   true, 
                                                                                                                                                                                                                   11, 
                                                                                                                                                                                                                   false, 
                                                                                                                                                                                                                   true, 
                                                                                                                                                                                                                   false 
                                                                                                                                                                                                                ], 
                                                                                                                                                                                                                "colors" => [
                                                                                                                                                                                                                      "hex" => "#000000" 
                                                                                                                                                                                                                   ], 
                                                                                                                                                                                                                "tag" => false, 
                                                                                                                                                                                                                "tag_text" => "" 
                                                                                                                                                                                                             ] 
                                                                                                                                                                                                          ] 
                                              ], 
                                              "height" => "5", 
                                              "x" => "25", 
                                              "y" => "100", 
                                              "contents" => '$data.lineset', 
                                              "blockHeight" => 200 
                                          ],
                                          [
                                             "elements" => [
                                               [
                                                  "type" => "text", 
                                                  "config" => [
                                                     "x" => 0, 
                                                     "y" => 0, 
                                                     "contents" => '$item.organisation', 
                                                     "font" => [
                                                        "Arial", 
                                                        true, 
                                                        11, 
                                                        false, 
                                                        false, 
                                                        false 
                                                     ], 
                                                     "colors" => [
                                                           "hsl" => [
                                                              "h" => 0, 
                                                              "s" => 0, 
                                                              "l" => 0, 
                                                              "a" => 1 
                                                           ], 
                                                           "hex" => "#000000", 
                                                           "hex8" => "#000000FF", 
                                                           "rgba" => [
                                                                 "r" => 0, 
                                                                 "g" => 0, 
                                                                 "b" => 0, 
                                                                 "a" => 1 
                                                              ], 
                                                           "hsv" => [
                                                                    "h" => 0, 
                                                                    "s" => 0, 
                                                                    "v" => 0, 
                                                                    "a" => 1 
                                                                 ], 
                                                           "oldHue" => 0, 
                                                           "source" => "hsva", 
                                                           "a" => 1 
                                                        ], 
                                                     "tag" => false, 
                                                     "tag_text" => "" 
                                                  ] 
                                               ],
                                                  [
                                                   "type" => "text", 
                                                   "config" => [
                                                      "x" => 0, 
                                                      "y" => 0, 
                                                      "contents" => '$item.planboard', 
                                                      "font" => [
                                                         "Arial", 
                                                         false, 
                                                         11, 
                                                         false, 
                                                         false, 
                                                         false 
                                                      ], 
                                                      "colors" => [
                                                            "hsl" => [
                                                               "h" => 0, 
                                                               "s" => 0, 
                                                               "l" => 0, 
                                                               "a" => 1 
                                                            ], 
                                                            "hex" => "#000000", 
                                                            "hex8" => "#000000FF", 
                                                            "rgba" => [
                                                                  "r" => 0, 
                                                                  "g" => 0, 
                                                                  "b" => 0, 
                                                                  "a" => 1 
                                                               ], 
                                                            "hsv" => [
                                                                     "h" => 0, 
                                                                     "s" => 0, 
                                                                     "v" => 0, 
                                                                     "a" => 1 
                                                                  ], 
                                                            "oldHue" => 0, 
                                                            "source" => "hsva", 
                                                            "a" => 1 
                                                         ], 
                                                      "tag" => false, 
                                                      "tag_text" => "" 
                                                   ] 
                                                ], 
                                                [
                                                                        "type" => "text", 
                                                                        "config" => [
                                                                           "x" => 0, 
                                                                           "y" => 0, 
                                                                           "contents" => '$item.period', 
                                                                           "font" => [
                                                                              "Arial", 
                                                                              false, 
                                                                              11, 
                                                                              false, 
                                                                              false, 
                                                                              false 
                                                                           ], 
                                                                           "colors" => [
                                                                                 "hsl" => [
                                                                                    "h" => 221.25, 
                                                                                    "s" => 0, 
                                                                                    "l" => 0, 
                                                                                    "a" => 1 
                                                                                 ], 
                                                                                 "hex" => "#000000", 
                                                                                 "hex8" => "#000000FF", 
                                                                                 "rgba" => [
                                                                                       "r" => 0, 
                                                                                       "g" => 0, 
                                                                                       "b" => 0, 
                                                                                       "a" => 1 
                                                                                    ], 
                                                                                 "hsv" => [
                                                                                          "h" => 221.25, 
                                                                                          "s" => 0, 
                                                                                          "v" => 0, 
                                                                                          "a" => 1 
                                                                                       ], 
                                                                                 "oldHue" => 221.25, 
                                                                                 "source" => "hsva", 
                                                                                 "a" => 1 
                                                                              ], 
                                                                           "tag" => false, 
                                                                           "tag_text" => "" 
                                                                        ] 
                                                                     ], 
                                                [
                                                                                             "type" => "text", 
                                                                                             "config" => [
                                                                                                "x" => 0, 
                                                                                                "y" => 0, 
                                                                                                "contents" => '$item.transport', 
                                                                                                "font" => [
                                                                                                   "Arial", 
                                                                                                   false, 
                                                                                                   "10", 
                                                                                                   false, 
                                                                                                   false, 
                                                                                                   false 
                                                                                                ], 
                                                                                                "colors" => [
                                                                                                      "hsl" => [
                                                                                                         "h" => 0, 
                                                                                                         "s" => 0, 
                                                                                                         "l" => 0.0427, 
                                                                                                         "a" => 1 
                                                                                                      ], 
                                                                                                      "hex" => "#0B0B0B", 
                                                                                                      "hex8" => "#0B0B0BFF", 
                                                                                                      "rgba" => [
                                                                                                            "r" => 11, 
                                                                                                            "g" => 11, 
                                                                                                            "b" => 11, 
                                                                                                            "a" => 1 
                                                                                                         ], 
                                                                                                      "hsv" => [
                                                                                                               "h" => 0, 
                                                                                                               "s" => 0, 
                                                                                                               "v" => 0.0427, 
                                                                                                               "a" => 1 
                                                                                                            ], 
                                                                                                      "oldHue" => 0, 
                                                                                                      "source" => "hsva", 
                                                                                                      "a" => 1 
                                                                                                   ], 
                                                                                                "tag" => false, 
                                                                                                "tag_text" => "" 
                                                                                             ] 
                                                                                          ],
                                                                                          [
                                                                                            "type" => "text", 
                                                                                            "config" => [
                                                                                               "x" => 0, 
                                                                                               "y" => 0, 
                                                                                               "contents" => '$item.itemlabelhead', 
                                                                                               "font" => [
                                                                                                  "Arial", 
                                                                                                  true, 
                                                                                                  "11", 
                                                                                                  false, 
                                                                                                  false, 
                                                                                                  false 
                                                                                               ], 
                                                                                               "colors" => [
                                                                                                     "hsl" => [
                                                                                                        "h" => 0, 
                                                                                                        "s" => 0, 
                                                                                                        "l" => 0.0427, 
                                                                                                        "a" => 1 
                                                                                                     ], 
                                                                                                     "hex" => "#0B0B0B", 
                                                                                                     "hex8" => "#0B0B0BFF", 
                                                                                                     "rgba" => [
                                                                                                           "r" => 11, 
                                                                                                           "g" => 11, 
                                                                                                           "b" => 11, 
                                                                                                           "a" => 1 
                                                                                                        ], 
                                                                                                     "hsv" => [
                                                                                                              "h" => 0, 
                                                                                                              "s" => 0, 
                                                                                                              "v" => 0.0427, 
                                                                                                              "a" => 1 
                                                                                                           ], 
                                                                                                     "oldHue" => 0, 
                                                                                                     "source" => "hsva", 
                                                                                                     "a" => 1 
                                                                                                  ], 
                                                                                               "tag" => false, 
                                                                                               "tag_text" => "" 
                                                                                            ] 
                                                                                         ], 
                                                [
                                                                                                                  "type" => "text", 
                                                                                                                  "config" => [
                                                                                                                     "x" => 0, 
                                                                                                                     "y" => 0, 
                                                                                                                     "contents" => '$item.country', 
                                                                                                                     "font" => [
                                                                                                                        "Arial", 
                                                                                                                        false, 
                                                                                                                        11, 
                                                                                                                        false, 
                                                                                                                        false, 
                                                                                                                        false 
                                                                                                                     ], 
                                                                                                                     "colors" => [
                                                                                                                           "hsl" => [
                                                                                                                              "h" => 0, 
                                                                                                                              "s" => 0, 
                                                                                                                              "l" => 0.0578, 
                                                                                                                              "a" => 1 
                                                                                                                           ], 
                                                                                                                           "hex" => "#0F0F0F", 
                                                                                                                           "hex8" => "#0F0F0FFF", 
                                                                                                                           "rgba" => [
                                                                                                                                 "r" => 15, 
                                                                                                                                 "g" => 15, 
                                                                                                                                 "b" => 15, 
                                                                                                                                 "a" => 1 
                                                                                                                              ], 
                                                                                                                           "hsv" => [
                                                                                                                                    "h" => 0, 
                                                                                                                                    "s" => 0, 
                                                                                                                                    "v" => 0.0578, 
                                                                                                                                    "a" => 1 
                                                                                                                                 ], 
                                                                                                                           "oldHue" => 0, 
                                                                                                                           "source" => "hsva", 
                                                                                                                           "a" => 1 
                                                                                                                        ], 
                                                                                                                     "tag" => false, 
                                                                                                                     "tag_text" => "" 
                                                                                                                  ] 
                                                                                                               ], 
                                                [
                                                                                                                                       "type" => "text", 
                                                                                                                                       "config" => [
                                                                                                                                          "x" => 0, 
                                                                                                                                          "y" => 0, 
                                                                                                                                          "contents" => '$item.zipcode', 
                                                                                                                                          "font" => [
                                                                                                                                             "Arial", 
                                                                                                                                             false, 
                                                                                                                                             11, 
                                                                                                                                             false, 
                                                                                                                                             false, 
                                                                                                                                             false 
                                                                                                                                          ], 
                                                                                                                                          "colors" => [
                                                                                                                                                "hsl" => [
                                                                                                                                                   "h" => 0, 
                                                                                                                                                   "s" => 0, 
                                                                                                                                                   "l" => 0, 
                                                                                                                                                   "a" => 1 
                                                                                                                                                ], 
                                                                                                                                                "hex" => "#000000", 
                                                                                                                                                "hex8" => "#000000FF", 
                                                                                                                                                "rgba" => [
                                                                                                                                                      "r" => 0, 
                                                                                                                                                      "g" => 0, 
                                                                                                                                                      "b" => 0, 
                                                                                                                                                      "a" => 1 
                                                                                                                                                   ], 
                                                                                                                                                "hsv" => [
                                                                                                                                                         "h" => 0, 
                                                                                                                                                         "s" => 0, 
                                                                                                                                                         "v" => 0, 
                                                                                                                                                         "a" => 1 
                                                                                                                                                      ], 
                                                                                                                                                "oldHue" => 0, 
                                                                                                                                                "source" => "hsva", 
                                                                                                                                                "a" => 1 
                                                                                                                                             ], 
                                                                                                                                          "tag" => false, 
                                                                                                                                          "tag_text" => "" 
                                                                                                                                       ] 
                                                                                                                                    ], 
                                                [
                                                                                                                                                            "type" => "text", 
                                                                                                                                                            "config" => [
                                                                                                                                                               "x" => 70, 
                                                                                                                                                               "y" => 0, 
                                                                                                                                                               "contents" => '$item.customerhead', 
                                                                                                                                                               "font" => [
                                                                                                                                                                  "Arial", 
                                                                                                                                                                  true, 
                                                                                                                                                                  10, 
                                                                                                                                                                  false, 
                                                                                                                                                                  false, 
                                                                                                                                                                  false 
                                                                                                                                                               ], 
                                                                                                                                                               "colors" => [
                                                                                                                                                                     "hex" => "#000000" 
                                                                                                                                                                  ], 
                                                                                                                                                               "tag" => false, 
                                                                                                                                                               "tag_text" => "" 
                                                                                                                                                            ] 
                                                                                                                                                         ],
                                                                                                                                                         [
                                                                                                                                                           "type" => "text", 
                                                                                                                                                           "config" => [
                                                                                                                                                              "x" => 70, 
                                                                                                                                                              "y" => 0, 
                                                                                                                                                              "contents" => '$item.customer', 
                                                                                                                                                              "font" => [
                                                                                                                                                                 "Arial", 
                                                                                                                                                                 false, 
                                                                                                                                                                 10, 
                                                                                                                                                                 false, 
                                                                                                                                                                 false, 
                                                                                                                                                                 false 
                                                                                                                                                              ], 
                                                                                                                                                              "colors" => [
                                                                                                                                                                    "hex" => "#000000" 
                                                                                                                                                                 ], 
                                                                                                                                                              "tag" => false, 
                                                                                                                                                              "tag_text" => "" 
                                                                                                                                                           ] 
                                                                                                                                                        ], 
                                                [
                                                                                                                                                                        "type" => "text", 
                                                                                                                                                                        "config" => [
                                                                                                                                                                           "x" => 133, 
                                                                                                                                                                           "y" => 0, 
                                                                                                                                                                           "contents" => '$item.code', 
                                                                                                                                                                           "font" => [
                                                                                                                                                                              "Arial", 
                                                                                                                                                                              false, 
                                                                                                                                                                              10, 
                                                                                                                                                                              false, 
                                                                                                                                                                              false, 
                                                                                                                                                                              false 
                                                                                                                                                                           ], 
                                                                                                                                                                           "colors" => [
                                                                                                                                                                                 "hex" => "#000000" 
                                                                                                                                                                              ], 
                                                                                                                                                                           "tag" => false, 
                                                                                                                                                                           "tag_text" => "" 
                                                                                                                                                                        ] 
                                                                                                                                                                     ],
                                                                                                                                                                     [
                                                                                                                                                                       "type" => "text", 
                                                                                                                                                                       "config" => [
                                                                                                                                                                          "x" => 133, 
                                                                                                                                                                          "y" => 0, 
                                                                                                                                                                          "contents" => '$item.codehead', 
                                                                                                                                                                          "font" => [
                                                                                                                                                                             "Arial", 
                                                                                                                                                                             true, 
                                                                                                                                                                             10, 
                                                                                                                                                                             false, 
                                                                                                                                                                             false, 
                                                                                                                                                                             false 
                                                                                                                                                                          ], 
                                                                                                                                                                          "colors" => [
                                                                                                                                                                                "hex" => "#000000" 
                                                                                                                                                                             ], 
                                                                                                                                                                          "tag" => false, 
                                                                                                                                                                          "tag_text" => "" 
                                                                                                                                                                       ] 
                                                                                                                                                                          ], 
                                                [
                                                                                                                                                                                    "type" => "text", 
                                                                                                                                                                                    "config" => [
                                                                                                                                                                                       "x" => "125", 
                                                                                                                                                                                       "y" => 0, 
                                                                                                                                                                                       "contents" => '$item.commission', 
                                                                                                                                                                                       "font" => [
                                                                                                                                                                                          "Arial", 
                                                                                                                                                                                          false, 
                                                                                                                                                                                          11, 
                                                                                                                                                                                          false, 
                                                                                                                                                                                          false, 
                                                                                                                                                                                          false 
                                                                                                                                                                                       ], 
                                                                                                                                                                                       "colors" => [
                                                                                                                                                                                             "hex" => "#000000" 
                                                                                                                                                                                          ], 
                                                                                                                                                                                       "tag" => false, 
                                                                                                                                                                                       "tag_text" => "" 
                                                                                                                                                                                    ] 
                                                                                                                                                                                 ], 
                                                [
                                                                                                                                                                                                "type" => "text", 
                                                                                                                                                                                                "config" => [
                                                                                                                                                                                                   "x" => 0, 
                                                                                                                                                                                                   "y" => 0, 
                                                                                                                                                                                                   "contents" => '$item.break', 
                                                                                                                                                                                                   "font" => [
                                                                                                                                                                                                      "Arial", 
                                                                                                                                                                                                      false, 
                                                                                                                                                                                                      11, 
                                                                                                                                                                                                      false, 
                                                                                                                                                                                                      false, 
                                                                                                                                                                                                      false 
                                                                                                                                                                                                   ], 
                                                                                                                                                                                                   "colors" => [
                                                                                                                                                                                                         "hex" => "#000000" 
                                                                                                                                                                                                      ], 
                                                                                                                                                                                                   "tag" => false, 
                                                                                                                                                                                                   "tag_text" => "" 
                                                                                                                                                                                                ] 
                                                                                                                                                                                             ], 
                                                [
                                                                                                                                                                                                            "type" => "text", 
                                                                                                                                                                                                            "config" => [
                                                                                                                                                                                                               "x" => 0, 
                                                                                                                                                                                                               "y" => 0, 
                                                                                                                                                                                                               "contents" => '$item.header', 
                                                                                                                                                                                                               "font" => [
                                                                                                                                                                                                                  "Arial", 
                                                                                                                                                                                                                  true, 
                                                                                                                                                                                                                  11, 
                                                                                                                                                                                                                  false, 
                                                                                                                                                                                                                  true, 
                                                                                                                                                                                                                  false 
                                                                                                                                                                                                               ], 
                                                                                                                                                                                                               "colors" => [
                                                                                                                                                                                                                     "hex" => "#000000" 
                                                                                                                                                                                                                  ], 
                                                                                                                                                                                                               "tag" => false, 
                                                                                                                                                                                                               "tag_text" => "" 
                                                                                                                                                                                                            ] 
                                                                                                                                                                                                         ] 
                                             ], 
                                             "height" => "5", 
                                             "x" => "150", 
                                             "y" => "70", 
                                             "contents" => '$data.pageinfo', 
                                             "blockHeight" => 200 
                                          ] 
                                        ],
                                         
                         ] 
                      ], 
                "pages" => [
                                                                                                                                                                                                                            [
                                                                                                                                                                                                                               "layoutKey" => "pagelayout1" 
                                                                                                                                                                                                                            ] 
                                                                                                                                                                                                                         ] 
             ];
             //return $data;
             
            $response = Http::withHeaders([
                'Content-Type' => 'application/pdf',
                'Accept' => 'application/pdf',
                'Authorization' => 'Bearer zE10WhWIdddJYCKe2UlywbeTJHEd9FWS3VpIfyhHQqty6CG7l3lZRQhXRGyslZ3caqCNIycFBNMqO5rH'
            ])->post('https://documenttool.online/api/convert-only',$data);
         $filename = $organisation->id."/".$planning->id."/cell.pdf";

         Storage::disk('local')->put($filename, $response);
            //shell_exec('sudo curl -X POST -H "Content-Type: application/json" -H "Authorization: Bearer zE10WhWIdddJYCKe2UlywbeTJHEd9FWS3VpIfyhHQqty6CG7l3lZRQhXRGyslZ3caqCNIycFBNMqO5rH" -H "Accept: application/json" -d @../storage/app/test.json https://documenttool.online/api/convert-only --output ../storage/app/test-sudo.pdf');
        return $filename;
        //return Storage::download($filename,$organisation->name." - ".$planning->name."(".$transportlabel." - ".$periodlabel.").pdf");
       
    }
    public function exportPlanboardToPdf(Request $request,$id,$planningId){
      $organisation = $this->organisation->getOrganisationById($id);
      $planning = $this->planning->getPlanningById($planningId);
      //dd($organisation,$planning);
      $startlocation = false;
      $planningArr = json_decode($planning->routedata,true);
      $pageCount = 0;
      if ($planningArr['startlocation']){
         $startlocation = $planningArr['startlocation'];
      }
      $pdfs = "";
      foreach($planningArr['cells'] as $index => $cell){
         if(count($cell['locations']) > 1){
            $filename = $this->cellToPdf($id,$planningId,$cell['transport']['key'],$cell['period']['key']);
            Storage::delete($organisation->id."/".$planning->id."/".$pageCount.".pdf");
            Storage::move($filename,$organisation->id."/".$planning->id."/".$pageCount.".pdf");
            $pdfs .= "../storage/app/".$organisation->id."/".$planning->id."/".$pageCount.".pdf ";
            $pageCount++;
         }
      }
      Storage::delete($organisation->id.'/'.$planning->id.'/'.$planning->id.'.pdf');
      shell_exec('pdftk '.$pdfs.' cat output ../storage/app/'.$organisation->id.'/'.$planning->id.'/'.$planning->id.'.pdf');
      $filename = "../storage/app/".$organisation->id.'/'.$planning->id.'/'.$planning->id.'.pdf';
      $path = storage_path($filename);

      return Response::make(file_get_contents($path), 200, [
         'Content-Type' => 'application/pdf',
         'Content-Disposition' => 'inline; filename="'.$organisation->name.' - '.$planning->name.'.pdf"'
      ]);
    }
  public function getMap(){
   return storage::get('map.png');
  }
}