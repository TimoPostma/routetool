<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\ImportSettings;
use App\Organisation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Session;

class ImportSettingsController extends Controller
{
    public $importSettings;
    public $organisation;

    public function __construct(ImportSettings $importSettings, Organisation $organisation)
    {
        $this->importSettings = $importSettings;
        $this->organisation = $organisation;
    }

    public function getIndexPage($id)
    {
        $organisation = $this->organisation->getOrganisationById($id);

        if(!isset($organisation)) {
            abort(404);
        }

        if($organisation->user_id !== Auth()->user()->id) {
            Session::flash('warning', __('organisations.no_permission'));
            return back();
        }


        $importSettings = $this->importSettings->getImportSettingByOrganisationId($id);
        return view('user.organisations.import-settings.index', ['importSettings' => $importSettings, 'organisationId' => $id]);
    }

    public function create($id)
    {
        $organisation = $this->organisation->getOrganisationById($id);

        if(!isset($organisation)) {
            abort(404);
        }

        if($organisation->user_id !== Auth()->user()->id) {
            Session::flash('warning', __('organisations.no_permission'));
            return back();
        }

        return view('user.organisations.import-settings.create', ['organisationId' => $id]);
    }

    public function store(Request $request,$id)
    {
        $organisation = $this->organisation->getOrganisationById($id);

        if(!isset($organisation)) {
            abort(404);
        }

        if($organisation->user_id !== Auth()->user()->id) {
            Session::flash('warning', __('organisations.no_permission'));
            return back();
        }

        $request->validate([
            'name' => ['required'],
            'description' => ['required'],
            'address_header' => ['required'],
            'place_header' => ['required'],
            'period_header' => ['required'],
            'transport_header' => ['required'],
            'overhead_time_header' => ['required'],
            'item_time_header' => ['required'],
            'item_header' => ['required']
        ]);

        //Store the settings into the database
        try {
            $this->importSettings->storeImportSettings($request, $id);
        } catch (\Exception $exception) {
            Session::flash('error', __('organisations.could_not_save'));
            Log::error('Cannot save import settings to database' . ' ' . $exception->getMessage());
            return back();
        }
        return redirect()->route('import.settings.index', ['id' => $organisation->id]);
    }
}
