<?php

namespace App\Http\Controllers\User;

use App\Helpers\InviteUrlHelper;
use App\Helpers\MandrillHelper;
use App\Http\Controllers\Controller;
use App\Invitation;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class InvitationController extends Controller
{
    protected $invitation;
    protected $user;

    public function __construct(Invitation $invitation, User $user)
    {
        $this->invitation = $invitation;
        $this->user = $user;
    }


    public function getIndexPage()
    {
        return view('user.invitation.index');
    }

    public function generateInvitationUrl()
    {
        $userId = Auth()->user()->id;
        $invitation =  $this->invitation->getInvitation($userId);

        //If inviation is set and the link has not expired yet return an response

        if(isset($invitation) && Carbon::parse($invitation->expiration_date) > Carbon::now()) {
            $jsonData = ['status' => 'url found', 'url' => $invitation->url, 'message' => 'url found, return url'];
            return Response()->json($jsonData, 200);
        }

        $signature = sha1(Str::random(10));
        $expiration_date = Carbon::now()->addMonth();

        while ($this->invitation->checkIfSignatureExists($signature)) {
            $signature = sha1(Str::random(10));
        }

        $timestamp = $expiration_date->timestamp;

        $url = InviteUrlHelper::generateInvitationUrl($userId, $signature, $timestamp);

        $updateInvitation = false;

        if(isset($invitation)) {
            $updateInvitation = true;
        }

        //If invitation exists we only to update the existing relation

        try {
            if($updateInvitation) {
                $this->invitation->updateInvitation($invitation, $signature, $expiration_date, $url, $timestamp);
            } else {
                $this->invitation->storeInvitation($userId, $signature, $expiration_date, $url, $timestamp);
            }
        } catch (\Exception $exception) {
            Log::error('Cannot store or update the invitation url' . ' ' . $exception->getMessage());
            return Response()->json('error storing generated url. try again later', 500);
        }

        $jsonData = ['status' => 'url generated', 'url' => $url, 'message' => 'url is generated'];

        return Response()->json($jsonData, 200);
    }

    public function checkIfGeneratedLinkHasExpired()
    {
        $userId = Auth()->user()->id;
        $invitation =  $this->invitation->getInvitation($userId);

        $url = '';
        if(!isset($invitation)) {
            return Response()->json($url, 200);
        }

        if(Carbon::parse($invitation->expiration_date) < Carbon::now()) {
            return Response()->json($url, 200);
        }

        $url = $invitation->url;
        return Response()->json($url, 200);
    }

    public function inviteUserByEmail(Request $request)
    {
        $request->validate([
           'email' => [
               'email',
               'required'
           ],
            'firstName' => [
                'required'
            ],
            'lastName' => [
                'required'
            ]
        ]);


        //Check if the user already exists in the database
        $user = $this->user->getUserByEmail($request->email);

        if(isset($user)) {
            $jsonData = ['status' => 'success', 'message' => 'send email'];
            return Response()->json($jsonData, 200);
        }

        //Get the invitation Url
        $userId = Auth()->user()->id;
        $invitation = $this->invitation->getInvitation($userId);

        if(!isset($invitation)) {
            $jsonData = ['status' => 'error', 'not found' => 'No invitation found'];
            return Response()->json($jsonData, 404);
        }

        //TODO Send the actual email
        try {

            $template = 'Routetool invite';
            $fromMail = 'info@routetool.online';
            $fromName = 'routetool.online';
            $mergeVars = ['INVITEURL' => $invitation['url']];
            $toEmail = $request->email;
            $toName = $request->name;

            MandrillHelper::sendEmailWithoutAttachment($template, 'Check out the Routetool', $fromMail, $fromName, $mergeVars, $toEmail, $toName);

        } catch (\Exception $exception) {
            error_log($exception);
        }

        return Response()->json($invitation->url, 200);
    }
}
