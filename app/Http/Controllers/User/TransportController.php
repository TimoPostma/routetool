<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Transport;
use App\Organisation;
use Illuminate\Support\Facades\Auth;

class TransportController extends Controller
{   public $transport;
    public $organisation;
    public function __construct(Transport $transport, Organisation $organisation)
    {
        $this->transport = $transport;
        $this->organisation = $organisation;
     
    }
    public function getIndexPage($id)
    {   
        $organisation = $this->organisation->getOrganisationById($id);
        $transports = $this->transport->getTransportsByOrganisationId($id);

        return view('user.transports.index', compact('transports','id','organisation'));
    }

    public function create($id){
        $organisation = $this->organisation->getOrganisationById($id);
        return view('user.transports.create',compact('id','organisation'));
    }
    public function edit($id,$transportId){
        $organisation = $this->organisation->getOrganisationById($id);
        $transport = $this->transport->getTransportById($transportId);
        return view('user.transports.edit',compact('id','organisation','transport'));
    }

    public function store(Request $request){
 
        if (isset($request->transport_id)){
            $transport = $this->transport->getTransportById($request->id);
        }else if(isset($request->label)){
            $transport = $this->transport->getTransportByLabel($request->label);
            if($transport == null){
                $transport = new Transport();
            }
        }else{
            $transport = new Transport();
        }
        $organisation_id = Auth::user()->organisations[0]->id;
       
        $transportdata = [];
        if (isset($request->label)){
            $transport->label = $request->label;
        }
        if (isset($request->method)){
            $transportdata['method'] = $request->method;
                //$transport->speed = $request->overhead_time;
        }
        if (isset($request->speed)){
            $transportdata['speed'] = $request->speed;
                //$transport->method = $request->description;
        }
        if (isset($request->showMap)){
            $transportdata['showMap'] = $request->showMap;
                //$transport->method = $request->description;
        }
        if (isset($request->showTime)){
                $transportdata['showTime'] = $request->showTime;
                //$transport->method = $request->description;
        }
        if (isset($request->circular)){
                $transportdata['circular'] = $request->circular;
                //$transport->method = $request->description;
        }
        //dd($transport);
            //return $address;
        if ($transport->id){
            return $this->transport->updateTransport($transport,$organisation_id,$transportdata);
        }    
        return $this->transport->storeTransport($request,$organisation_id);
    }
    public function delete(Request $request){
        //$address = $this->address->getAddressById($request->id);
        //Route::get("organisation.address.index");
        $this->transport->deleteTransport($request->transportId);
        return redirect()->route("organisation.transport.index",["id"=>$request->organisationId,"transportId"=>$request->transportId]);

    }
    public function list($organisationId){
        return $this->transport->getTransportsByOrganisationId($organisationId);
    }
}
