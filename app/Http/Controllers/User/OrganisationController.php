<?php

namespace App\Http\Controllers\User;

use App\Helpers\InviteUrlHelper;
use App\Http\Controllers\Controller;
use App\InviteOrganisation;
use App\Mail\SendActivationEmail;
use App\Organisation;
use App\Planning;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Session;

class OrganisationController extends Controller
{
    public $organisation;
    public $user;
    public $planning;
    public $inviteOrganisation;

    public function __construct(Organisation $organisation, User $user, Planning $planning,InviteOrganisation $inviteOrganisation)
    {
        $this->organisation = $organisation;
        $this->user = $user;
        $this->planning = $planning;
        $this->inviteOrganisation = $inviteOrganisation;
    }

    public function getIndexPage()
    {
        $user = Auth()->user();
        $ownerOrganisations = $this->organisation->getOwnerOrganisationsByUserId($user->id);

        $invitedOrganisations = $user->organisationUser()->get();


        return view('user.organisations.index', compact('ownerOrganisations', 'invitedOrganisations'));
    }

    public function create()
    {
        return view('user.organisations.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required'],
            'description' => ['required'],
        ]);

        $userId = Auth()->user()->id;

        try {
            $this->organisation->storeOrganisation($request->name, $request->description, $userId);
        } catch (\Exception $exception) {
            Log::error('Error saving organisation to the database' . ' ' . $exception->getMessage());
            Session::flash('error', __('organisations.something_went_wrong'));
            return back();
        }
        Session::flash('success', __('organisations.organisation_added'));
        return redirect()->route('user.organisations.index');
    }
    public function getEditPage($id)
    {   
        $organisation = $this->organisation->getOrganisationById($id);
        return view('user.organisations.edit',compact('organisation'));
    }

    public function save(Request $request)
    {
        $request->validate([
            'name' => ['required'],
            'description' => ['required'],
            
        ]);

        $userId = Auth()->user()->id;
        $organisation = $this->organisation->getOrganisationById($request->id);
        $organisation->name=$request->name;
        $organisation->description = $request->description;
        $organisation->item_time = $request->item_time;
        $organisation->overhead_time = $request->overhead_time;
   
     
        try {
            $this->organisation->SaveOrganisation($organisation);
        } catch (\Exception $exception) {
            Log::error('Error saving organisation to the database' . ' ' . $exception->getMessage());
            Session::flash('error', __('organisations.something_went_wrong'));
            return back();
        }
        Session::flash('success', __('organisations.organisation_updated'));
        return redirect()->route('user.organisations.settings',['id'=>$organisation->id]);
    }


    public function show($id)
    {
        $organisation = $this->organisation->getOrganisationById($id);

        if(!isset($organisation)) {
            abort(404);
        }

        $hasPermissions = false;
        $user = Auth()->user();


        if($organisation->user_id ===  $user->id || $organisation->isPartOfTheOrganisation($user)) {
            $hasPermissions = true;
        }

        if(!$hasPermissions) {
            abort(403);
        }

        Session(['organisation' => $organisation->id]);

        $plannings = $this->planning->getAlPlanningsByOrganisationId($id);
        return view('user.organisations.show', ['organisation' => $organisation], ['plannings' => $plannings]);
    }

    public function inviteUserToOrganisation(Request $request, $id)
    {
        $request->validate([
            'email' => [
                'required'
            ]
        ]);

        $user = $this->user->getUserByEmail($request->email);

        if(Auth()->user()->email === $request->email) {
            Session::flash('error', __('organisations.invited_self'));
            return back();
        }


        $organisation = $this->organisation->getOrganisationById($id);

        if(!isset($organisation)) {
            abort(404);
        }

        if($organisation->user_id !== Auth()->user()->id) {
            Session::flash('warning', __('organisations.no_permission'));
            return back();
        }

        //If user has no account send an email with an invitation
        if(!isset($user)) {

            $signature = sha1(Str::random(10));
            $expiration_date = Carbon::now()->addMonth();

            while ($this->inviteOrganisation->checkIfSignatureExists($signature)) {
                $signature = sha1(Str::random(10));
            }

            $timestamp = $expiration_date->timestamp;

            $signedUrl = InviteUrlHelper::generateOrganisationUrl($organisation->id, $signature, $timestamp);

            $this->inviteOrganisation->storeOrganisationInvitation($organisation->id, $signature, $expiration_date, $signedUrl, $timestamp);

            // Try to send the email to the user
            try {
                if(env('APP_ENV') === 'local') {
                    Mail::to($request->email)->send(new SendActivationEmail($signedUrl));
                } else {
                    //@TODO add mandrill template
                }
            } catch (\Exception $exception) {
                Log::error('Invitation to organisation email could not be send' . ' ' . $exception->getMessage());
                Session::flash('error', __('organisations.email_not_sent'));
                return redirect()->route('user.organisations.show', ['id' => $organisation->id]);
            }

            Session::flash('success', __('organisations.user_added'));
            return redirect()->route('user.organisations.show', ['id' => $organisation->id]);
        }

        if($organisation->isPartOfTheOrganisation($user)) {
            Session::flash('warning', __('organisations.user_already_added'));
            return back();
        }

        $organisation->users()->attach($user->id);
        Session::flash('success', __('organisations.user_added'));
        return redirect()->route('user.organisations.show', ['id' => $organisation->id]);
    }

    public function settings($id)
    {
        $organisation = $this->organisation->getOrganisationById($id);

        if(!isset($organisation)) {
            abort(404);
        }

        if($organisation->user_id !== Auth()->user()->id) {
            Session::flash('warning', __('organisations.no_permission'));
            return back();
        }


        return view('user.organisations.settings', ['organisation' => $organisation]);
    }

    public function getUsers($id)
    {
        $organisation = $this->organisation->getOrganisationById($id);

        if(!isset($organisation)) {
            abort(404);
        }

        if($organisation->user_id !== Auth()->user()->id) {
            Session::flash('warning', __('organisations.no_permission'));
            return back();
        }
        return view('user.organisations.users.index', ['organisation' => $organisation]);
    }

    public function makeUserAndAttachToOrganisation(Request $request)
    {
        //Check of the request is valid
        if(!$request->hasValidSignature()) {
            Session::flash('warning', __('organisations.link_expired'));
            return redirect()->route('login');
        }

        if(!$this->inviteOrganisation->checkIfTimeStampAndSignatureAreValid($signature, $timestamp, $organisationId)) {
            Session::flash('warning', 'Link has expired');
            return redirect()->route('login');
        }

        $invitationOrganisation = $this->inviteOrganisation->getUniqueUrl($signature, $timestamp, $organisationId);

        if($request->method() === 'GET') {
            return view('authentication.register');
        }

        $request->validate([
            'email' => [
                'required',
                'email',
                Rule::unique('users', 'email')
            ],
            'password' => [
                ['required', 'min:8']
            ]
        ]);

        $organisation = $this->organisation->getOrganisationById($organisationId);

        if(!isset($organisation)) {
            Session::flash('warning', __('organisations.link_expired'));
            return redirect()->route('login');
        }


        //Add the user to the database and activate the account
        $user = $this->user->addUser($request, true);

        if(!$organisation->isPartOfTheOrganisation($user)) {
            $organisation->users()->attach($user->id);
        }

        //Remove link from database
        $invitationOrganisation->delete();

        Auth()->login($user);
        return redirect()->route('pages.index');
    }
}
