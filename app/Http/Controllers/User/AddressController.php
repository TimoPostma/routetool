<?php

namespace App\Http\Controllers\User;
use Illuminate\Support\Facades\Route;
use App\Address;
use App\Organisation;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AddressController extends Controller
{   public $address;
    public $organisation;
    public function __construct(Address $address, Organisation $organisation)
    {
        $this->address = $address;
        $this->organisation = $organisation;
     
    }
    public function getIndexPage($id)
    {
        $addresses = $this->address->getAddressesByOrganisationId($id);
        
        // $user = Auth()->user();
        // $ownerOrganisations = $this->organisation->getOwnerOrganisationsByUserId($user->id);

        // $invitedOrganisations = $user->organisationUser()->get();


        return view('user.addresses.index', compact('addresses','id'));
    }
    public function store(Request $request){
        $label = $request->label ?? "";
        $street = $request->street ?? "";
        $zipcode = $request->zipcode ?? "";
        $city = $request->city ?? "";
        $country = $request->country ?? "";
        $address = $this->address->getAddress($request->label,$request->street,$request->zipcode,$request->city,$request->country);

        if($address !== null){
            if (isset($request->item_time)){
                $address->item_time = $request->item_time;
            }
            if (isset($request->overhead_time)){
                $address->overhead_time = $request->overhead_time;
            }
            if (isset($request->description)){
                $address->description = $request->description;
            }
            if (isset($request->notes)){
                $address->notes = $request->notes;
            }
            //return $address;
            return $this->address->updateAddress($address,$request->organisationId);
        }
        return $this->address->storeAddress($request,$request->organisationId);
    }
    public function favorite(Request $request){
        $address = $this->address->getAddressById($request->addressId);
        $this->address->favoriteAddress($address);
        return redirect()->route("organisation.address.index",["id"=>$request->id,"addressId"=>$request->addressId]);
    }
    public function list($organisationId){
        return $this->address->getAddressesByOrganisationId($organisationId);
    }
    public function qeuelist($organisationId){
        $organisation = $this->organisation->getOrganisationById($organisationId);
        return $organisation->qeuelist;
        //return $this->address->getAddressesByOrganisationId($organisationId);
    }
    public function storeqeuelist(Request $request,$organisationId){
        $organisation = $this->organisation->getOrganisationById($organisationId);
        $organisation->qeuelist = $request->qeuelist;
        $organisation->update();
        return $organisation;
        //return $this->address->getAddressesByOrganisationId($organisationId);
    }
    public function delete(Request $request){
        //$address = $this->address->getAddressById($request->id);
        //Route::get("organisation.address.index");
        $this->address->deleteAddress($request->addressId);
        return redirect()->route("organisation.address.index",["id"=>$request->organisationId,"addressId"=>$request->addressId]);

    }
    public function create($id){
        $organisation = $this->organisation->getOrganisationById($id);
        return view('user.addresses.create',compact('id','organisation'));
    }
    public function edit($id,$addressId){
        $organisation = $this->organisation->getOrganisationById($id);
        $address = $this->address->getAddressById($addressId);
        return view('user.addresses.edit',compact('id','address','organisation'));
    }
}
