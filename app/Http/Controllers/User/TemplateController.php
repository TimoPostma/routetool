<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Organisation;
use App\Template;
use App\Transport;
use App\Period;
use App\Planning;
use Illuminate\Support\Facades\Storage;

class TemplateController extends Controller
{   public $organisation;
    public $template;
    public function __construct(Organisation $organisation,Template $template,Transport $transport,Period $period,Planning $planning)
    {
        $this->organisation = $organisation;
        $this->template = $template;
        $this->transport = $transport;
        $this->period = $period;
        $this->planning = $planning;
      
    }
    public function getIndexPage($id)
    {
        //$user = Auth()->user();
        //$organisations = $user->organisationUser()->get();
        $organisation = $this->organisation->getorganisationById($id);
        
        $templates = $this->template->getTemplatesByOrganisationId($id);

        return view('user.templates.index', compact('templates','organisation'));
    }
    public function getTemplates($id)
    {
        //$user = Auth()->user();
        //$organisations = $user->organisations()->get();
        
        
        $templates = $this->template->getTemplatesByOrganisationId($id);

        return $templates;
    }

    public function show($id,$templateId)
    {
        $organisation = $this->organisation->getOrganisationById($id);

        if(!isset($organisation)) {
            abort(404);
        }
        
        $template = $this->template->getTemplateById($templateId);

        return view('user.templates.show', compact('template','organisation'));
    }
    public function create($id)
    {
        $organisation = $this->organisation->getOrganisationById($id);

        if(!isset($organisation)) {
            abort(404);
        }
        $transports = [];
        $periods = [];
        $cells = [];

        $transport = new Transport();
        $transport->key = "eigen";
        $transport->label = "Eigen vrachtwagen";
        $transport->roundtrip = false;
        $transport->showmap= false;
        array_push($transports,$transport);

        $transport = new Transport();
        $transport->key = "dhl";
        $transport->label = "DHL";
        $transport->roundtrip = false;
        $transport->showmap= false;
        array_push($transports,$transport);

        $transport = new Transport();
        $transport->key = "gls";
        $transport->label = "GLS";
        $transport->roundtrip = false;
        $transport->showmap= false;
        array_push($transports,$transport);

        $period = new Period();
        $period->key = "monday";
        $period->label = "Maandag";
        $period->date = "2022-25-04";
        array_push($periods,$period);
        $period = new Period();
        $period->key = "tuesday";
        $period->label = "Dinsdag";
        $period->date = "2022-26-04";
        array_push($periods,$period);
        $period = new Period();
        $period->key = "wednesday";
        $period->label = "Woensdag";
        $period->date = "2022-27-04";
        array_push($periods,$period);
        $period = new Period();
        $period->key = "thursday";
        $period->label = "Donderdag";
        $period->date = "2022-28-04";
        array_push($periods,$period);
        $period = new Period();
        $period->key = "friday";
        $period->label = "Vrijdag";
        $period->date = "2022-29-04";
        array_push($periods,$period);

     
       
            $rows = [];
            foreach($transports as $transport){
                $colls = [];
                foreach($periods as $period){
                    $obj = new Period();
                    $obj->transport = $transport->key;
                    $obj->period = $period->key;
                    $obj->locations = [];
                    
                    array_push($colls,$obj);
                }
                array_push($rows,$colls);
            }
            
        
        //return $rows;
        
        
        $periods = implode(",",$periods);
        $transports = implode(",",$transports);
        $rows = json_encode($rows);
        
        return view('user.templates.create', compact("organisation","transports","periods","rows"));
    }
    public function store(){
        
    }
    public function apply($id,$planboardId,$templateId){
        $planboard= $this->planning->getPlanningById($planboardId);
        $transports = [];
        $periods = [];
        $cells = [];

        $transport = new Transport();
        $transport->key = "eigen";
        $transport->label = "Eigen vrachtwagen";
        $transport->roundtrip = false;
        $transport->showmap= false;
        array_push($transports,$transport);

        $transport = new Transport();
        $transport->key = "dhl";
        $transport->label = "DHL";
        $transport->roundtrip = false;
        $transport->showmap= false;
        array_push($transports,$transport);

        $transport = new Transport();
        $transport->key = "gls";
        $transport->label = "GLS";
        $transport->roundtrip = false;
        $transport->showmap= false;
        array_push($transports,$transport);

        $period = new Period();
        $period->key = "monday";
        $period->label = "Maandag";
        $period->date = "2022-25-04";
        array_push($periods,$period);
        $period = new Period();
        $period->key = "tuesday";
        $period->label = "Dinsdag";
        $period->date = "2022-26-04";
        array_push($periods,$period);
        $period = new Period();
        $period->key = "wednesday";
        $period->label = "Woensdag";
        $period->date = "2022-27-04";
        array_push($periods,$period);
        $period = new Period();
        $period->key = "thursday";
        $period->label = "Donderdag";
        $period->date = "2022-28-04";
        array_push($periods,$period);
        $period = new Period();
        $period->key = "friday";
        $period->label = "Vrijdag";
        $period->date = "2022-29-04";
        array_push($periods,$period);

     
        
        
        $rows = [];    
            
            foreach($periods as $period){
                $colls = [];
                foreach($transports as $transport){
                    $obj = new Period();
                    $obj->transport = ["key"=>$transport->key,"label"=>$transport->label];
                    $obj->period = ["key"=>$period->key,"label"=>$period->label,"date"=>$period->date];
                    $obj->locations = [["address"=>["street"=>"Boterdiep ZZ 27", "zipcode"=>"9781 GP","city"=>"Bedum"],
                                        "orders"=>[[
                                            "label"=>"202234254"],[
                                            "label"=>'202289842']
                                        ],"times"=>["itemtime"=>2,"overheadtime"=>15]],
                                        ["address"=>["street"=>"Petrus Campersingel 185", "zipcode"=>"9713AK","city"=>"Groningen"],
                                        "orders"=>[[
                                            "label"=>"202234254",
                                            "label"=>'202289842']
                                        ],"times"=>["itemtime"=>2,"overheadtime"=>15]]];
                    
                    array_push($colls,$obj);
                }
                array_push($rows,$colls);
            }
            
        $routeData = [["periods"=>$periods,"transports"=>$transports,"cells"=>$rows]];
        //return $rows;
        $this->planning->apply($planboard,$routeData);
        
        // $periods = implode(",",$periods);
        // $transports = implode(",",$transports);
        // $rows = json_encode($rows);

        return [$id,$planboardId,$templateId];
    }
    public function getExcelTemplate(){
        return Storage::download('ExcelImportTemplate.xlsx');
    }
}


// [{"transport":
//     [{"routes":[],
//         "totalTime":"",
//         "startingRoute":false,
//         "label":"eigen",
//         "map":true,
//         "calc":true,
//         "circ":true,
//         "show":true},
//     {"routes":[],
//         "totalTime":"",
//         "startingRoute":false,
//         "label":"extern",
//         "map":true,
//         "calc":true,
//         "circ":true,
//         "show":true}],
//      "label":"2-5-2022",
//       periodId":1},
// {"transport":
//     [{"routes":[],
//         "totalTime":"",
//         "startingRoute":false,
//         "label":"eigen",
//         "map":true,
//         "calc":true,
//         "circ":true,
//         "show":true},
//     {"routes":[],
//         "totalTime":"",
//         "startingRoute":false,
//         "label":"extern",
//         "map":true,
//         "calc":true,
//         "circ":true,
//         "show":true}],
//         "label":"3-5-2022",
//         "periodId":2},
// {"transport":
//     [{"routes":[],
//         "totalTime":"",
//         "startingRoute":false,
//         "label":"eigen","
//         map":true,"
//         calc":true,
//         "circ":true,
//             "show":true},
//         {"routes":[],
//             "totalTime":"",
//             "startingRoute":false,
//             "label":"extern",
//             "map":true,
//             "calc":true,
//             "circ":true,
//             "show":true}],
//             "label":"4-5-2022",
//             "periodId":3},
// {"transport":[{"routes":[],"totalTime":"","startingRoute":false,"label":"eigen","map":true,"calc":true,"circ":true,"show":true},{"routes":[],"totalTime":"","startingRoute":false,"label":"extern","map":true,"calc":true,"circ":true,"show":true}],"label":"5-5-2022","periodId":4},{"transport":[{"routes":[],"totalTime":"","startingRoute":false,"label":"eigen","map":true,"calc":true,"circ":true,"show":true},{"routes":[],"totalTime":"","startingRoute":false,"label":"extern","map":true,"calc":true,"circ":true,"show":true}],"label":"6-5-2022","periodId":5}]