<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use User;
use Illuminate\Support\Facades\Auth;
class LocaleController extends Controller
{
    public function setLocale($locale)
    {   App()->setLocale($locale);
        $locales = ['en', 'nl'];
        if(Auth::user()){
            $user = Auth::user();
            $user->locale = $locale;
            $user->update();
           
            App()->setLocale($user->locale);
            return back();
        }
       
        Session()->put('locale', $locale);
        //Check if the locale exists in the array if not return an error
        // if(!in_array($locale, $locales)) {
        //     Session::flash('error', __('sessions.locale_not_exist'));
        //     return redirect()->route('pages.index');
        // }
        // Session()->put('locale', $locale);
        // App()->setLocale($locale);
        return back();
    }
}
