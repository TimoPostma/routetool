<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\ImportSettings;
use App\Organisation;
use Illuminate\Http\Request;

class ImportSettingController extends Controller
{
    public $organisation;
    public $importSettings;

    public function __construct(ImportSettings $importSettings, Organisation $organisation)
    {
        $this->importSettings = $importSettings;
        $this->organisation = $organisation;
    }

    /**
     * @OA\Get (
     *     path="/api/v1/organisations/{organisationId}/import-settings/{importSettingId}",
     *     description="Get import settings by organisationId",
     *     @OA\Parameter (
     *     name="organisationId",
     *     in="path",
     *     description="organisationId",
     *      required=true,
     *     @OA\Schema (type="string"),
     *     ),
     *     @OA\Parameter (
     *     name="importSettingId",
     *     in="path",
     *     description="importSettingId",
     *      required=true,
     *     @OA\Schema (type="integer"),
     *     ),
     *     @OA\Response(response=200, description="import settings with details"),
     *     @OA\Response(response=401, description="Unauthorized"),
     *     @OA\Response(response=403, description="Unauthorized")
     *
     * )
     */

    public function getImportSettingDetails($organisationId , $importSettingsId)
    {
        $organisation = $this->organisation->getOrganisationById($organisationId);

        if(!isset($organisation)) {
            $jsonData = ['status' => 'not found', 'messsage' => 'organisation not found'];
            return Response()->json($jsonData, 404);
        }

        $user = Auth()->user();

        if(!$organisation->isPartOfTheOrganisation($user) && $organisation->user_id !== $user->id) {
            $jsonData = ['status' => 'forbidden', 'messsage' => 'organisation not found'];
            return Response()->json($jsonData, 403);
        }

        $importSetting = $this->importSettings->getImportSettingByIdAndOrganisationId($importSettingsId, $organisationId);

        if(!isset($importSetting)) {
            $jsonData = ['status' => 'not found', 'messsage' => 'import setting not found'];
            return Response()->json($jsonData, 404);
        }

        $jsonData = ['status' => 'found', 'data' => $importSetting, 'message' => 'import setting found'];
        return Response()->json($jsonData, 200);
    }

    /**
     * @OA\Get (
     *     path="/api/v1/organisations/{organisationId}/import-settings/",
     *     description="Get import settings by organisationId",
     *     @OA\Parameter (
     *     name="organisationId",
     *     in="path",
     *     description="organisationId",
     *      required=true,
     *     @OA\Schema (type="string"),
     *     ),
     *     @OA\Response(response=200, description="import settings object"),
     *     @OA\Response(response=401, description="Unauthorized"),
     *     @OA\Response(response=403, description="Unauthorized")
     *
     * )
     */

    public function getImportSettingsByOrganisation($organisationId)
    {
        $organisation = $this->organisation->getOrganisationById($organisationId);

        if(!isset($organisation)) {
            $jsonData = ['status' => 'not found', 'messsage' => 'organisation not found'];
            return Response()->json($jsonData, 404);
        }

        $user = Auth()->user();

        if(!$organisation->isPartOfTheOrganisation($user) && $organisation->user_id !== $user->id) {
            $jsonData = ['status' => 'forbidden', 'messsage' => 'organisation not found'];
            return Response()->json($jsonData, 403);
        }

        $importSettings = $this->importSettings->getImportSettingByOrganisationId($organisationId);
        $jsonData = ['status' => 'found', 'data' => $importSettings, 'message' => 'import settings found'];
        return Response()->json($jsonData, 200);
    }
}
