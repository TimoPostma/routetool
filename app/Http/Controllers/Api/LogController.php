<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Log;
use Illuminate\Http\Request;

class LogController extends Controller
{
    public $logs;

    public function __construct(Log $logs)
    {
        $this->logs = $logs;
    }

    public function getLogs(Request $request)
    {
        $level = $request->input('level');

        if(!isset($level)) {
            $jsonData = ['status' => 'missing', 'message' => 'level parameter is missing'];
            return Response()->json($jsonData, 422);
        }

        $logs = $this->logs->getLogs($level);
        return Response()->json($logs, 200);

    }
}
