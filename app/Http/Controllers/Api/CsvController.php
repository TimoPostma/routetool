<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use League\Csv\Exception;
use League\Csv\Reader;
use League\Csv\Statement;

class CsvController extends Controller
{
    public function uploadCSV(Request $request)
    {
        $file = $request->file('file');

        $type = $file->getMimeType();

        if($type !== 'text/plain') {
            $jsonData = ['status' => 'error', 'message' => 'file is not an csv file'];
            return Response()->json($jsonData, 422);
        }

        $csv = Reader::createFromFileObject($file->openFile('r'));

        try {
            $csv->setHeaderOffset(0);
        } catch (Exception $e) {
            return Response()->json('error', 422);
        }

        $csvError = false;

        $addresses = [];

        $stmt = (new Statement())
            ->offset(0);

        $records = $stmt->process($csv);

        foreach ($records as $record) {

            foreach ($record as $type) {
                //@TODO Overleggen hoe we de CSV file aangeleverd moet worden
                Log::debug($type);
                $addresses[] = $type;
                continue;
            }
        }

        if($csvError) {
            $jsonData =  ['status' => 'error', 'message' => 'cannot process the csv file'];
            return Response()->json($jsonData, 422);
        }

        return Response()->json($addresses, 200);

    }
}
