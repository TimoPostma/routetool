<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;

class AuthenticationController extends Controller
{

    /**
     * Creates a link to directly login
     */
    public function authenticateWithBearerToken(Request $request)
    {
        $bearerToken = $request->bearerToken();

        if($bearerToken === null) {
            return Response()->json('no bearer token found on the url', 400);
        }

	// get data for the url
	$organisation_uuid = $request->input('organisation_uuid');
	$planboard_uuid = $request->input('planboard_uuid');

        //@TODO for now we will validate 2 bearer tokens, who can craft a signed url
	// @todo The bearer token is stored in the `organisations`.`bearertoken` field
        $reyndesignBearerToken = "dewfrafewrfgfe";
        $evolveproductionsBearerToken = "adewffwadfwfwfg";

        $accountFound = false;

        if($bearerToken === $reyndesignBearerToken) {
            $accountFound = true;
            $userId = 15;
        }

        if($bearerToken === $evolveproductionsBearerToken) {
            $accountFound = true;
            $userId = 14;
        }

        if(!$accountFound) {
            return Response()->json("Bearertoken is invalid", 401);
        }

        $planboardUrl = URL::temporarySignedRoute(
          'organisation.planning.show', Now()->addMinutes(15), ['userId' => $userId, 'id'=> $organisation_uuid, 'planningId' => $planboard_uuid]
        );

        return Response()->json($planboardUrl, 200);
    }
}
