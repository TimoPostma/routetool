<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Route;
use Carbon\CarbonInterval;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use TeamPickr\DistanceMatrix\DistanceMatrix;
use TeamPickr\DistanceMatrix\Licenses\StandardLicense;
use Illuminate\Support\Facades\Cache;

class GoogleMatrixController extends Controller
{
    public function getDistances(Request $request)
    {   $addresses = [];
        $requestdata = $request->getContent();
        $cacheKey = md5($requestdata);
        $data = json_decode($request->getContent(), true);

        //Check if the addresses key has been set
        if(!array_key_exists('addresses', $data)) {
            $jsonData = ['status' => 'Bad request', 'message' => 'key addresses is missing'];
            return response()->json($jsonData, 400);
        }


        //Check if the addresses is an array
        if(!is_array($data['addresses'])) {
            $jsonData = ['status' => 'Bad request', 'message' => 'addresses has to be an array'];
            return Response()->json($jsonData, 400);
        }

        $addressesObject = $data['addresses'];

        if(count($addressesObject) <= 1) {
            $jsonData = ['status' => 'error', 'message' => 'Cannot generate origin and destination addresses with one ore less addresses'];
            return Response()->json($jsonData, 400);
        }
     
        $extraTime = 0;
     
        foreach ($addressesObject as $addressObject) {
            $address = "";
            
            if(isset($addressObject['address']['street'])){
                $address .= $addressObject['address']['street']." ";
            }
            if(isset($addressObject['address']['zipcode'])){
                $address .= $addressObject['address']['zipcode']." ";
            }
            if(isset($addressObject['address']['city'])){
                $address .= $addressObject['address']['city']." ";
            }
            if(isset($addressObject['address']['country'])){
                $address .= $addressObject['address']['country'];
            }
            
            $addresses[] = $address;
            
        }
        
        if(!array_key_exists('wayBack', $data)) {
            $data['wayBack'] = false;
        }

        if($data['wayBack']) {
            $addresses[] = $addresses[0];
        }
  
        $origins = $addresses;
        $destinations = $addresses;


        //On the origins we will remove the last entry because we will have data already in the Matrix
        $origins = array_splice($origins, 0, count($origins) - 1);


        //On the destination we will remove the start entry because we already have the data
        $destinations = array_splice($destinations, 1);


        $licence = new StandardLicense(env('GOOGLE_MAPS_KEY'));

        $response =  DistanceMatrix::license($licence);


        foreach ($origins as $origin) {
            $response->addOrigin($origin);
        }

        foreach ($destinations as $destination) {
            $response->addDestination($destination);
        }

        $response = $response->request();


        if(!$response->successful()) {
            Log::error('Something went wrong when getting the routes');
            return Response()->json($response->error(), 500);
        }
        

        $rows = $response->json['rows'];
        $calculations = [];
        $totalTime = 0;
        $canCalculate = true;
        //Count the rows and get the element, rIdx is origin_address
        for ($rIdx = 0; $rIdx<count($rows);$rIdx++) {

            $destinationIdx = $rIdx;
            //Get the elements which routes needs to be saved into the database
            for($eIdx = 0; $eIdx < count($rows); $eIdx++) {
                $route = new Route();
                $hash =  sha1($origins[$rIdx] . '-' . $destinations[$eIdx]);
            }

            $calculations[$rIdx]['origin_address'] = $response->json['origin_addresses'][$rIdx];
            $calculations[$rIdx]['destination_addresses'] = $response->json['destination_addresses'][$rIdx];
            
            switch($rows[$rIdx]['elements'][$rIdx]['status']) {
            case 'ZERO_RESULTS':
                
                $canCalculate = false;
                break;
            case 'OK':
                $calculations[$rIdx]['distance_value'] = $rows[$rIdx]['elements'][$rIdx]['distance']['value'];
                $calculations[$rIdx]['duration_text'] = $rows[$rIdx]['elements'][$rIdx]['duration']['text'];
                $calculations[$rIdx]['duration_value'] = $rows[$rIdx]['elements'][$rIdx]['duration']['value'];
                $totalTime += $rows[$rIdx]['elements'][$rIdx]['duration']['value'];
                break;
            default:

                $canCalculate = false;
                
            }

            if (!$canCalculate){
                return Response()->json("cant calculate", 200);
             }   
        }

        $totalTime += ($extraTime * 60);
        Log::debug($totalTime);
        $calculations['total_time'] = $totalTime;

        Cache::put($cacheKey, $calculations, $seconds = 3600);
        return Response()->json($calculations, 200);
    
}
}