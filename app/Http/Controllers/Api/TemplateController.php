<?php

namespace App\Http\Controllers\Api;
use App\Organisation;
use App\Template;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TemplateController extends Controller
{   
    private $organisation;
    private $template;

    public function __construct(Organisation $organisation,Template $template)
    {
        $this->template = $template;
        $this->organisation = $organisation;
    }
    public function saveTemplate(Request $request)
    {
        $data = json_decode($request->getContent(), true);

        if(!array_key_exists('organisationId', $data)) {
            $jsonData = ['status' => 'key missing', 'message' => 'organisationId is missing'];
            return Response()->json($jsonData, 400);
        }
        if(!array_key_exists('label', $data)) {
            $jsonData = ['status' => 'key missing', 'message' => 'label is missing'];
            return Response()->json($jsonData, 400);
        }

        //Check if the organisation exists
        $organisation = $this->organisation->getOrganisationById($data['organisationId']);

        if(!isset($organisation)) {
            $jsonData = ['status' => 'not found', 'message' => 'organisation not found'];
            return Response()->json($jsonData, 404);
        }

        $user = Auth()->user();

        //Store the routes data
        //encode the data to json text
        $templateData = json_encode($data['templatedata']);
        $label = $data['label'];
        $description = $data['description'];
        $this->template->storeTemplate($label,$description,$organisation->id,$templateData);
        try {
            
        } catch (\Exception $exception) {
            
            $jsonData = ['status' => 'error', 'message' => 'cannot save the file to database'];
            return Response()->json($jsonData, 500);
        }
        return Response()->json('stored in database', 200);
    }
}
