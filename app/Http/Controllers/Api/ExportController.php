<?php

namespace App\Http\Controllers\Api;

use App\Exports\PlanningExport;
use App\Http\Controllers\Controller;
use App\Planning;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Facades\Excel;

class ExportController extends Controller
{
    private $planning;

    public function __construct(Planning $planning)
    {
        $this->planning = $planning;
    }

    public function exportToExcel(Request $request, $planningId)
    {  
        //return $planningId;
        //Hardcoded to test
        $planning = $this->planning->getPlanningById($planningId)->toArray();
        $planning = $planning->routeData;

        if(!isset($planning)) {
            return Response()->json("planning not found" . ' ' . $planningId, 404);
        }
        

        $periods =  json_decode($planning['routedata'], true);

        $exelArray = [];

        //Split address to address and city

        foreach ($periods as $period) {
            Log::debug(print_r($period['label'], true));
            $columnArray = [];
            $this->array_insert($columnArray, 0, $period['label']);
            foreach ($period['transport'] as $transport) {
                $periodArray = $columnArray;
                $this->array_insert($periodArray, 1, $transport['label']);
                foreach ($transport['routes'] as $route) {
                    $routeArray = $periodArray;

                   $variable = filter_var($route['address'], FILTER_SANITIZE_NUMBER_INT);
                   $city = trim(Str::after($route['address'], $variable));
                   $address = trim(Str::before($route['address'], $variable));

                    $this->array_insert($routeArray, 0, $address . ' ' . $variable);
                    $this->array_insert($routeArray, 1, $city);
                    $exelArray[] = $routeArray;
                }

            }

        }

        $export = new PlanningExport($exelArray);


        $selectedSetting = "xlsx";

        switch ($selectedSetting) {
            case 'xls' :
                $setting = \Maatwebsite\Excel\Excel::XLS;
                break;
            case 'xlsx' :
                $setting = \Maatwebsite\Excel\Excel::XLSX;
                break;
            case 'csv' :
                $setting =  \Maatwebsite\Excel\Excel::CSV;
                break;
            default :
                return Response()->json('Setting not supported' . ' ' . $selectedSetting, 422);
        }


        return Excel::download($export , $planning['name'] . '-' . time() . '.' . $selectedSetting, $setting );
    }

    /**
     * @param array      $array
     * @param int|string $position
     * @param mixed      $insert
     */
    function array_insert(&$array, $position, $insert)
    {
        if (is_int($position)) {
            array_splice($array, $position, 0, $insert);
        } else {
            $pos = array_search($position, array_keys($array));
            $array = array_merge(
                array_slice($array, 0, $pos),
                $insert,
                array_slice($array, $pos)
            );
        }
    }
}
