<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Organisation;
use App\Planning;
use App\Address;
use App\Transport;
use App\Services\PlanningBoardService;
use App\Template;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Http;
use App\Services\RouteTemplateService;

/**
 * Control plannings (currently called Planboards in the frontend)
 */
class PlanningController extends Controller
{
	/**
	 * The planning instance that is being controlled
	 */
	public $planning;
	public $address;
    	public $transport;

	/**
	 * The organisation that owns the planning
	 */
	private $organisation;


	/**
 	 * Construct planning controller
 	 *
	 */
	public function __construct(Planning $planning, Organisation $organisation, Address $address, Transport $transport)
	{
		$this->planning = $planning;
		$this->organisation = $organisation;
		$this->address = $address;
		$this->transport = $transport;
	}

    public function savePlanning(Request $request)
    {
        $data = json_decode($request->getContent(), true);


        if(!array_key_exists('planningId', $data)) {
            $jsonData = ['status' => 'key missing', 'message' => 'planningId is missing'];
            return Response()->json($jsonData, 400);
        }


        if(!array_key_exists('organisationId', $data)) {
            $jsonData = ['status' => 'key missing', 'message' => 'organisationId is missing'];
            return Response()->json($jsonData, 400);
        }

        if(!array_key_exists('routes', $data)) {
            $jsonData = ['status' => 'key missing', 'message' => 'routes is missing'];
            return Response()->json($jsonData, 400);
        }

        if(!is_array($data['routes'])) {
            $jsonData = ['status' => 'invalid key', 'message' => 'Key needs to be an array'];
            return Response()->json($jsonData, 400);
        }

        //Check if an planning exists
        $planning = $this->planning->getPlanningById($data['planningId']);

        if(!isset($planning)) {
            $jsonData = ['status' => 'not found', 'message' => 'Planning not found'];
            return Response()->json($jsonData, 404);
        }

        if($planning->permission_planning_id < 2) {
            $jsonData = ['status' => 'no writing permissions', 'message' => 'U have not the right permissions to write to the planning'];
            return Response()->json($jsonData, 403);
        }

        //Check if the organisation exists
        $organisation = $this->organisation->getOrganisationById($data['organisationId']);

        if(!isset($organisation)) {
            $jsonData = ['status' => 'not found', 'message' => 'organisation not found'];
            return Response()->json($jsonData, 404);
        }

        if($planning->organisation_id !== $data['organisationId']) {
            $jsonData = ['status' => 'forbidden', 'cannot store the planning'];
            return Response()->json($jsonData, 403);
        }
        $user = Auth()->user();
        $itemTime = $organisation->item_time;
        $overheadTime = $organisation->overhead_time;

        $hasPermissions = false;

        if($organisation->user_id === $user->id || $organisation->isPartOfTheOrganisation($user)) {
            $hasPermissions = true;
        }

        if(!$hasPermissions) {
            $jsonData = ['status' => 'forbidden', 'cannot store the planning'];
            return Response()->json($jsonData, 403);
        }


        //Store the routes data
        //encode the data to json text
        $routeData = json_encode($data['routes']);
        try {
            $this->planning->storeRouteData($planning, $routeData);
        } catch (\Exception $exception) {
            Log::error('Cannot store routedata to database' . ' ' . $exception->getMessage());
            $jsonData = ['status' => 'error', 'message' => 'cannot save the file to database'];
            return Response()->json($jsonData, 500);
        }
        return Response()->json('stored in database', 200);
    }

    /**
     * Synchronize from within routetool
     */
    public function synchronizePlanning(Request $request){
        $data = json_decode($request->getContent(), true);

        if(!array_key_exists('planningId', $data)) {
            $jsonData = ['status' => 'key missing', 'message' => 'planningId is missing'];
            return Response()->json($jsonData, 400);
        }

        if(!array_key_exists('organisationId', $data)) {
            $jsonData = ['status' => 'key missing', 'message' => 'organisationId is missing'];
            return Response()->json($jsonData, 400);
        }

	$planboardId = $data['planningId'];

	$planning = (new Planning())->getPlanningById($planboardId);

        $calldata = [
                'planboard_uuid' => $planboardId,
                'routedata' => $planning->routedata
        ];

        $syncUrl = $planning->webhookurl;
        if($syncUrl === null){
                $jsonData = ['status' => 'error', 'message' => 'No url to sync to.'];
                return Response()->json($jsonData, 500);
        }

        // @todo make a dynamic token
        $token = 'kdjfgnerigsjkfsdkgsdfsglfgsd';

try {
        $response = Http::post($planning->webhookurl, $calldata);
}catch(\Throwable $t){
die($t->getMessage());
}
        if($response->ok()) {
                return Response()->json(['status' => 'success'], 200);
        } else {
                return Response()->json(['status' => 'error', 'message' => 'Remote error'], 500);
        }
    }

    /**
     * @OA\Get(
     *     path="/api/v2/organisations/{organisationId}/planboards/{planboardId}/synchronize-ovs",
     *     description="Synchronize planboard with OVS webhookurl",
     *     @OA\Parameter (
     *     in="path",
     *     name="organisationId",
     *     required=true,
     *     description="organisationId",
     *     @OA\Schema (type="string"),
     *     ),
     *     @OA\Parameter (
     *     in="path",
     *     name="planboardId",
     *     required=true,
     *     description="planboardId",
     *     @OA\Schema (type="string"),
     *     ),
     *     @OA\Response(response=200, description="The current planboard routedata."),
     *     @OA\Response(response=401, description="Unauthorized"),
     *     @OA\Response(response=404, description="Organisation or planboard not found")
     * )
     */
    public function synchronizeWithOvs(Request  $request, $organisationId ,$planningId){

        $organisation = (new Organisation())->getOrganisationById($organisationId);
        if($organisation === null) {
            $jsonData = ['status' => 'not found', 'message' => 'organisation not found'];
            return Response()->json($jsonData, 404);
        }

    	$token = $request->bearerToken();
	if($token !== $organisation->bearertoken){
		return Response()->json(['status' => 'Unauthorized'], 401);
	}

        $planning = (new Planning())->getPlanningById($planningId);
        if($planning === null){
            $jsonData = ['status' => 'not found', 'message' => 'planboard not found'];
            return Response()->json($jsonData, 404);
	}

	// @todo check if planning is owned by the organisation

	$data = [
		'planboard_uuid' => $planningId,
		'routedata' => $planning->routedata
	];


        return Response()->json($data, 200);

        //@TODO needed for webhooks, but for now we will ignore this

	$syncUrl = $planning->webhookurl;
	if($syncUrl === null){
		$jsonData = ['status' => 'not found', 'message' => 'No url to sync to.'];
            	return Response()->json($jsonData, 500);
	}

	// @todo make a dynamic token
	$token = 'kdjfgnerigsjkfsdkgsdfsglfgsd';


	$response = Http::withToken($token)->post($planning->webhookurl, $data);
	if($response->ok()) {
	        return Response()->json(['status' => 'success'], 200);
	} else {
		return Response()->json(['status' => 'error', 'message' => 'Remote error'], 500);
	}
    }

    public function getRouteData($organisationId ,$planningId)
    {
        $organisation = $this->organisation->getOrganisationById($organisationId);
        $planning = $this->planning->getPlanningById($planningId);

        if(!isset($organisation) || !isset($planning)) {
            $jsonData = ['status' => 'not found', 'message' => 'planning or organisation not found'];
            return Response()->json($jsonData, 404);
        }

        $user = Auth()->user();

        $hasPermissions = false;

        if($organisation->user_id === $user->id || $organisation->isPartOfTheOrganisation($user)) {
            $hasPermissions = true;
        }

        if(!$hasPermissions) {
            $jsonData = ['status' => 'forbidden', 'cannot store the planning'];
            return Response()->json($jsonData, 403);
        }

        $routeData = $this->planning->getRouteData($planningId);
        return Response()->json($routeData['routedata'], 200);

    }

    /**
     * @OA\Get(
     *     path="/api/v1/organisations/{organisationId}/plannings",
     *     description="Get all plannings that belong to the organisation",
     *     @OA\Parameter (
     *     in="path",
     *     name="organisationId",
     *     required=true,
     *     description="organisationId",
     *     @OA\Schema (type="string"),
     *     ),
     *     @OA\Response(response=200, description="All plannings owned by the opganisation."),
     *     @OA\Response(response=401, description="Unauthorized"),
     *     @OA\Response(response=404, description="Organisation not found")
     * )
     */

    public function getOrganisationPlannings($organisationId)
    {
        $organisation = $this->organisation->getOrganisationById($organisationId);

        if(!isset($organisation)) {
            $jsonData = ['status' => 'not found', 'message' => 'organisation not found'];
            return Response()->json($jsonData, 404);
        }

        $user = Auth()->user();

        if($user->id !== $organisation->user_id && !$organisation->isPartOfTheOrganisation($user)) {
            $jsonData = ['status' => 'forbidden', 'Cannot get the plannings of the organisation'];
            return Response()->json($jsonData, 403);
        }

        $plannings = $this->planning->getAlPlanningsNameAndDescriptionByOrganisationId($organisationId);
        return Response()->json($plannings);
    }

    /**
     * @OA\Get(
     *     path="/api/v2/organisations/{organisationId}/planboards",
     *     description="Get all plannings that belong to the organisation",
     *     @OA\Parameter (
     *     in="path",
     *     name="organisationId",
     *     required=true,
     *     description="organisationId",
     *     @OA\Schema (type="string"),
     *     ),
     *     @OA\Response(response=200, description="All plannings owned by the opganisation."),
     *     @OA\Response(response=401, description="Unauthorized"),
     *     @OA\Response(response=404, description="Organisation not found")
     * )
     */
    public function getOrganisationPlanboards($organisationId, Request $request)
    {

	$organisation = (new Organisation())->getOrganisationById($organisationId);
        if(!isset($organisation)) {
            $jsonData = ['status' => 'not found', 'message' => 'organisation not found'];
            return Response()->json($jsonData, 404);
        }

	$token = $request->bearerToken();
	if($token !== $organisation->bearertoken){
		return Response()->json(['status' => 'Unauthorized'], 401);
	}

        $plannings = (new Planning())->getAlPlanningsNameAndDescriptionByOrganisationId($organisationId);
        return Response()->json($plannings);
    }


    /**
     * @OA\Post(
     *     path="/api/v2/organisations/{organisationId}/planboards/create",
     *     description="Creates a new planboard that belongs to the organisation",
     *     @OA\Parameter (
     *     in="path",
     *     name="organisationId",
     *     required=true,
     *     description="organisationId",
     *     @OA\Schema (type="string"),
     *     ),
     *     @OA\Response(response=200, description="All plannings owned by the opganisation."),
     *     @OA\Response(response=401, description="Unauthorized"),
     *     @OA\Response(response=404, description="Organisation not found")
     * )
     */
    public function createOrganisationPlanboard($organisationId, Request $request)
    {
        Log::info('CreateOrganisationPlanboard');

        $organisation = (new Organisation())->getOrganisationById($organisationId);

        if(!isset($organisation)) {
            $jsonData = ['status' => 'not found', 'message' => 'organisation not found'];
            return Response()->json($jsonData, 404);
        }

        $token = $request->bearerToken();
        if($token !== $organisation->bearertoken){
            return Response()->json(['status' => 'Unauthorized'], 401);
        }

        $periods = json_decode($request['periods'], true);
        $transports = json_decode($request['transports'], true);
        $ovsData = json_decode($request['entries'], true);

        Log::debug(print_r($periods, true));
        Log::debug(print_r($transports, true));
        Log::debug(print_r($ovsData, true));


        $name = $request->input('name');
        // new code to save the transports loaded from ovs
        // $transports is a key-value map mapping name to name
    //    foreach($transports as $transport){
  //          $this->transport->getTransportByLabel($transport);
//        }

        try {
            $routedata = PlanningBoardService::createPlanBoardData($transports, $periods, $ovsData, $organisationId);
        } catch (\Exception $exception) {
            Log::error('Something goes wrong, creating the plan data' . ' ' . $exception->getMessage());
            return Response()->json('Something did go wrong', 500);
        }

        $planning = new Planning;
        $planning->organisation_id = $organisationId;
        $planning->name = $name;
        $planning->routedata = json_encode($routedata);
        $planning->permission_planning_id = 2;
        // since this is a remotely created board, we want to lock the set of items from the routetool-interface. Only moving is allowed
        $planning->is_locked_set = 1;
        $saved = $planning->save();

        if(!$saved) {
            return Response()->json('Could not save the planboard', 500);
        }

	// the id is a uuid
        return Response()->json($planning->id);
    }

    /**
     * @OA\Get(
     *     path="/api/v2/organisations/{organisationId}/planboards/{planboardId}",
     *     description="Gets a planboard that belongs to the organisation",
     *     @OA\Parameter (
     *     in="path",
     *     name="organisationId",
     *     required=true,
     *     description="organisationId",
     *     @OA\Schema (type="string"),
     *     ),
     *     @OA\Parameter (
     *     in="path",
     *     name="planboardId",
     *     required=true,
     *     description="planboardId",
     *     @OA\Schema (type="string"),
     *     ),
     *     @OA\Response(response=200, description="The specific planboard owned by the opganisation."),
     *     @OA\Response(response=401, description="Unauthorized"),
     *     @OA\Response(response=404, description="Organisation or planboard not found")
     * )
     */
    public function getOrganisationPlanboard($organisationId, $planboardId, Request $request)
    {
        $organisation = (new Organisation())->getOrganisationById($organisationId);

        if($organisation === null) {
            $jsonData = ['status' => 'not found', 'message' => 'organisation not found'];
            return Response()->json($jsonData, 404);
        }

        $token = $request->bearerToken();
            if($token !== $organisation->bearertoken){
                return Response()->json(['status' => 'Unauthorized'], 401);
            }


	$planning = (new Planning())->getPlanningById($planboardId);
	if($planning === null){
            $jsonData = ['status' => 'not found', 'message' => 'planboard not found'];
            return Response()->json($jsonData, 404);
	}
	if($planning->routedata === null){
		$planning->routedata = [
				"meta" => [
					"type" => "planitems",
					"version" => "1.0.0"
				]
			];
	}
        return Response()->json($planning->routedata);
    }


    /**
     * @OA\Post(
     *     path="/api/v2/organisations/{organisationId}/planboards/{planboardId}/update_order_period",
     *     description="Updates the period  of an order on the planboard",
     *     @OA\Parameter (
     *     in="path",
     *     name="organisationId",
     *     required=true,
     *     description="organisationId",
     *     @OA\Schema (type="string"),
     *     ),
     *     @OA\Parameter (
     *     in="path",
     *     name="planboardId",
     *     required=true,
     *     description="planboardId",
     *     @OA\Schema (type="string"),
     *     ),
     *     @OA\Response(response=200, description="Period for order has been updated."),
     *     @OA\Response(response=401, description="Unauthorized"),
     *     @OA\Response(response=404, description="Organisation or planboard not found")
     * )
     */
    public function updateOrganisationPlanboardOrderPeriod($organisationId, $planboardId, Request $request)
    {

        Log::info('updateOrganisationPlanboardOrderPeriod');

        $organisation = (new Organisation())->getOrganisationById($organisationId);

        if ($organisation === null) {
            $jsonData = ['status' => 'not found', 'message' => 'organisation not found'];
            return Response()->json($jsonData, 404);
        }

        $token = $request->bearerToken();
        if ($token !== $organisation->bearertoken) {
            return Response()->json(['status' => 'Unauthorized'], 401);
        }


        $planning = (new Planning())->getPlanningById($planboardId);
        if ($planning === null) {
            $jsonData = ['status' => 'not found', 'message' => 'planboard not found'];
            return Response()->json($jsonData, 404);
        }

        $data = $request->all();

        $new_period_date = Arr::get($data, 'perioddate', null);
        if ($new_period_date === null) {
            Log::error('updateOrganisationPlanboardOrderPeriod: invalid perioddate');
            $jsonData = ['status' => 'not found', 'message' => 'Perioddate not found in request'];
            return Response()->json($jsonData, 404);
        }

        $new_period_label = Arr::get($data, 'periodlabel', null);
        if ($new_period_label === null) {
            Log::error('updateOrganisationPlanboardOrdedPeriod: invalid periodlabel');
            $jsonData = ['status' => 'not found', 'message' => 'Periodlabel not found in request'];
            return Response()->json($jsonData, 404);
        }
        $order = Arr::get($data, 'order', null);
        if ($order === null) {
            Log::error('updateOrganisationPlanboardOrderPeriod: orderdata missing');
            $jsonData = ['status' => 'not found', 'message' => 'Order not found in request'];
            return Response()->json($jsonData, 404);
        }

        if (is_string($order)) {
            $order = json_decode($order, true);
        }
        $current_routedata = $planning->routedata;
        if (is_string($current_routedata)) {
            $current_routedata = json_decode($current_routedata, true);

            $current_routedata['cells'] = array_values(PlanningBoardService::updatePeriod($current_routedata, $order, $new_period_date));

            $planning->routedata = json_encode($current_routedata);
            $planning->save();

        }
        return Response()->json(['status' => 'success']);
    }


    /**
     * @OA\Post(
     *     path="/api/v2/organisations/{organisationId}/planboards/{planboardId}/update_order_transport",
     *     description="Updates the transport of an order on the planboard",
     *     @OA\Parameter (
     *     in="path",
     *     name="organisationId",
     *     required=true,
     *     description="organisationId",
     *     @OA\Schema (type="string"),
     *     ),
     *     @OA\Parameter (
     *     in="path",
     *     name="planboardId",
     *     required=true,
     *     description="planboardId",
     *     @OA\Schema (type="string"),
     *     ),
     *     @OA\Response(response=200, description="Transport for order has been updated."),
     *     @OA\Response(response=401, description="Unauthorized"),
     *     @OA\Response(response=404, description="Organisation or planboard not found")
     * )
     */
    public function updateOrganisationPlanboardOrderTransport($organisationId, $planboardId, Request $request)
    {

	Log::info('updateOrganisationPlanboardOrderTransport');

        $organisation = (new Organisation())->getOrganisationById($organisationId);

        if($organisation === null) {
            $jsonData = ['status' => 'not found', 'message' => 'organisation not found'];
            return Response()->json($jsonData, 404);
        }

        $token = $request->bearerToken();
            if($token !== $organisation->bearertoken){
                return Response()->json(['status' => 'Unauthorized'], 401);
            }

	$planning = (new Planning())->getPlanningById($planboardId);
	if($planning === null){
            $jsonData = ['status' => 'not found', 'message' => 'planboard not found'];
            return Response()->json($jsonData, 404);
	}

	$data = $request->all();

	$new_transport_label = Arr::get($data, 'transportlabel', null);
	if($new_transport_label === null){
		Log::error('updateOrganisationPlanboardOrderTransport: invalid transportlabel');
		$jsonData = ['status' => 'not found', 'message' => 'Transportlabel not found in request'];
		return Response()->json($jsonData, 404);
	}
	$order = Arr::get($data, 'order', null);
	if($order === null){
		Log::error('updateOrganisationPlanboardOrderTransport: orderdata missing');
		$jsonData = ['status' => 'not found', 'message' => 'Order not found in request'];
                return Response()->json($jsonData, 404);
	}

	if(is_string($order)) {
		$order = json_decode($order, true);
	}

    $current_routedata = $planning->routedata;

    if(is_string($current_routedata)) {
        $current_routedata = json_decode($current_routedata, true);

        $current_routedata['cells'] = array_values(PlanningBoardService::updateTransport($current_routedata, $order, $new_transport_label));

        $planning->routedata = json_encode($current_routedata);
        $planning->save();
    }

	return Response()->json(['status'=>'success']);
    }

    /**
     * @OA\Post(
     *     path="/api/v2/organisations/{organisationId}/planboards/{planboardId}/locations/add",
     *     description="Adds a location to the planboard",
     *     @OA\Parameter (
     *     in="path",
     *     name="organisationId",
     *     required=true,
     *     description="organisationId",
     *     @OA\Schema (type="string"),
     *     ),
     *     @OA\Parameter (
     *     in="path",
     *     name="planboardId",
     *     required=true,
     *     description="planboardId",
     *     @OA\Schema (type="string"),
     *     ),
     *     @OA\Response(response=200, description="The specific planboard owned by the opganisation."),
     *     @OA\Response(response=401, description="Unauthorized"),
     *     @OA\Response(response=404, description="Organisation or planboard not found")
     * )
     */
    public function addOrganisationPlanboardLocation($organisationId, $planboardId , Request $request)
    {
        Log::info('Calling add location to planboard ' . $planboardId . ' of organisation ' . $organisationId);

        $organisation = (new Organisation())->getOrganisationById($organisationId);
        if($organisation === null) {
		// @todo Decide if this is a potential leak of information, since any wrong token can inform the caller
		// that a certain organisation uuid exists
		$jsonData = ['status' => 'not found', 'message' => 'organisation not found'];
		return Response()->json($jsonData, 404);
        }

	// we need to validate the token against the organisation
        $token = $request->bearerToken();
	if($token !== $organisation->bearertoken){
		Log::info('Invalid token in addOrganisationPlanboardLocation');
		return Response()->json(['status' => 'Unauthorized'], 401);
	}

	$planning = (new Planning())->getPlanningById($planboardId);
	if($planning === null){
            $jsonData = ['status' => 'not found', 'message' => 'planboard not found'];
            return Response()->json($jsonData, 404);
	}

	// Get the current planboard-data from the planning.
	$current_routedata = $planning->routedata;
	if(is_string($current_routedata)) {
		$current_routedata = json_decode($current_routedata, true);
	}

	$data = $request->all();

	Log::info('API AddLocation process: ' . json_encode($data));
    
	//save transport to database
	$transport_label = $data['transport_label'];

	// save period
	$period_label = $data['period_label'];
	$period_date = $data['period_date'];

	// Orders consist of entries like {"label":"Waskom","customer":"Brabander","code":"112233"}
	$orders = json_decode($data['orders'], true);

	// create a location, in case this is a new entry
	$location = PlanningBoardService::createAddableLocation($data['label'],$data['street'],$data['zipcode'],$data['city'],$data['country'],$orders);
        $location_exists = $this->address->getAddress($data['label'],$data['street'],$data['zipcode'],$data['city'],$data['country']);

        if($location_exists){
          Arr::set($location, "item_time", $location_exists->item_time);
          Arr::set($location, "overhead_time", $location_exists->overhead_time);
        }else{
          Arr::set($location, "item_time", $organisation->item_time);
          Arr::set($location, "overhead_time", $organisation->overhead_time);
        }

	// create a cell
	$newcell =  PlanningBoardService::createAddableCell($transport_label, $period_label, $period_date, $location,$current_routedata);

	
	// reform the planning routedata
	$planning->routedata = PlanningBoardService::addCellToPlanboard($current_routedata, $newcell);

	// save the cahnges
	$planning->save();

        return Response()->json(['status'=>'success']);
    }


}
