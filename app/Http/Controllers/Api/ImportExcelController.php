<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Imports\AddressImport;
use App\ImportSettings;
use App\Organisation;
use App\Planning;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\URL;
use Maatwebsite\Excel\Facades\Excel;

class ImportExcelController extends Controller
{

    private $planning;
    private $importSettings;
    private $organisation;

    public function __construct(Planning $planning, Organisation $organisation, ImportSettings $importSettings)
    {
        $this->planning = $planning;
        $this->organisation = $organisation;
        $this->importSettings = $importSettings;
    }



    public function convertExcelToArray(Request $request)
    {   
        $file = $request->file('file');


        //Check if the file exists
        if(!isset($file)) {
            $jsonData = ['status' => 'error', 'message' => 'No file found on the request'];
            return Response()->json($jsonData, 422);
        }

        $supportedMimeTypes = ['application/vnd.ms-excel', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'];


        if(!in_array($file->getMimeType(), $supportedMimeTypes)) {
            $jsonData = ['status' => 'error', 'message' => 'File type is not supported'];
            return Response()->json($jsonData, 422);
        }
    
        try {
           $toArray = (new AddressImport)->toArray($file);
        } catch (\Exception $exception) {
            Log::error('Problem converting Excel to array' . $exception->getMessage());
            $jsonData = ['status' => 'error', 'message' => 'Problem converting Excel to array'];
            return Response()->json($jsonData, 500);
        }

        $jsonData = ['headers' => [], 'data' => []];

        //Make data ready for the frontend
        $excelData = $toArray[0];

        foreach ($excelData as $eIdx => $data) {
            
                //If eIdx is zero we now this the head data
                if($eIdx === 0) {
                    $jsonData['headers'][] = $data;
                } else {
                    $jsonData['data'][$eIdx][] = $data;
                }
            
        }

        return Response()->json($jsonData, 200);
    }

    public function downloadExcelExample()
    {
        $filepath = '/examples/example.xls';

        //Check if the file exists on the server
        if(!File::exists(public_path($filepath))) {
            $jsonData = ['status' => 'not found', 'message' => 'file not found on the server'];
            return Response()->json($jsonData, 404);
        }

        return Response()->json($filepath, 200);
    }


    /**
     * @OA\Post(
     *     description="Import Excel file to a planning",
     *     path="/api/v1/organisations/{organisationId}/import-settings/{importSettingId}/plannings/{planningId}/import-to-planning",
     *     @OA\Parameter (
     *     name="organisationId",
     *     in="path",
     *     required=true,
     *     description="organisationId",
     *     @OA\Schema (type="string"),
     *     ),
     *     @OA\Parameter (
     *     name="planningId",
     *     in="path",
     *     description="planningId",
     *      required=true,
     *     @OA\Schema (type="integer"),
     *     ),
     *     @OA\Parameter (
     *     name="importSettingId",
     *     in="path",
     *     description="ImportSettingId",
     *      required=true,
     *     @OA\Schema (type="integer"),
     *     ),
     *     @OA\Response(response=200, description="Get signed Url to login to a planning"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=401, description="Unauthenicated"),
     *     @OA\Response(response=500, description="Cannot import planning"),
     * )
     */

    public function ImportExcelToPlanning(Request $request, $organisationId, $importSettingId, $planningId)
    {
        //Get the organisation and check if the user has permission to import an planning
        $organisation = $this->organisation->getOrganisationById($organisationId);

        if(!isset($organisation)) {
            $jsonData = ['status' => 'organisation not found', 'message' => 'organisation not found'];
            return Response()->json($jsonData, 404);
        }


        //Check if planning exists
        $planning = $this->planning->getPlanningById($planningId);

        if(!isset($planning)) {
            $jsonData = ['status' => 'not found', 'message' => 'planning not found'];
            return Response()->json($jsonData, 404);
        }

        if($planning->permission_planning_id < 2) {
            $jsonData = ['status' => 'no writing permissions', 'message' => 'U have not the right permissions to write to the planning'];
            return Response()->json($jsonData, 403);
        }

        $user = Auth()->user();

        if($organisation->user_id !== $user->id && !$organisation->isPartOfTheOrganisation($user)) {
            $jsonData = ['status' => 'not allowed', 'message' => 'not allowed to import an excel to a planning'];
            return Response()->json($jsonData, 403);
        }

        //Check if the importSetting exists and belongs to the organisation

        $importSettings = $this->importSettings->getImportSettingByIdAndOrganisationId($importSettingId, $organisation->id);

        if(!isset($importSettings)) {
            $jsonData = ['status' => 'not found', 'message' => 'import setting not found'];
            return Response()->json($jsonData, 404);
        }


        $file = $request->file('file');

        //Check if the file exists
        if(!isset($file)) {
            $jsonData = ['status' => 'error', 'message' => 'No file found on the request'];
            return Response()->json($jsonData, 422);
        }

        $supportedMimeTypes = ['application/vnd.ms-excel', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'];

        $mimeType = $file->getMimeType();

        if(!in_array($mimeType, $supportedMimeTypes)) {
            $jsonData = ['status' => 'not valid format', 'message' => 'File is not an XLS or XLSX file'];
            return Response()->json($jsonData, 422);
        }

        //Convert the Excel to an array
        try {
            $toArray = Excel::toArray(new AddressImport(), $file);
        } catch (\Exception $exception) {
            Log::error('Problem converting Excel to array' . $exception->getMessage());
            $jsonData = ['status' => 'error', 'message' => 'Problem converting Excel to array'];
            return Response()->json($jsonData, 500);
        }

        $jsonData = ['headers' => [], 'data' => []];
        //Make data ready for the frontend
        $excelData = $toArray[0];

        foreach ($excelData as $eIdx => $eData) {
            foreach ($eData as $dIdx => $data) {
                //If eIdx is zero we now this the head data
                if($eIdx === 0) {
                    $jsonData['headers'][] = $data;
                } else {
                    $jsonData['excelData'][$eIdx][] = $data;
                }
            }
        }

        $settings = [];

        //Get the headers and add them to the settings
        foreach ($jsonData['headers'] as $hIdx => $header) {
            if($importSettings->address_header === $header) {
                $settings['address_header'] = $hIdx;
                continue;
            }

            if($importSettings->place_header === $header) {
                $settings['place_header'] = $hIdx;
                continue;
            }

            if($importSettings->period_header === $header) {
                $settings['period_header'] = $hIdx;
                continue;
            }

            if($importSettings->transport_header === $header) {
                $settings['transport_header'] = $hIdx;
                continue;
            }

            if($importSettings->overhead_time_header === $header) {
                $settings['overhead_time_header'] = $hIdx;
                continue;
            }

            if($importSettings->item_time_header === $header) {
                $settings['item_time_header'] = $hIdx;
                continue;
            }

            if($importSettings->item_header === $header) {
                $settings['item_header'] = $hIdx;
                continue;
            }
        }

        $required_settings = ['address_header', 'place_header', 'period_header', 'transport_header'];

        $requiredSettingsFound = false;

        //Check if all the required settings are found
        foreach ($required_settings as $setting) {
            if(!array_key_exists($setting, $settings))  {
                break;
            }
            $requiredSettingsFound = true;
        }

        if(!$requiredSettingsFound) {
            $jsonData = ['status' => 'error', 'message' => 'Cannot import excel to planning are you sure import settings are correct'];
            return Response()->json($jsonData, 422);
        }

        //Filter the data to the planning
        $planningData = [];

        foreach ($jsonData['excelData'] as $data) {

            //Make a period with the period data
            if(count($planningData) < 1) {
                $planningData[] = [
                    'periodId' => count($planningData) + 1,
                    'label' => $data[$settings['period_header']],
                    'transport' => []
                ];

                $periodIdx = count($planningData) - 1;

                //Set the first transport with the route
                $planningData[$periodIdx]['transport'][] = [
                    'label' => $data[$settings['transport_header']],
                    'routes' => [],
                    'totalTime' => '',
                    'startingRoute' => false
                ];

                $overHeadTime = isset($data[$settings['overhead_time_header']]) ? $data[$settings['overhead_time_header']] : 0;
                $items = isset($data[$settings['item_header']]) ? count(explode(',' , $data[$settings['item_header']])) : 0;
                $itemTime = isset($data[$settings['item_time_header']]) ? $data[$settings['item_time_header']] : 0;

                if(!is_int($itemTime)) {
                    $itemTime = 0;
                }

                $transportIdx = count($planningData[$periodIdx]['transport']) - 1;
                $planningData[$periodIdx]['transport'][$transportIdx]['routes'][] = [
                    "address" => $data[$settings["address_header"]] . ' ' . $data[$settings['place_header']],
                    "travelTime" => "",
                    "overheadTime" => $overHeadTime,
                    "items" => $items,
                    'itemTime' => $itemTime,
                    "standingTime" => $items * $itemTime
                ];

            } else {
                //Loop over the planningdata and check if period exists
                foreach ($planningData as $pIdx => $pData) {                    //
                    if($pData['label'] !== $data[$settings['period_header']]) {
                        $planningData[] = [
                            'periodId' => count($planningData) + 1,
                            'label' => $data[$settings['period_header']],
                            'transport' => []
                        ];

                        $periodIdx = count($planningData) - 1;

                        //Set the first transport with the route
                        $planningData[$periodIdx]['transport'][] = [
                            'label' => $data[$settings['transport_header']],
                            'routes' => [],
                            'totalTime' => '',
                            'startingRoute' => false
                        ];

                        $overHeadTime = isset($data[$settings['overhead_time_header']]) ? $data[$settings['overhead_time_header']] : 0;
                        $items = isset($data[$settings['item_header']]) ? count($data[$settings['item_header']]) : 0;
                        $itemTime = isset($data[$settings['item_time_header']]) ? $data[$settings['item_time_header']] : 0;

                        if(!is_int($itemTime)) {
                            $itemTime = 0;
                        }

                        $transportIdx = count($planningData[$periodIdx]['transport']) - 1;
                        $planningData[$periodIdx]['transport'][$transportIdx]['routes'] = [
                            "address" => $data[$settings["address_header"]] . ' ' . $data[$settings['place_header']],
                            "travelTime" => "",
                            "overheadTime" => $overHeadTime,
                            "items" => $items,
                            'itemTime' => $itemTime,
                            "standingTime" => $items * $itemTime
                        ];
                    } else {
                        $overHeadTime = isset($data[$settings['overhead_time_header']]) ? $data[$settings['overhead_time_header']] : 0;
                        $items = isset($data[$settings['item_header']]) ? count(explode(',' , $data[$settings['item_header']])) : 0;
                        $itemTime = isset($data[$settings['item_time_header']]) ? $data[$settings['item_time_header']] : 0;

                        if(!is_int($itemTime)) {
                            $itemTime = 0;
                        }

                        foreach ($planningData[$pIdx]['transport'] as $tIdx => $transportData) {
                            $transport = $planningData[$pIdx]['transport'][$tIdx];
                            if($transport['label'] === $data[$settings['transport_header']]) {
                                $planningData[$pIdx]['transport'][$tIdx]['routes'][] = [
                                    "address" => $data[$settings["address_header"]] . ' ' . $data[$settings['place_header']],
                                    "travelTime" => "",
                                    "overheadTime" => $overHeadTime,
                                    "items" => $items,
                                    'itemTime' => $itemTime,
                                    "standingTime" => $items * $itemTime
                                ];
                            } else if($tIdx === count($planningData[$pIdx]['transport']) - 1) {
                                $planningData[$periodIdx]['transport'][] = [
                                    'label' => $data[$settings['transport_header']],
                                    'routes' => [],
                                    'totalTime' => '',
                                    'startingRoute' => false
                                ];

                                $transportIdx = count($planningData[$periodIdx]['transport']) - 1;
                                $planningData[$periodIdx]['transport'][$transportIdx]['routes'][] = [
                                    "address" => $data[$settings["address_header"]] . ' ' . $data[$settings['place_header']],
                                    "travelTime" => "",
                                    "overheadTime" => $overHeadTime,
                                    "items" => $items,
                                    'itemTime' => $itemTime,
                                    "standingTime" => $items * $itemTime
                                ];
                            }
                        }

                    }
                }
            }
        }

        //Store the data into the planning

        try {
            $this->planning->storeRouteData($planning, json_encode($planningData));
        } catch (\Exception $exception) {
            Log::error('Something went wrong importing the planning' . ' ' . $exception->getMessage());
            $jsonData = ['status' => 'error', 'message' => 'Cannot import the planning'];
            return Response()->json($jsonData, 500);
        }


        //Make a signed url to login an user
        $signedUrl = URL::temporarySignedRoute(
            'planning.login', Carbon::now()->addHour(), ['userId' => $user->id , 'organisationId' => $organisation->id, 'planningId' => $planning->id]
        );

        $jsonData = ['status' => 'success', 'url' => $signedUrl, 'message' => 'Planning successfully added'];

        return Response()->json($jsonData, 200);
    }

    public function convertExcelExampleDataToPlanning(Request $request)
    {
        $filepath = 'examples/example.xls';

        if(!File::exists(public_path($filepath))) {
            return Response()->json('file not found', 404);
        }

        try {
            $toArray = Excel::toArray(new AddressImport(), $filepath);
        } catch (\Exception $exception) {
            Log::error('Problem converting Excel to array' . $exception->getMessage());
            $jsonData = ['status' => 'error', 'message' => 'Problem converting Excel to array'];
            return Response()->json($jsonData, 500);
        }

        $jsonData = ['headers' => [], 'data' => []];
        //Make data ready for the frontend
        $excelData = $toArray[0];

        foreach ($excelData as $eIdx => $eData) {
            foreach ($eData as $dIdx => $data) {
                //If eIdx is zero we now this the head data
                if($eIdx === 0) {
                    $jsonData['headers'][] = $data;
                } else {
                    $jsonData['excelData'][$eIdx][] = $data;
                }
            }
        }

        $planningData = [];

        //Make hardcoded data settings

        $settings = [];
        $settings['address_header'] = 1;
        $settings['place_header'] = 3;
        $settings['period_header'] = 5;
        $settings['transport_header'] = 4;
        $settings['overhead_time_header'] = 6;
        $settings['item_time_header'] = 7;
        $settings['item_header'] = 8;


        foreach ($jsonData['excelData'] as $data) {

            //Make a period with the period data
            if(count($planningData) < 1) {
                $planningData[] = [
                    'periodId' => count($planningData) + 1,
                    'label' => $data[$settings['period_header']],
                    'transport' => []
                ];

                $periodIdx = count($planningData) - 1;

                //Set the first transport with the route
                $planningData[$periodIdx]['transport'][] = [
                    'label' => $data[$settings['transport_header']],
                    'routes' => [],
                    'totalTime' => '',
                    'startingRoute' => false
                ];

                $overHeadTime = isset($data[$settings['overhead_time_header']]) ? $data[$settings['overhead_time_header']] : 0;
                $items = isset($data[$settings['item_header']]) ? count(explode(',' , $data[$settings['item_header']])) : 0;
                $itemTime = isset($data[$settings['item_time_header']]) ? $data[$settings['item_time_header']] : 0;

                $transportIdx = count($planningData[$periodIdx]['transport']) - 1;
                $planningData[$periodIdx]['transport'][$transportIdx]['routes'][] = [
                    "address" => $data[$settings["address_header"]] . ' ' . $data[$settings['place_header']],
                    "travelTime" => "",
                    "overheadTime" => $overHeadTime,
                    "items" => $items,
                    'itemTime' => $itemTime,
                    "standingTime" => $items * $itemTime
                ];

            } else {
                //Loop over the planningdata and check if period exists
                foreach ($planningData as $pIdx => $pData) {                    //
                    if($pData['label'] !== $data[$settings['period_header']]) {
                        $planningData[] = [
                            'periodId' => count($planningData) + 1,
                            'label' => $data[$settings['period_header']],
                            'transport' => []
                        ];

                        $periodIdx = count($planningData) - 1;

                        //Set the first transport with the route
                        $planningData[$periodIdx]['transport'][] = [
                            'label' => $data[$settings['transport_header']],
                            'routes' => [],
                            'totalTime' => '',
                            'startingRoute' => false
                        ];

                        $overHeadTime = isset($data[$settings['overhead_time_header']]) ? $data[$settings['overhead_time_header']] : 0;
                        $items = isset($data[$settings['item_header']]) ? count($data[$settings['item_header']]) : 0;
                        $itemTime = isset($data[$settings['item_time_header']]) ? $data[$settings['item_time_header']] : 0;

                        $transportIdx = count($planningData[$periodIdx]['transport']) - 1;
                        $planningData[$periodIdx]['transport'][$transportIdx]['routes'] = [
                            "address" => $data[$settings["address_header"]] . ' ' . $data[$settings['place_header']],
                            "travelTime" => "",
                            "overheadTime" => $overHeadTime,
                            "items" => $items,
                            'itemTime' => $itemTime,
                            "standingTime" => $items * $itemTime
                        ];
                    } else {
                        $overHeadTime = isset($data[$settings['overhead_time_header']]) ? $data[$settings['overhead_time_header']] : 0;
                        $items = isset($data[$settings['item_header']]) ? count(explode(',' , $data[$settings['item_header']])) : 0;
                        $itemTime = isset($data[$settings['item_time_header']]) ? $data[$settings['item_time_header']] : 0;

                        foreach ($planningData[$pIdx]['transport'] as $tIdx => $transportData) {
                            $transport = $planningData[$pIdx]['transport'][$tIdx];
                            if($transport['label'] === $data[$settings['transport_header']]) {
                                $planningData[$pIdx]['transport'][$tIdx]['routes'][] = [
                                    "address" => $data[$settings["address_header"]] . ' ' . $data[$settings['place_header']],
                                    "travelTime" => "",
                                    "overheadTime" => $overHeadTime,
                                    "items" => $items,
                                    'itemTime' => $itemTime,
                                    "standingTime" => $items * $itemTime
                                ];
                            } else if($tIdx === count($planningData[$pIdx]['transport']) - 1) {
                                $planningData[$periodIdx]['transport'][] = [
                                    'label' => $data[$settings['transport_header']],
                                    'routes' => [],
                                    'totalTime' => '',
                                    'startingRoute' => false
                                ];

                                $transportIdx = count($planningData[$periodIdx]['transport']) - 1;
                                $planningData[$periodIdx]['transport'][$transportIdx]['routes'][] = [
                                    "address" => $data[$settings["address_header"]] . ' ' . $data[$settings['place_header']],
                                    "travelTime" => "",
                                    "overheadTime" => $overHeadTime,
                                    "items" => $items,
                                    'itemTime' => $itemTime,
                                    "standingTime" => $items * $itemTime
                                ];
                            }
                        }

                    }
                }
            }
        }


        return Response()->json($planningData, 200);
    }


}
