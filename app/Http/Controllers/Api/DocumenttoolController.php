<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;

class DocumenttoolController extends Controller
{
    public function sendToDocumenttool(Request $request){
        //return $request->json;
        $client = new Client();
        $client->header = "Authorization: Bearer ".env('DOCUMENTTOOL').", 'Content-Type' : 'application/json',
        'Accept': ' application/json'";
        $res = $client->post('https://documenttool.online/api/convert-only', $request->json);
        return [$res];
    }
}
