<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Organisation;
use Illuminate\Http\Request;

class OrganisationController extends Controller
{
    public $organisation;

    public function __construct(Organisation $organisation)
    {
        $this->organisation = $organisation;
    }

    /**
     * @OA\Get(
     *     path="/api/v1/organisations",
     *     description="Get all organisations where an user belong to",
     *     @OA\Response(response=200, description="Get organisations"),
     *     @OA\Response(response=401, description="Unauthorized")
     * )
     */

    public function getOrganisations()
    {
        $user = Auth()->user();
        $ownedOrganisations = $this->organisation->getOwnerOrganisationsByUserId($user->id)->toArray();
        $sharedOrganisations = $user->organisationUser()->get()->toArray();
        $organisations = array_merge($ownedOrganisations, $sharedOrganisations);
        $jsonData = ['status' => 'success', 'data' => $organisations, 'message' => 'found organisations'];
        return Response()->json($jsonData, 200);
    }

    /**
     * @OA\Get(
     *     path="/api/v1/organisations/{organisationId}",
     *     description="Get all organisations where an user belong to",
     *     @OA\Parameter (
     *          in="path",
     *     name="organisationId",
     *     required=true,
     *     @OA\Schema (type="string")
     *     ),
     *     @OA\Response(response=200, description="Get organisation"),
     *     @OA\Response(response=401, description="Unauthorized"),
     *     @OA\Response(response=404, description="Organisation not found")
     * )
     */

    public function getOrganisation($organisationId)
    {
        $organisation = $this->organisation->getOrganisationById($organisationId);

        if(!isset($organisation)) {
            $jsonData = ['status' => 'not found', 'message' => 'Organisation not found'];
            return Response()->json($jsonData, 404);
        }

        $user = Auth()->user();

        //Check if user is part of the organisation
        if($organisation->user_id !== $user->id && !$organisation->isPartOftheOrganisation($user)) {
            $jsonData = ['status' => 'forbidden', 'message' => 'Not allowed to view this organisation'];
            return Response()->json($jsonData, 403);
        }
        $jsonData = ['status' => 'success', 'data' => $organisation, 'message' => 'organisation found'];
        return Response()->json($jsonData, 200);
    }
}
