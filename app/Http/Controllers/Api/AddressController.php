<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\AddressService;
use App\AddressHash;

class AddressController extends Controller
{   public $addresshash;


    public function __construct(AddressHash $addresshash)
    {
       $this->addresshash = $addresshash;
   
    }
    public function getLatLong(Request $request){
        
        $address = $_GET['address'];
        if(!isset($address) || $address == ""){
            return "string empty";
        }
        $hash = md5($address);
        //$latlong = AddressService::getLatLong($hash);
        $addressHash = $this->addresshash->getAddressHashByHash($hash);
    
        if ($addressHash ==  null){
            $addressHash = AddressService::save_hash($address);
        }
        return $addressHash;
    }
    
    public function saveHash(Request $request){
        $address = $request['address'];
        $hash = md5($address);
        $addressHash = $this->addresshash->getAddressHashByHash($hash);
        if ($addressHash ==  null){
            $addressHash = AddressService::save_hash($address);
        }
        return $addressHash;
    }
}
