<?php

namespace App\Http\Controllers;

use App\Helpers\Calculator;
use App\Helpers\MandrillHelper;
use App\Route;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function getIndexPage()
    {
        return view('pages.index');
    }


    public function getSpecialPage()
    {
        return view('pages.special');
    }
    //TODO function to needs to be more simplerer

    public function getShortestRoute()
    {
        $route = new Route();
        $routes = DB::table('routes')->where('origin', '=', 'Zoutkamp,nl')->get();

        $shortestRoute = new Calculator();
        $shortroute = $shortestRoute->shortestDistance($routes);
        dd($shortroute);
    }

    public function getShortestDuration()
    {
        $route = new Route();
        $routes = DB::table('routes')->where('origin', '=', 'Zoutkamp,nl')->get();

        $shortestRoute = new Calculator();
        $shortroute = $shortestRoute->shortestDuration($routes);
        dd($shortroute);
    }

    public function contact(Request $request)
    {
        if($request->method() === 'GET') {
            return view('pages.contact');
        }

        $request->validate([
            'name' => ['required'],
            'email' => ['required', 'email'],
            'message' => ['required']
        ]);

        $request->validate([
            'name' => ['required'],
            'email' => ['required', 'email'],
            'description' => ['required']
        ]);


        $sended = MandrillHelper::sendContactEmail($request['email'], $request['name'], '');

        if(!$sended) {
            Session::flash('error', 'Email kon niet verstuurd, probeer later opnieuw');
            return back();
        }

        Session::flash('success', 'Formulier verstuurd');
        return redirect()->route('pages.index');


    }
}
