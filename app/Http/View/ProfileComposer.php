<?php


namespace App\Http\View;


use App\User;
use Illuminate\View\View;

class ProfileComposer
{
    /**
     * User Model implementation
     *
     * @var User $user
     * @return void
     */

    protected $users;

    public function __construct(User $users)
    {
        $this->users = $users;
    }

    public function compose(View $view)
    {
        $view->with('userCount', $this->users->count());
    }
}
