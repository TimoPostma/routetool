<?php

namespace App\Http\View;

use App\Organisation;
use Illuminate\Support\Facades\Route;
use Illuminate\View\View;

class OrganisationComposer
{
    protected $organisation;

    public function __construct(Organisation $organisation)
    {
        $this->organisation = $organisation;
    }

    public function compose(View $view)
    {
        //Get all the route parameters
        $routeParameters = Route::getCurrentRoute()->parameters();

        //@TODO maybe better to call this organisationId
        $organisationId = $routeParameters['id'];
        //Get the organisation
        $organisation = $this->organisation->getOrganisationById($organisationId);
        $view->with('navOrganisation', $organisation);
    }
}
