<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Cache;

class EnsureSsoTokenIsValid
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
//	if(Route::currentRouteName() == 'login'){
//		// prevent indefinite loops
//		return $next($request);
//	}

	$signature = $request->input('signature');
	if($signature === null){
		// no signature to check 
		return $next($request);
	}

	$cacheKey = 'consumable|' . $signature;
        $cacheExists = Cache::has($cacheKey);


        if($cacheExists || !$request->hasValidSignature()) {
            Session()->flash('warning', 'Token expired');
            return redirect('login');
        }

	$expiresWhen = $request->input('expires');
        Cache::put($cacheKey, 'used', $expiresWhen);
        $userId = $request->input('userId');
        Auth()->loginUsingId($userId);

        return $next($request);
    }
}
