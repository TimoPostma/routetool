<?php

namespace App\Http\Middleware;

use App\Admin;
use Closure;
use Illuminate\Support\Str;

class CheckIfApiAdminTokenExists
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!isset(Auth()->user()->apitoken)) {
            $adminObject = new Admin();
            $admin = $adminObject->getAdminById(Auth()->user()->id);
            $token = Str::random(80);

            //Check if the admin api token exists
            while ($adminObject->checkIfApiTokenExists($token)) {
                $token = Str::random(80);
            }

            $adminObject->updateApiToken($admin, $token);

        }

        return $next($request);
    }
}
