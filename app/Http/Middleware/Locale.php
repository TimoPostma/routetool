<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;
use Closure;

class Locale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {   
        
        if(Auth::user()){
            $user = Auth::user();
            $locale = $user->locale;

        }else if(Session()->has('locale')){
            $locale = Session()->get('locale');
        }else{
            $locale = 'en';
        }
        App()->setLocale($locale);
        Session()->put('locale', $locale);
        return $next($request);
    }
}
