<?php

namespace App\Http\Middleware;
use Session;

use Closure;

class CheckIfUserIsActivated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth()->user();

        //Check if user is logged in
        if(Auth()->check()) {
            //If user isn't activated log the suer out

            if(!$user->activated) {
                Auth()->logout();
                Session::flash('warning', __('sessions.user_not_activated'));
                return redirect()->route('login');

            }
        }

        return $next($request);
    }
}
