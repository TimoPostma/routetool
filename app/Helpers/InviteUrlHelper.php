<?php


namespace App\Helpers;


use App\Invitation;
use Illuminate\Support\Str;

class InviteUrlHelper
{
    protected $invitation;

    public function __construct(Invitation $invitation)
    {
        $this->invitation  = $invitation;
    }

    public static function generateInvitationUrl($userId, $signature, $timestamp)
    {
        return Route('invitation') . '?userId=' . $userId . '&signature=' . $signature . '&timestamp=' .$timestamp;
    }

    public static function generateOrganisationUrl($organisationId, $signature, $timestamp)
    {
        return Route('organisation.invite') . '?organisationId=' . $organisationId . '&signature=' . $signature . '&timestamp=' .$timestamp;
    }
}
