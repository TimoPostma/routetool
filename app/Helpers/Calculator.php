<?php


namespace App\Helpers;
use App\Route;
use Carbon\CarbonInterval;

class Calculator
{
    public function shortestDistance($routes)
    {
        if(empty($routes)) {
            return $shortestDistances = [];
        }

        $shortestDistances = [];
        $endRoute = '';
        $endpoint = false;
        $longestDistance = 0;

        $index = 0;

        $origins = [];
        $destinations = [];
        $origin = 'start';
        $calculateFromOrigin = false;
        $totalKm = 0;

        while (!$endpoint) {
            $shortestDistance = 0;


            if($endRoute === $origin) {
                break;
            }

            $beginPoint = true;

            foreach ($routes as $route) {

                if($longestDistance < $route->distance) {
                    $longestDistance = $route->distance;
                    $endRoute = $route->destination;
                }

                if($route->distance < $shortestDistance || $beginPoint) {
                    $beginPoint = false;
                    $shortestDistance = $route->distance;
                    if($calculateFromOrigin) {
                        $origin = $route->origin;
                    } else {
                        $origin = $route->destination;
                    }
                }

            }

            $calculateFromOrigin = false;


            $shortestDistances[$index]['distance'] = $shortestDistance;
            $shortestDistances[$index]['origin'] =  $route->origin;
            $shortestDistances[$index]['destination'] = $origin;
            $totalKm += $shortestDistance;




            $routes = $this->getDestinations($origin);

            //It is the endpoint but not nessarcy the fastest route


            if(count($routes) === 0) {
                $calculateFromOrigin = true;
                $routes = $this->getOrigins($origin, $origins);
            } else {
                $routes = $this->getDestinations($origin);
            }

            $index++;

        }
        $shortestDistances['total_km'] = $totalKm / 1000 . 'km';

        return $shortestDistances;

    }

    public function shortestDuration($routes)
    {
        if(empty($routes)) {
            return $shortestDistances = [];
        }

        $shortestDistances = [];
        $endRoute = '';
        $endpoint = false;
        $longestDistance = 0;

        $index = 0;

        $origins = [];
        $destinations = [];
        $origin = 'start';
        $calculateFromOrigin = false;
        $totalTime = 0;

        while (!$endpoint) {
            $shortestDistance = 0;


            if($endRoute === $origin) {
                break;
            }

            $beginPoint = true;

            foreach ($routes as $route) {


                if($longestDistance < $route->duration) {
                    $longestDistance = $route->duration;
                    $endRoute = $route->destination;
                }

                if($route->duration < $shortestDistance || $beginPoint) {
                    $beginPoint = false;
                    $shortestDistance = $route->duration;
                    if($calculateFromOrigin) {
                        $origin = $route->origin;
                    } else {
                        $origin = $route->destination;
                    }
                }

            }

            $calculateFromOrigin = false;


            $shortestDistances[$index]['duration'] = $shortestDistance;
            $shortestDistances[$index]['origin'] =  $route->origin;
            $shortestDistances[$index]['destination'] = $origin;
            $totalTime+= $shortestDistance;


            $routes = $this->getDestinations($origin);

            //It is the endpoint but not nessarcy the fastest route


            if(count($routes) === 0) {
                $calculateFromOrigin = true;
                $routes = $this->getOrigins($origin, $origins);
            } else {
                $routes = $this->getDestinations($origin);
            }

            $index++;

        }
        $shortestDistances['total_time'] = CarbonInterval::seconds($totalTime)->cascade()->forHumans();

        return $shortestDistances;
    }

    protected function getDestinations($origin)
    {
        $routes = new Route();
        return $routes->getDestinations($origin);
    }

    protected function getOrigins($destination, $origins)
    {
        $routes = new Route();
        return $routes->getOrigins($destination, $origins);
    }
}
