<?php

namespace App\Imports;

use App\Route;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithValidation;


class AddressImport implements WithMapping, WithValidation
{
    use Importable;


    public function map($row): array
    {
        foreach ($row as $cIdx => $column) {
            $validDate = $this->checkIfColumnIsValidDate($column);

            if($validDate) {
                try {
                  
                $row[$cIdx] = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($column);
                }catch(\Throwable $t){
                    print($t->getmessage());die();
                    }
            }
        }

        return $row;
    }

    public function rules(): array
    {
        return [];
    }

    private function checkIfColumnIsValidDate($column) : bool
    {
        $validDate = false;
        try {
            Carbon::parse($column);
            $validDate = true;
        } catch (\Exception $exception) {

        }
        return $validDate;
    }
}
