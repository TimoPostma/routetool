<?php

namespace App\Exceptions;

use Throwable;
use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Support\Arr;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Throwable $throwable
     * @return void
     *
     * @throws \Exception
     */
    public function report(\Throwable $throwable)
    {
        parent::report($throwable);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable $throwable
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Exception
     */
    public function render($request, \Throwable $throwable)
    {
        return parent::render($request, $throwable);
    }

    public function unauthenticated($request, AuthenticationException $exception)
    {
        if($request->isJson()) {
            abort(401, 'Unauthorized');
        }

        $guard = Arr::get($exception->guards(), 0);

        //The api guards

        $guards = ['api', 'admin_api', 'passport_api'];

        //If guard is api or admin api return unauthorized
        if(in_array($guard, $guards)) {
            return Response()->json('unauthorized', 401);
        }

        switch ($guard) {
            case 'admin' :
                $login = 'admin.login';
                break;
            default :
                $login = 'login';
        }
        return redirect()->route($login);
    }
}
