<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class GenerateAdminAccount extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'admin:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate an admin account, for monitoring the Produvar route tool';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('we are going to generate an admin account');
        $name = $this->ask('What is your name?');

        if(trim($name) === '') {
            $this->error('Name cannot be empty');
            return;
        }


        $email = $this->ask('What is your email address');

        if(trim($email) === '') {
            $this->error('Email cannot be empty');
            return;
        }

        if(!preg_match('/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/', $email)) {
            $this->error('Not a valid email');
            return;
        }

        $password = $this->secret('Which password you are going to use?');

        if(strlen($password) < 8) {
            $this->error('Password needs 8 characters');
        }

        $this->info('We are now generating the admin account');

        try {
            DB::table('admins')->insert([
                'name' => $name,
                'email' => $email,
                'password' => bcrypt($password),
                'api_token' => Str::random(80),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
        } catch (\Exception $exception) {
            $this->error('Admin account could not be generated:' . ' ' . $exception->getMessage());
            return;
        }

        Log::info('Admin account' . ' ' . $name . ' ' . 'has been generated');


    }
}
