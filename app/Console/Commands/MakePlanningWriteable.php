<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class MakePlanningWriteable extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'plannings:writeable';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('make all plannings writeable');

        $plannings = DB::table('plannings')->whereNull('permission_planning_id')->get();

        foreach ($plannings as $planning) {
            DB::table('plannings')->where('id', $planning->id)->update([
                'permission_planning_id' => 2
            ]);
        }
    }
}
