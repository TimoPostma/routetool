<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class GenerateUserAccount extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to generate a user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info('we are going to generate a user account');
        $name = $this->ask('What is the name of the user?');
 
        if(trim($name) === '') {
            $this->error('Name cannot be empty');
            return;
        }


        $email = $this->ask('What is the email address of the user?');

        if(trim($email) === '') {
            $this->error('Email cannot be empty');
            return;
        }

        if(!preg_match('/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/', $email)) {
            $this->error('Not a valid email');
            return;
        }

        $password = $this->secret('Which password is this user going to use?');

        if(strlen($password) < 8) {
            $this->error('Password needs 8 characters');
        }

        $this->info('We are now generating the user account');

        try {
            DB::table('users')->insert([
                'name' => $name,
                'email' => $email,
                'password' => bcrypt($password),
                'api_token' => Str::random(80),
                'activated' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
        } catch (\Exception $exception) {
            $this->error('Admin account could not be generated:' . ' ' . $exception->getMessage());
            return;
        }

        Log::info('Admin account' . ' ' . $name . ' ' . 'has been generated');

    }
}
