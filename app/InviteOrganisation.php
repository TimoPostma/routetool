<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InviteOrganisation extends Model
{
    public function organisation()
    {
        return $this->belongsTo('App\Organisation');
    }

    public function storeOrganisationInvitation($organisationId, $signature, $expiration_date, $url, $timestamp)
    {
        $this->url = $url;
        $this->organisation_id = $organisationId;
        $this->expiration_date = $expiration_date;
        $this->signature = $signature;
        $this->timestamp = $timestamp;
        $this->save();
    }

    public function checkIfSignatureExists($signature)
    {
        return $this->where('signature', $signature)->exists();
    }

    public function getInvitationOrganisation($organisationId)
    {
        return $this->where('organisation_id', $organisationId)->first();
    }

    public function getInvitationOrganisationByUrl($url)
    {
        return $this->where('url', $url)->first();
    }

    public function checkIfTimeStampAndSignatureAreValid($signature, $timestamp, $organisationId)
    {
        return $this->where('signature', $signature)->where('timestamp', $timestamp)->where('organisation_id', $organisationId)->exists();
    }

    public function getUniqueUrl($signature, $timestamp, $organisationId)
    {
        return $this->where('signature', $signature)->where('timestamp', $timestamp)->where('organisation_id', $organisationId)->first();
    }
}
