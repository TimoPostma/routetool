<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendActivationEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $signedUrl;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($signedUrl)
    {
        $this->signedUrl = $signedUrl;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->from('noreply@routetool.local')
            ->subject('activate account')
            ->view('email.activation_email');
    }
}
