<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImportSettings extends Model
{
    public function organisation()
    {
        return $this->belongsTo('App\Organisation');
    }

    public function getImportSettingByOrganisationId($organisationId)
    {
        return $this->where('organisation_id', $organisationId)->get();
    }

    public function storeImportSettings($request, $organisationId)
    {
        $this->name = $request->name;
        $this->description = $request->description;
        $this->address_header = $request->address_header;
        $this->place_header =$request->place_header;
        $this->period_header = $request->period_header;
        $this->transport_header = $request->transport_header;
        $this->overhead_time_header = $request->overhead_time_header;
        $this->item_time_header =$request->item_time_header;
        $this->item_header = $request->item_header;
        $this->organisation_id = $organisationId;
        $this->save();
    }

    public function getImportSettingByIdAndOrganisationId($importSettingId, $organisationId)
    {
        return $this->where('id', $importSettingId)->where('organisation_id', $organisationId)->first();
    }
}
