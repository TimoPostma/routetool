<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PermissionPlanning extends Model
{
    public function plannings()
    {
        return $this->hasMany('App\PermissionPlanning');
    }

    public function getPermissionLevel($level)
    {
        return $this->where('permission', $level)->first();
    }
}
