<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class addressHash extends Model
{
    use HasFactory;


    public function saveHash($hash,$lat,$long){
        $this->hash = $hash;
        $this->lat = $lat;
        $this->long = $long;
       
        $this->save();
        return $this;
    }
    public function getAddressHashByHash($hash){
        return $this->where('hash', $hash)->first();
    }
}
