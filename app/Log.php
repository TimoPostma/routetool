<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    public function storeLog($status , $description)
    {
        $this->status = $status;
        $this->description = $description;
        $this->save();
    }

    public function getLogs($level)
    {
        return $this->where('level', '>=' , $level)->orderBy('created_at', 'DESC')->get();
    }
}
