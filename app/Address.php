<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    use HasFactory;

    public function organisation()
    {
        return $this->belongsTo('App\Organisation');
    }
    public function getAddressById($id){
        return $this->where('id', $id)->first();
    }
    public function getAddressesByOrganisationId($id){
        return $this->where('organisation_id', $id)->orderBy('id', 'DESC')->get();
    }
    public function storeAddress($request,$organisationId){
        $this->label = $request->label;
        $this->description = $request->description;
        $this->street = $request->street;
        $this->zipcode = $request->zipcode;
        $this->city = $request->city;
        if (isset($request->item_time)){
            $this->item_time = $request->item_time;
        }
        if (isset($request->overhead_time)){
            $this->overhead_time = $request->overhead_time;
        }
        $this->country = $request->country;
        $this->organisation_id = $organisationId;
        $this->save();
        return $this;
    }
    public function updateAddress($request,$organisationId){

        $address = $this->where('id',$request->id)->first();

        $address->label = $request->label;
        $address->description = $request->description;
        $address->notes = $request->notes;
        $address->street = $request->street;
        $address->zipcode = $request->zipcode;
        $address->city = $request->city;
        $address->country = $request->country;
        
        if (isset($request->item_time)){
            $address->item_time = $request->item_time;
        }
        if (isset($request->overhead_time)){
            $address->overhead_time = $request->overhead_time;
        }
        if (isset($request->description)){
            $address->description = $request->description;
        }
        if (isset($request->notes)){
            $address->notes = $request->notes;
        }
        $address->organisation_id = $organisationId;
        $address->update();
        return $address;
    }
    public function deleteAddress($id){
        $address = $this->where('id',$id)->first();
        $address->delete();

    }
    public function getAddress($label,$street,$zipcode,$city,$country){
        return $this->select("*")->where('label','=',$label)->where('street','=',$street)->first();
    }
    public function addressExist($label,$street,$zipcode,$city,$country){
        return $address = $this->select("id")->where(['label'=>$label,'street' => $street,'zipcode'=>$zipcode,'city'=>$city,'country'=>$country])->first();
        
    }
    public function favoriteAddress($address){
        $address->is_favorite = !$address->is_favorite;
        $address->update();
        return $address;
    }
}
