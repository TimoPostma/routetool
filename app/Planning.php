<?php

namespace App;

use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Model;

class Planning extends Model
{
    public function organisation()
    {
        return $this->belongsTo('App\Organisation');
    }

    public function permissionPlanning()
    {
        return $this->belongsTo('App\PermissionPlanning');
    }

    public function getAlPlanningsByOrganisationId($organisationId)
    {
        return $this->where('organisation_id', $organisationId)->orderby('created_at','desc')->get();
    }

    public function getAlPlanningsNameAndDescriptionByOrganisationId($organisationId)
    {
        return $this->where('organisation_id', $organisationId)->get(['name', 'description', 'id', 'created_at']);
    }

    public function storePlanning($request ,$organisationId, $permissionId,$routeData)
    {
        $this->name = $request->name;
        $this->description = $request->description;
        $this->organisation_id = $organisationId;
        $this->permission_planning_id = $permissionId;
        $this->routedata = $routeData;
        $this->save();
        return $this;
    }
    public function updatePlanning($planning, $name , $description, $organisationId, $permissionId)
    {   $planning->name = $name;
        $planning->description = $description;
        $planning->update();
        return $planning;
        
     
    }

    public function getPlanningById($id)
    {
        return $this->where('id', $id)->first();
    }

    public function checkIfPlanningExists($id)
    {
        return $this->where('id', $id)->first();
    }

    /**
     * Store routedata in the database
     */
    public function storeRouteData($planning, $routedata)
    {
        $planning->routedata = $routedata;
        $planning->update();
    }

    public function getRouteData($planningId)
    {
        return $this->select('id', 'routedata')->where('id', $planningId)->first();
    }
    public function apply($planning,$routes)
    {   $planning->routeData = $routes;
        $planning->update();
        return $planning;
    }
}
