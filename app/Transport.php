<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;


/**
 * A model for transport rows
 *
 * A transport is a transport method that is specific for an organisation. For instance, 'our truck' is an example.
 */
class Transport extends Model

{   

    protected $showMap = true;
    protected $showTime = true;
    protected $circular = true;

    //makes a new transport with a label
    public function makeTransportData($label){
             
        return $transportdata = json_encode([
            'label'=>$label,
            'key' => Str::random(6),
            'showMap'=>$this->showMap,
            'showTime'=>$this->showTime,
            'circular'=>$this->circular
        ]);
    }
 
    /**
     * Get the organisation where this transport belongs to
     */
    public function organisation()
    {
        return $this->belongsTo('App\Organisation');
    }
    public function getTransportsByOrganisationId($id)
    {
        return $this->where('organisation_id', $id)->orderBy('id', 'DESC')->get();
    }
    public function getTransportById($id)
    {
        return $this->select("*")->where('id','=',$id)->first();
    }

    /**
     * Find a transport based on the label.
     *
     * This function requires an exact text match.
     */
    public function getTransportByLabel($label)
    {
	// ignore trailing spaces
	$label = trim($label);

        return $this->select("*")->where('label','=',$label)->firstOr(function() { return null;});
    }

    public function storeTransport($request,$organisationId)
    {
        $this->label = $request->label;
        $this->organisation_id = $organisationId;
        $transportdata = ['speed'=>$request->speed,'method'=>$request->method];
        $this->transportdata = json_encode($transportdata);
        $this->save();
        return $this;
    }

    /**
     * Sace data that came from OVS
     */
    public function saveFromOvs($label, $data)
    {   
        $user = Auth::user();
	if($user === null){
		// no user has been authenticated @todo remove hardcoded Evolve
		$organisationId = 'e69f8351-78a9-4f0d-8f40-c42dbbbbaa5d';
	} else {
	 	$organisationId = $user->organisation;
	}
        $this->organisation_id = $organisationId;
        $this->label = $label;
        $this->transportdata = $data;
        $this->save();
        return $this;
    }

    public function updateTransport($request,$organisationId,$transportdata)
    {
        $transport = $this->where('id',$request->id)->first();
        $transport->label = $request->label;
        $transport->transportdata = $transportdata;
        $transport->organisation_id = $organisationId;
        $transport->update();
        return $transport;
    }
    public function deleteTransport($id){
        $transport = $this->where('id',$id)->first();
        $transport->delete();

    }
}
