<?php

namespace App\Providers;

use App\Http\View\ProfileComposer;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        View()->composer('layouts.admin.components._nav', 'App\Http\View\ProfileComposer');
        View()->composer('layouts.organisation.components._nav', 'App\Http\View\OrganisationComposer');
    }
}
