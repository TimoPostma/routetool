<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMapToPlanningsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('plannings', function (Blueprint $table) {
            $table->boolean('map')->default(true);
            $table->boolean('calc')->default(true);
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('plannings', 'map')){
  
            Schema::table('plannings', function (Blueprint $table) {
                $table->dropColumn('map');
            });
        }
        if (Schema::hasColumn('plannings', 'calc')){
  
            Schema::table('plannings', function (Blueprint $table) {
                $table->dropColumn('calc');
            });
        }
    }
}
