<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIsOvsToPlanningsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('plannings', function (Blueprint $table) {
            // Schema::table('templates', function (Blueprint $table) {
            $table->boolean('is_locked_set')->nullable();
    
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('plannings', function (Blueprint $table) {
            if (Schema::hasColumn('plannings', 'is_locked_set')){
  
                Schema::table('plannings', function (Blueprint $table) {
                    $table->dropColumn('is_locked_set');
                });
            }
  
        });
    }
}
