<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PermissionPlanningSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permission_plannings')->insert([
            'label' => 'Read only',
            'description' => 'Can read the planning',
            'permission' => 1,
        ]);

        DB::table('permission_plannings')->insert([
            'label' => 'read and write',
            'description' => 'Can read and write to the planning ',
            'permission' => 2,
        ]);
    }
}
