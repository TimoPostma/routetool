<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Services\PlanningBoardService;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;
use Illuminate\Log\LogManager;
use TiMacDonald\Log\LogFake;


/**
 * Tests for the PlanningBoardService
 */
class PlanningTest extends TestCase
{
    /**
     * Test the creation of a location.
     *
     * @return void
     */
    public function testCreateLocation()
    {

	$label = 'Van Dam';
 	$street = 'Dorpsstraat 1';
	$zipcode = '9988AA';
	$city = 'Hoorn';
	$country = 'Nederland';
	$orders = [];

	$result = PlanningBoardService::createAddableLocation($label, $street, $zipcode, $city, $country, $orders);

	$expected = [
                        'address' => [
                                'label'   => $label,
                                'street'  => $street,
                                'zipcode' => $zipcode,
                                'city'    => $city,
                                'country' => $country
                        ],
                        'orders' => $orders
                ];

	$this->assertEquals($result, $expected);
    }

	/**
	 * Test placement of a cell in a new empty planning
	 */
	public function testCreatePlanningWithOneCell(){
		$cell = 'cell';

		$result = PlanningBoardService::createPlanningWithOneCell($cell);

                $expected = [
                                "meta" => [
                                        "type" => "planitems",
                                        "version" => "1.0.0"
                                ],
                                "cells" =>[
                                        $cell
                                ]
                        ];
		$this->assertEquals($result, $expected);
	}


	/**
	 * Creates a cell that can be added to a planning
	 */
	public function testCreateAddableCell(){
		$transport_label = 'eigen bakwagen';
		$period_label = 'maandag';
		$period_date = '2022-08-29';

		$label = 'Van Dam';
		$street = 'Dorpsstraat 1';
		$zipcode = '9988AA';
		$city = 'Hoorn';
		$country = 'Nederland';
		$orders = [];

		$location = PlanningBoardService::createAddableLocation($label, $street, $zipcode, $city, $country, $orders);

		$result = PlanningBoardService::createAddableCell($transport_label, $period_label, $period_date, $location);

		// remove random keys
		Arr::forget($result, ['transport.key', 'period.key']);

		$expected = [
		    "transport" => [
			"label" => "eigen bakwagen",
			"map" => true,
			"showTime" => true,
			"circular" => true
		    ],
		    "period" => [
			"label" => "maandag",
			"date" => "2022-08-29"
		    ],
		    "locations" => [[
			    "address" => [
				"label" => "Van Dam",
				"street" => "Dorpsstraat 1",
				"zipcode" => "9988AA",
				"city" => "Hoorn",
				"country" => "Nederland"
			    ],
			    "orders" => []
			]
		    ]
		];

		$this->assertEquals($result, $expected);
	}

	/**
	 * Adds multiple cells to a planning to see if the are merged correctly. The cells have the same 
	 * transport, period and address, so only the orders should be merged
	 */
	public function testCreateLocationInPlanningA(){

                $transport_label = 'eigen bakwagen';
                $period_label = 'maandag';
                $period_date = '2022-08-29';

                $label = 'Van Dam';
                $street = 'Dorpsstraat 1';
                $zipcode = '9988AA';
                $city = 'Hoorn';
                $country = 'Nederland';
                $orders = [[
			'label' => 'Opdracht A',
			'customer' => 'Jan',
			'code' => '2022022203'
		]];

                $locationA = PlanningBoardService::createAddableLocation($label, $street, $zipcode, $city, $country, $orders);

                $transport_label = 'eigen bakwagen';
                $period_label = 'maandag';
                $period_date = '2022-08-29';

                $label = 'Van Dam';
                $street = 'Dorpsstraat 1';
                $zipcode = '9988AA';
                $city = 'Hoorn';
                $country = 'Nederland';
                $orders = [[
			'label' => 'Opdracht B',
			'customer' => 'Piet',
			'code' => '2022022204'
		]];

                $locationB = PlanningBoardService::createAddableLocation($label, $street, $zipcode, $city, $country, $orders);


		$planning = PlanningBoardService::createEmptyPlanning();
		
		$cellA = PlanningBoardService::createAddableCell($transport_label, $period_label, $period_date, $locationA);
		$cellB = PlanningBoardService::createAddableCell($transport_label, $period_label, $period_date, $locationB);


		$planningA = PlanningBoardService::addCellToPlanboard($planning, $cellA);
		$planningB = PlanningBoardService::addCellToPlanboard($planningA, $cellB);

		$planningB = removeRandomKeysFromCells($planningB);

		$expect_json = <<<JSON
{"meta":{"type":"planitems","version":"1.0.0"},"cells":[{"transport":{"label":"eigen bakwagen","map":true,"showTime":true,"circular":true},"period":{"label":"maandag","date":"2022-08-29"},"locations":[{"address":{"label":"Van Dam","street":"Dorpsstraat 1","zipcode":"9988AA","city":"Hoorn","country":"Nederland"},"orders":[{"label":"Opdracht A","customer":"Jan","code":"2022022203"},{"label":"Opdracht B","customer":"Piet","code":"2022022204"}]}]}]}
JSON;

		$expect = json_decode($expect_json, true);

		$this->assertEquals($planningB, $expect);
	}

	/**
	 * Adds multiple cells to a planning to see if the are merged correctly
	 */
	public function testCreateLocationInPlanningB(){

                $transport_label = 'eigen bakwagen';
                $period_label = 'maandag';
                $period_date = '2022-08-29';

                $label = 'Van Dam';
                $street = 'Dorpsstraat 1';
                $zipcode = '9988AA';
                $city = 'Hoorn';
                $country = 'Nederland';
                $orders = [[
			'label' => 'Opdracht A',
			'customer' => 'Jan',
			'code' => '2022022203'
		]];

                $locationA = PlanningBoardService::createAddableLocation($label, $street, $zipcode, $city, $country, $orders);

                $transport_label = 'eigen bakwagen';
                $period_label = 'maandag';
                $period_date = '2022-08-29';

                $label = 'Loonstra';
                $street = 'Dorpsstraat 1';
                $zipcode = '9988AA';
                $city = 'Hoorn';
                $country = 'Nederland';
                $orders = [[
			'label' => 'Opdracht B',
			'customer' => 'Piet',
			'code' => '2022022204'
		]];

                $locationB = PlanningBoardService::createAddableLocation($label, $street, $zipcode, $city, $country, $orders);


		$planning = PlanningBoardService::createEmptyPlanning();
		
		$cellA = PlanningBoardService::createAddableCell($transport_label, $period_label, $period_date, $locationA);
		$cellB = PlanningBoardService::createAddableCell($transport_label, $period_label, $period_date, $locationB);


		$planningA = PlanningBoardService::addCellToPlanboard($planning, $cellA);
		$planningB = PlanningBoardService::addCellToPlanboard($planningA, $cellB);

		$planningB = removeRandomKeysFromCells($planningB);

		$expect_json = <<<JSON
{"meta":{"type":"planitems","version":"1.0.0"},"cells":[{"transport":{"label":"eigen bakwagen","map":true,"showTime":true,"circular":true},"period":{"label":"maandag","date":"2022-08-29"},"locations":[{"address":{"label":"Van Dam","street":"Dorpsstraat 1","zipcode":"9988AA","city":"Hoorn","country":"Nederland"},"orders":[{"label":"Opdracht A","customer":"Jan","code":"2022022203"}]},{"address":{"label":"Loonstra","street":"Dorpsstraat 1","zipcode":"9988AA","city":"Hoorn","country":"Nederland"},"orders":[{"label":"Opdracht B","customer":"Piet","code":"2022022204"}]}]}]}
JSON;

		$expect = json_decode($expect_json, true);

		$this->assertEquals($planningB, $expect);
	}




	/**
	 * Adds multiple cells to a planning to see if the are merged correctly
	 * add cells with different transports
	 */
	public function testCreateLocationInPlanningC(){
		$planning = PlanningBoardService::createEmptyPlanning();

                $transport_label = 'eigen bakwagen';
                $period_label = 'maandag';
                $period_date = '2022-08-29';

                $label = 'Van Dam';
                $street = 'Dorpsstraat 1';
                $zipcode = '9988AA';
                $city = 'Hoorn';
                $country = 'Nederland';
                $orders = [[
			'label' => 'Opdracht A',
			'customer' => 'Jan',
			'code' => '2022022203'
		]];

                $locationA = PlanningBoardService::createAddableLocation($label, $street, $zipcode, $city, $country, $orders);
		$cellA = PlanningBoardService::createAddableCell($transport_label, $period_label, $period_date, $locationA);
		$planningA = PlanningBoardService::addCellToPlanboard($planning, $cellA);


                $transport_label = 'ophalen';
                $period_label = 'maandag';
                $period_date = '2022-08-29';

                $label = 'Van Dam';
                $street = 'Dorpsstraat 1';
                $zipcode = '9988AA';
                $city = 'Hoorn';
                $country = 'Nederland';
                $orders = [[
			'label' => 'Opdracht B',
			'customer' => 'Piet',
			'code' => '2022022204'
		]];

        $locationB = PlanningBoardService::createAddableLocation($label, $street, $zipcode, $city, $country, $orders);
		$cellB = PlanningBoardService::createAddableCell($transport_label, $period_label, $period_date, $locationB);
		$planningB = PlanningBoardService::addCellToPlanboard($planningA, $cellB);


		// since the periods are the same, the keys must also be identical
		$periodKeyA = Arr::get($planningB, 'cells.0.period.key');
		$periodKeyB = Arr::get($planningB, 'cells.1.period.key');
		$this->assertEquals($periodKeyA, $periodKeyB);

		$planningB = removeRandomKeysFromCells($planningB);
		$expect_json = <<<JSON
{"meta":{"type":"planitems","version":"1.0.0"},"cells":[{"transport":{"label":"eigen bakwagen","map":true,"showTime":true,"circular":true},"period":{"label":"maandag","date":"2022-08-29"},"locations":[{"address":{"label":"Van Dam","street":"Dorpsstraat 1","zipcode":"9988AA","city":"Hoorn","country":"Nederland"},"orders":[{"label":"Opdracht A","customer":"Jan","code":"2022022203"}]}]},{"transport":{"label":"ophalen","map":true,"showTime":true,"circular":true},"period":{"label":"maandag","date":"2022-08-29"},"locations":[{"address":{"label":"Van Dam","street":"Dorpsstraat 1","zipcode":"9988AA","city":"Hoorn","country":"Nederland"},"orders":[{"label":"Opdracht B","customer":"Piet","code":"2022022204"}]}]}]}
JSON;

		$expect = json_decode($expect_json, true);

		$this->assertEquals($planningB, $expect);
	}

	/**
	 * Adds multiple cells to a planning to see if the are merged correctly.
	 * Add cells with the same transport but different period
	 */
	public function testCreateLocationInPlanningD(){
		$planning = PlanningBoardService::createEmptyPlanning();

                $transport_label = 'eigen bakwagen';
                $period_label = 'dinsdag';
                $period_date = '2022-08-30';

                $label = 'Van Dam';
                $street = 'Dorpsstraat 1';
                $zipcode = '9988AA';
                $city = 'Hoorn';
                $country = 'Nederland';
                $orders = [[
			'label' => 'Opdracht A',
			'customer' => 'Jan',
			'code' => '2022022203'
		]];

                $locationA = PlanningBoardService::createAddableLocation($label, $street, $zipcode, $city, $country, $orders);
		$cellA = PlanningBoardService::createAddableCell($transport_label, $period_label, $period_date, $locationA);
		$planningA = PlanningBoardService::addCellToPlanboard($planning, $cellA);


                $transport_label = 'eigen bakwagen';
                $period_label = 'maandag';
                $period_date = '2022-08-29';

                $label = 'Van Dam';
                $street = 'Dorpsstraat 1';
                $zipcode = '9988AA';
                $city = 'Hoorn';
                $country = 'Nederland';
                $orders = [[
			'label' => 'Opdracht B',
			'customer' => 'Piet',
			'code' => '2022022204'
		]];

        $locationB = PlanningBoardService::createAddableLocation($label, $street, $zipcode, $city, $country, $orders);
		$cellB = PlanningBoardService::createAddableCell($transport_label, $period_label, $period_date, $locationB);
		$planningB = PlanningBoardService::addCellToPlanboard($planningA, $cellB);


		// since the periods are the same, the keys must also be identical
		$transportKeyA = Arr::get($planningB, 'cells.0.transport.key');
		$transportKeyB = Arr::get($planningB, 'cells.1.transport.key');
		$this->assertEquals($transportKeyA, $transportKeyB);

		$planningB = removeRandomKeysFromCells($planningB);
		$expect_json = <<<JSON
{"meta":{"type":"planitems","version":"1.0.0"},"cells":[{"transport":{"label":"eigen bakwagen","map":true,"showTime":true,"circular":true},"period":{"label":"dinsdag","date":"2022-08-30"},"locations":[{"address":{"label":"Van Dam","street":"Dorpsstraat 1","zipcode":"9988AA","city":"Hoorn","country":"Nederland"},"orders":[{"label":"Opdracht A","customer":"Jan","code":"2022022203"}]}]},{"transport":{"label":"eigen bakwagen","map":true,"showTime":true,"circular":true},"period":{"label":"maandag","date":"2022-08-29"},"locations":[{"address":{"label":"Van Dam","street":"Dorpsstraat 1","zipcode":"9988AA","city":"Hoorn","country":"Nederland"},"orders":[{"label":"Opdracht B","customer":"Piet","code":"2022022204"}]}]}]}
JSON;

		$expect = json_decode($expect_json, true);

		$this->assertEquals($planningB, $expect);
	}


	/**
	 * Adds multiple cells to a planning to see if the are merged correctly.
	 * Add more than two cells
	 */
	public function testCreateLocationInPlanningE(){
		$planning = PlanningBoardService::createEmptyPlanning();

                $transport_label = 'eigen bakwagen';
                $period_label = 'dinsdag';
                $period_date = '2022-08-30';

                $label = 'Van Dam';
                $street = 'Dorpsstraat 1';
                $zipcode = '9988AA';
                $city = 'Hoorn';
                $country = 'Nederland';
                $orders = [[
			'label' => 'Opdracht A',
			'customer' => 'Jan',
			'code' => '2022022203'
		]];

                $locationA = PlanningBoardService::createAddableLocation($label, $street, $zipcode, $city, $country, $orders);
		$cellA = PlanningBoardService::createAddableCell($transport_label, $period_label, $period_date, $locationA);
		$planningA = PlanningBoardService::addCellToPlanboard($planning, $cellA);


                $transport_label = 'eigen bakwagen';
                $period_label = 'maandag';
                $period_date = '2022-08-29';

                $label = 'Van Dam';
                $street = 'Dorpsstraat 1';
                $zipcode = '9988AA';
                $city = 'Hoorn';
                $country = 'Nederland';
                $orders = [[
			'label' => 'Opdracht B',
			'customer' => 'Piet',
			'code' => '2022022204'
		]];

                $locationB = PlanningBoardService::createAddableLocation($label, $street, $zipcode, $city, $country, $orders);
		$cellB = PlanningBoardService::createAddableCell($transport_label, $period_label, $period_date, $locationB);
		$planningB = PlanningBoardService::addCellToPlanboard($planningA, $cellB);

                $transport_label = 'eigen bakwagen';
                $period_label = 'maandag';
                $period_date = '2022-08-29';

                $label = 'Van Dam';
                $street = 'Dorpsstraat 1';
                $zipcode = '9988AA';
                $city = 'Hoorn';
                $country = 'Nederland';
                $orders = [[
			'label' => 'Opdracht C',
			'customer' => 'Klaas',
			'code' => '2022022205'
		]];

                $locationC = PlanningBoardService::createAddableLocation($label, $street, $zipcode, $city, $country, $orders);
		$cellC = PlanningBoardService::createAddableCell($transport_label, $period_label, $period_date, $locationC);
		$planningC = PlanningBoardService::addCellToPlanboard($planningB, $cellC);

		$planningC = removeRandomKeysFromCells($planningC);
		$expect_json = <<<JSON
{"meta":{"type":"planitems","version":"1.0.0"},"cells":[{"transport":{"label":"eigen bakwagen","map":true,"showTime":true,"circular":true},"period":{"label":"dinsdag","date":"2022-08-30"},"locations":[{"address":{"label":"Van Dam","street":"Dorpsstraat 1","zipcode":"9988AA","city":"Hoorn","country":"Nederland"},"orders":[{"label":"Opdracht A","customer":"Jan","code":"2022022203"}]}]},{"transport":{"label":"eigen bakwagen","map":true,"showTime":true,"circular":true},"period":{"label":"maandag","date":"2022-08-29"},"locations":[{"address":{"label":"Van Dam","street":"Dorpsstraat 1","zipcode":"9988AA","city":"Hoorn","country":"Nederland"},"orders":[{"label":"Opdracht B","customer":"Piet","code":"2022022204"},{"label":"Opdracht C","customer":"Klaas","code":"2022022205"}]}]}]}
JSON;

		$expect = json_decode($expect_json, true);

		$this->assertEquals($planningC, $expect);
	}

	
        /**
         * Adds a cell similar to an OVS-call that lead to duplicate registrations on 20220903
         */
        public function testCreateLocationInPlanningF(){
                $planning = PlanningBoardService::createEmptyPlanning();

                $transport_label = 'halen';
                $period_label = '10-06-2022';
                $period_date = '2022-06-10';

                $label = 'Van Dam';
                $street = 'Dorpsstraat 1';
                $zipcode = '9988AA';
                $city = 'Hoorn';
                $country = 'Nederland';
                $orders = [[
                        'label' => 'Opdracht A',
                        'customer' => 'Jan',
                        'code' => '2022022203'
                ]];

                $locationA = PlanningBoardService::createAddableLocation($label, $street, $zipcode, $city, $country, $orders);
                $cellA = PlanningBoardService::createAddableCell($transport_label, $period_label, $period_date, $locationA);
                $planningA = PlanningBoardService::addCellToPlanboard($planning, $cellA);

		$expect_json = <<<JSON
{"meta":{"type":"planitems","version":"1.0.0"},"cells":[{"transport":{"key":"kuKcqE","label":"halen","map":true,"showTime":true,"circular":true},"period":{"key":"NmgGLx","label":"10-06-2022","date":"2022-06-10"},"locations":[{"address":{"label":"Van Dam","street":"Dorpsstraat 1","zipcode":"9988AA","city":"Hoorn","country":"Nederland"},"orders":[{"label":"Opdracht A","customer":"Jan","code":"2022022203"}]}]}]}
JSON;

                $expect = json_decode($expect_json, true);

		$planningA = removeRandomKeysFromCells($planningA);
		$expect = removeRandomKeysFromCells($expect);
                $this->assertEquals($planningA, $expect);
	}

        /**
         * Generate planning with one cell 
         */
        public function testCreateLocationInPlanningG(){

		$year = 2022;
		$week = 22;

	        $dateTime = New \DateTime();
        	$dateTime->setISODate($year, $week);
	
        	$days = ['Maandag']; //, 'Dinsdag', 'Woensdag', 'Donderdag', 'Vrijdag'];
	
        	$transports = ['dummy'];

		//Generate the period cells
		for($p = 0; $p < count($days); $p++) {
		    if($p < 1) {
			$periods[] = ['label' => $days[$p], 'date' => $dateTime->modify("+0 days")->format('Y-m-d')];
		    } else {
			$periods[] = ['label' => $days[$p], 'date' => $dateTime->modify("+1 days")->format('Y-m-d')];
		    }
		}

		$entries = [];

		$planning = PlanningBoardService::createPlanBoardData($transports, $periods, $entries);

		$planning = removeRandomKeysFromCells($planning);
	
                $expect_json = <<<JSON
{"meta":{"type":"planitems","version":"1.0.0"},"startlocation":{"label":"no_startlocation"},"cells":[{"transport":{"label":"dummy","map":true,"showTime":true,"circular":true,"selected":false,"total_time":0,"total_item_time":0},"period":{"label":"Maandag","date":"2022-05-30"},"locations":[]}]}
JSON;
                $expect = json_decode($expect_json, true);
                $this->assertEquals($planning, $expect);
	}

	/**
	 * Create planning with one cell and then add a cell
	 */
        public function testCreateLocationInPlanningH(){

                $year = 2022;
                $week = 24;

                $dateTime = New \DateTime();
                $dateTime->setISODate($year, $week);

                $days = ['Maandag']; //, 'Dinsdag', 'Woensdag', 'Donderdag', 'Vrijdag'];

                $transports = ['halen'];

                //Generate the period cells
                for($p = 0; $p < count($days); $p++) {
                    if($p < 1) {
                        $periods[] = ['label' => $days[$p], 'date' => $dateTime->modify("+0 days")->format('Y-m-d')];
                    } else {
                        $periods[] = ['label' => $days[$p], 'date' => $dateTime->modify("+1 days")->format('Y-m-d')];
                    }
                }

                $entries = [];

                $planning = PlanningBoardService::createPlanBoardData($transports, $periods, $entries);



                $transport_label = 'halen';
                $period_label = '13-06-2022';
                $period_date = '2022-06-13';

                $label = 'Van Dam';
                $street = 'Dorpsstraat 1';
                $zipcode = '9988AA';
                $city = 'Hoorn';
                $country = 'Nederland';
                $orders = [[
                        'label' => 'Opdracht A',
                        'customer' => 'Jan',
                        'code' => '2022022203'
                ]];

                $locationA = PlanningBoardService::createAddableLocation($label, $street, $zipcode, $city, $country, $orders);
                $cellA = PlanningBoardService::createAddableCell($transport_label, $period_label, $period_date, $locationA);
                $planningA = PlanningBoardService::addCellToPlanboard($planning, $cellA);
                $planningA = removeRandomKeysFromCells($planningA);

                $expect_json = <<<JSON
{"meta":{"type":"planitems","version":"1.0.0"},"startlocation":{"label":"no_startlocation"},"cells":[{"transport":{"label":"halen","map":true,"showTime":true,"circular":true,"selected":false,"total_time":0,"total_item_time":
0},"period":{"label":"Maandag","date":"2022-06-13"},"locations":[{"address":{"label":"Van Dam","street":"Dorpsstraat 1","zipcode":"9988AA","city":"Hoorn","country":"Nederland"},"orders":[{"label":"Opdracht A","customer":"Jan","code":"2022022203"}]}]}]}
JSON;

                $expect = json_decode($expect_json, true);

                $expect = removeRandomKeysFromCells($expect);
                $this->assertEquals($planningA, $expect);
        }

	/**
	 * Create planning with one cell and then add the same  cell twice
	 */
        public function testCreateLocationInPlanningI(){

//                 $year = 2022;
//                 $week = 24;

//                 $dateTime = New \DateTime();
//                 $dateTime->setISODate($year, $week);

//                 $days = ['Maandag']; //, 'Dinsdag', 'Woensdag', 'Donderdag', 'Vrijdag'];

//                 $transports = ['halen'];

//                 //Generate the period cells
//                 for($p = 0; $p < count($days); $p++) {
//                     if($p < 1) {
//                         $periods[] = ['label' => $days[$p], 'date' => $dateTime->modify("+0 days")->format('Y-m-d')];
//                     } else {
//                         $periods[] = ['label' => $days[$p], 'date' => $dateTime->modify("+1 days")->format('Y-m-d')];
//                     }
//                 }

//                 $entries = [];

//                 $planning = PlanningBoardService::createPlanBoardData($transports, $periods, $entries);



//                 $transport_label = 'halen';
//                 $period_label = '13-06-2022';
//                 $period_date = '2022-06-13';

//                 $label = 'Van Dam';
//                 $street = 'Dorpsstraat 1';
//                 $zipcode = '9988AA';
//                 $city = 'Hoorn';
//                 $country = 'Nederland';
//                 $orders = [[
//                         'label' => 'Opdracht A',
//                         'customer' => 'Jan',
//                         'code' => '2022022203'
//                 ]];

//                 $locationA = PlanningBoardService::createAddableLocation($label, $street, $zipcode, $city, $country, $orders);
//                 $cellA = PlanningBoardService::createAddableCell($transport_label, $period_label, $period_date, $locationA);

// 		// add first time
//                 $planningA = PlanningBoardService::addCellToPlanboard($planning, $cellA);
// 		// add second time
// 		$planningA = PlanningBoardService::addCellToPlanboard($planningA, $cellA);

//                 $planningA = removeRandomKeysFromCells($planningA);

//                 $expect_json = <<<JSON
// {"meta":{"type":"planitems","version":"1.0.0"},"startlocation":{"label":"no_startlocation"},"cells":[{"transport":{"label":"halen","map":true,"showTime":true,"circular":true,"selected":false,"total_time":0,"total_item_time":
// 0},"period":{"label":"Maandag","date":"2022-06-13"},"locations":[{"address":{"label":"Van Dam","street":"Dorpsstraat 1","zipcode":"9988AA","city":"Hoorn","country":"Nederland"},"orders":[{"label":"Opdracht A","customer":"Jan","code":"2022022203"}]}]}]}
// JSON;

//                 $expect = json_decode($expect_json, true);

//                 $expect = removeRandomKeysFromCells($expect);
//                 $this->assertEquals($planningA, $expect);
        }


}

/**
 * Removes random keys from cells in the planning to make comparison easier.
 */
function removeRandomKeysFromCells($planning){
	$x = collect($planning['cells'])->map(function($obj) { Arr::forget($obj, ['transport.key', 'period.key']);  return $obj;});
	$planning['cells'] = $x->toArray();
	return $planning;
}
