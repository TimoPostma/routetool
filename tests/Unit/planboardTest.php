<?php

namespace Tests\Unit;
use App\Planning;
use PHPUnit\Framework\TestCase;
use App\Services\PlanboardService;
class planboardTest extends TestCase
{
    
    public function test_setting_itemtime_planboard_empty()
    {   
        $planning = new Planning;
        $itemtime = "888";
        $output = PlanboardService::set_default_itemtime($planning,$itemtime);
        $expected = $itemtime;
        $result = $output->routedata['default']['item_time'];
        $this->assertEquals($expected, $result);
    }
    public function test_setting_itemtime_planboard_withcontent()
    {   $itemtime = "888";
        $planning = new Planning;
        $planning->routedata = ["default"=>["item_time"=>"222"]];
        $output = PlanboardService::set_default_itemtime($planning,$itemtime);
        $expected = $itemtime;
        $result = $output->routedata['default']['item_time'];
        $this->assertEquals($expected, $result);
    }
    // public function test_setting_itemtime_planboard_is_locked_set(){
    //     $itemtime = "888";
    //     $planning = new Planning;
    //     $planning->routedata = ["default"=>["item_time"=>"222"]];
    //     $planning->is_locked_set = true;
    //     $output = PlanboardService::set_default_itemtime($planning,$itemtime);
    //     $expected = "";
    //     $result = $output->routedata['default']['item_time'];
    //     $this->assertEquals($expected, $result);

    // }




    // public function test_setting_overhead_planboard()
    // {   
    //     $planning = new Planning;
    //     $overheadtime = "888";
    //     $output = PlanboardService::set_default_overheadtime($planning,$overheadtime);
    //     $expected = $overheadtime;
    //     $result = $output->routedata['default']['overhead_time'];
    //     $this->assertEquals($expected, $result);
    // }
    // public function test_default(){
    //     $input = json_decode('{"defaults":{"x":"y"}}');
    //     $expected = json_decode('{"defaults":{"x":"y"}}');
    //     $output = PlanboardService::set_defaults($input,"888","999");

    //     $this->assertEquals($expected, $output);

    // }
}
